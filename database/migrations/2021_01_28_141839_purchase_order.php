<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PurchaseOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order', function (Blueprint $table) {
            $table->increments('id');
			$table->string('ref_no');
			$table->date('current_date');
			$table->date('due_date');
			$table->integer('supplier_id');
			$table->string('delivery_address');
			$table->string('note');
			$table->tinyInteger('order_status')->default(1)->comment = '1=Draft,2=Sent,3=Received,4=Partial received,5=Canceled';
			$table->string('tot_amt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order');
    }
}
