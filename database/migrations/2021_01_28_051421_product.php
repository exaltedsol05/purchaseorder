<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Product extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
			$table->string('po_no');
			$table->integer('product_id');
			$table->integer('product_quantity');
			$table->date('current_date');
			$table->date('due_date');
			$table->integer('supplier_id');
			$table->string('delivery_address');
			$table->string('note');
			$table->tinyInteger('order_status')->default(1)->comment = '1=Draft,2=Sent,3=Received,4=Partial received,5=Canceled';
			$table->string('bill_status');
			$table->string('threshold');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
