<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PurchaseOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_details', function (Blueprint $table) {
            $table->id();
            $table->string('ref_no');
            $table->string('woocomerceId');
            $table->string('product_name');
            $table->string('sku');
            $table->string('brand_name');
            $table->string('barcode');
            $table->integer('quantity');
            $table->integer('cost');
            $table->tinyinteger('pType')->default(0)->comment = '0=Real,1=SupplierCost';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_details');
    }
}
