<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\companyController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PurchaseOrderController;
use App\Http\Controllers\emailManagement;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('xero.auth.success');
Route::post('settings_store', [HomeController::class, 'store'])->name('home.store');
Route::get('address_delete/{id}', [HomeController::class, 'address_delete'])->name('home.address_delete');
Route::post('DefaultAddress', [HomeController::class, 'DefaultAddress'])->name('home.DefaultAddress');
Route::get('/manage/callback', [HomeController::class, 'callback'])->name('xero.auth.callback');

Route::get('suppliers', [SupplierController::class, 'index'])->name('supplier.index');
Route::get('supplier_delete/{id}', [SupplierController::class, 'destroy'])->name('supplier.destroy');
Route::get('supplier_create', [SupplierController::class, 'create'])->name('supplier.create');
Route::get('supplier_edit/{id}', [SupplierController::class, 'edit'])->name('supplier.edit');
Route::get('supplier_view/{id}', [SupplierController::class, 'view'])->name('supplier.view');
Route::post('supplier_post', [SupplierController::class, 'store'])->name('supplier.store');
Route::post('supplier_edit_post/{id}', [SupplierController::class, 'update'])->name('supplier.update');
Route::get('cost_delete/{id}/{costId}', [SupplierController::class, 'cost_delete'])->name('supplier.cost_delete');
Route::post('changeStatus', [SupplierController::class, 'changeStatus'])->name('supplier.changeStatus');
Route::post('supplierList', [SupplierController::class, 'supplierList'])->name('order.supplierList');

Route::get('create-product', [ProductController::class, 'create'])->name('product.create');
Route::post('product_post', [ProductController::class, 'store'])->name('product.store');
Route::get('product_delete/{id}/{table}', [ProductController::class, 'destroy'])->name('product.destroy');
Route::post('woocomerceData', [ProductController::class, 'woocomerceData'])->name('product.woocomerceData');
Route::get('woocomerceVariationData', [ProductController::class, 'woocomerceVariationData'])->name('product.woocomerceVariationData');
Route::get('products', [ProductController::class, 'index'])->name('product.index');
Route::get('product-view/{id}/{table}', [ProductController::class, 'view'])->name('product.view');
Route::get('product-edit/{id}/{table}', [ProductController::class, 'edit'])->name('product.edit');
Route::post('product-edit-post/{id}', [ProductController::class, 'update'])->name('product.update');
Route::post('getSCode', [ProductController::class, 'getSCode'])->name('product.getSDetails');
Route::post('getPDetails', [ProductController::class, 'getPDetails'])->name('product.getPDetails');
Route::post('filterProduct', [ProductController::class, 'filterProduct'])->name('product.filterProduct');
Route::post('changePStatus', [ProductController::class, 'changePStatus'])->name('product.changePStatus');

Route::get('create-order', [PurchaseOrderController::class, 'create'])->name('po.create_order');
Route::get('edit-order/{id}', [PurchaseOrderController::class, 'edit_order'])->name('po.edit_order');
Route::get('edit-order-mail/{id}', [PurchaseOrderController::class, 'edit_order_mail'])->name('po.edit_order_mail');
Route::post('edit-order-post/{id}', [PurchaseOrderController::class, 'edit_order_post'])->name('po.edit_order_post');
Route::get('prepare-order/{id}', [PurchaseOrderController::class, 'prepare_order'])->name('po.prepare_order');
Route::post('order_post', [PurchaseOrderController::class, 'store'])->name('order.store');
Route::post('order_duplicate', [PurchaseOrderController::class, 'duplicate'])->name('order.duplicate');
Route::get('orders', [PurchaseOrderController::class, 'orders'])->name('order.orders');
Route::get('draft', [PurchaseOrderController::class, 'draft'])->name('order.draft');
Route::get('sent', [PurchaseOrderController::class, 'sent'])->name('order.sent');
Route::get('order_view/{id}', [PurchaseOrderController::class, 'view'])->name('order.view');
Route::get('received', [PurchaseOrderController::class, 'received'])->name('order.received');
Route::get('cancel', [PurchaseOrderController::class, 'cancel'])->name('order.cancel');
Route::get('partial-received', [PurchaseOrderController::class, 'partial_received'])->name('order.partial-received');
Route::get('sent_order/{id}', [PurchaseOrderController::class, 'sent_order'])->name('order.sent_order');
Route::get('getBarcode/{po_no}', [PurchaseOrderController::class, 'getBarcode'])->name('order.getBarcode');
Route::post('partial-order/{id}', [PurchaseOrderController::class, 'partial'])->name('order.partial');
Route::get('od_delete/{id}/{pId}', [PurchaseOrderController::class, 'od_delete'])->name('order.od_delete');

Route::post('getSDetails', [PurchaseOrderController::class, 'getSDetails'])->name('order.getSDetails');
Route::get('searchProduct', [PurchaseOrderController::class, 'searchProduct'])->name('order.searchProduct');
Route::post('getProduct', [PurchaseOrderController::class, 'getProduct'])->name('order.getProduct');
Route::post('getProductList', [PurchaseOrderController::class, 'getProductList'])->name('order.getProductList');
//Route::get('getBarcode', [PurchaseOrderController::class, 'getBarcode'])->name('order.getBarcode');

Route::get('create-email', [emailManagement::class, 'create'])->name('email.create_email');
Route::post('email-post', [emailManagement::class, 'store'])->name('email.store');
Route::get('edit-email/{id}', [emailManagement::class, 'edit_email'])->name('email.edit_email');
Route::post('edit-email-post/{id}', [emailManagement::class, 'edit_email_post'])->name('email.edit_email_post');
Route::get('emails', [emailManagement::class, 'email'])->name('email.index');

Route::get('company', [companyController::class, 'index'])->name('company.index');
Route::post('company_store', [companyController::class, 'store'])->name('company.store');