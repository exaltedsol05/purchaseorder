@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Company</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Company</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Company</h3>
                    </div>
					@if (session('msg'))
                        <div class="alert alert-{{session('msgType')}}" role="alert">
                            {{session('msg')}}
                        </div>
                    @endif
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmclient" method="post" action="{{ route('company.store') }}" enctype="multipart/form-data">
					@csrf
                        <div class="card-body">
                            <div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Company name</label>
										<input type="text" class="form-control" name="name" value="{{$company->name}}" placeholder="Company name" required>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Company phone</label>
										<input type="text" class="form-control" name="phone" value="{{$company->phone}}" placeholder="Company phone" required>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Company email</label>
										<input type="text" class="form-control" name="email" value="{{$company->email}}" placeholder="Company email" required>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>PDF logo</label>
										<!--<input type="text" class="form-control" name="logo" value="{{$company->logo}}" placeholder="PDF logo" required>-->
										<input type="file" class="form-control" name="image">
										<input type="hidden" name="logo" value="{{$company->logo}}">
										<img src="{{url('public/images').'/'.$company->logo}}">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>PDF prefix</label>
										<input type="text" class="form-control" name="pdf_prefix" value="{{$company->pdf_prefix}}" placeholder="PDF prefix" required>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>PDF footer text</label>
										<textarea class="form-control" id="pdf_footer" name="pdf_footer" placeholder="PDF footer text" required>{{$company->pdf_footer}}</textarea>
									</div>
								</div>
                            </div>
                            <!--<div class="row" id="sDetails">
                            </div>-->
                        </div>
                        <!-- /.card-body -->
						<div class="card-footer">
							<div class="row">
								<div class="col-md-12">
									<button type="button" name="btnSubmit" id='addOrderButton' class="btn btn-success hidden-btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add</button>
								</div>
							</div>
						</div>
						<div class="card-body" id="tRow">
							<div class="row" id="">
								<div class="col-md-8">
                                    <div class="form-group">
                                        <label>Address</label>
                                    </div>
                                </div>
								<div class="col-md-2">
									<div class="form-group text-center">
										<label>Default</label>
									</div>
								</div>
							</div>
							<?php $i=1;?>
							@foreach($delivery_address as $delivery_addressval)
							<div class="row" id="dataRow{{$i}}">
								<div class="col-md-8">
                                    <div class="form-group">
										<textarea class="form-control" name="delivery_address[]" placeholder="Address" required>{{$delivery_addressval->delivery_address}}</textarea>
                                        <input type="hidden" name="odId[]" value="{{$delivery_addressval->id}}">
                                    </div>
                                </div>
								<div class="col-md-2 text-center">
									<?php if($delivery_addressval->is_default == 1){ ?>
									<button type="button" id="DefaultAddress{{$delivery_addressval->id}}" onclick="DefaultAddress('{{$delivery_addressval->id}}')" class="DefaultAddress btn btn-warning hidden-btn-xs">Default</button>
									<?php }else{ ?>
									<button type="button" id="DefaultAddress{{$delivery_addressval->id}}" onclick="DefaultAddress('{{$delivery_addressval->id}}')" class="DefaultAddress btn btn-secondary hidden-btn-xs">Default</button>
									<?php } ?>
                                </div>
								<div class="col-md-2 col-sm-2 col-xs-2">
									<a href="{{route('home.address_delete',['id'=> $delivery_addressval->id])}}"><button id="'+i+'" type="button" name="btnSubmit" class="btn btn-danger pull-right btn_remove"><i class="fa fa-trash" style="color:#fff"></i></button></a>
								</div>
							</div>
							<?php $i++;?>
							@endforeach
						</div>
                        <div class="card-footer">
                            <button type="submit" name="btnSubmit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
	$(document).ready(function(){
		CKEDITOR.replace( 'pdf_footer', {
			allowedContent:true,
		});
		if(<?php echo $delivery_address !=''?>){
			var i = <?php echo count($delivery_address);?>;
		}else{
			var i=1;
		}
		$("#addOrderButton").click(function () {
			i++;
			 $('#tRow').append('<div id="dataRow'+i+'" class="row"><div class="col-md-8"><div class="form-group"><textarea class="form-control" name="delivery_address[]" placeholder="Address" required></textarea><input type="hidden" name="odId[]" value=""></div></div><div class="col-md-2"></div><div class="col-md-2 col-sm-2 col-xs-2"><button id="'+i+'" onclick="remRow('+i+')" type="button" name="btnSubmit" class="btn btn-danger pull-right btn_remove"><i class="fa fa-trash" style="color:#fff"></i></button></div></div>');
		});
	});
	function DefaultAddress(d) {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			 }
		});
		$.ajax({
			url: "{{url('/DefaultAddress')}}",
			method: "POST",
			data: {id:d},
			dataType: 'html',
			success: function(response) {
				//alert(response);
				if(response==1){
					alert('Default address change successfully');
					$(".DefaultAddress").removeClass('btn-warning','btn-secondary').addClass('btn-secondary');
					$("#DefaultAddress"+d).removeClass('btn-secondary').addClass('btn-warning');
				}else{
					alert('Default address not change');
				}
			}
		});
	}
	function remRow(c) {
		$("#dataRow" + c).remove();
	}
</script>
@endsection