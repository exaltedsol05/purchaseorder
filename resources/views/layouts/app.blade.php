<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Purchase Order</title>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
	<!-- iCheck -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
	<!-- DataTables -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.css') }}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}">
	<!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
	<!-- Date picker -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.css') }}">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	$.widget.bridge('uibutton', $.ui.button)
	</script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- Select2 -->
	<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
	<!-- DataTables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
	<!-- ChartJS -->
	<script src="{{ asset('assets/plugins/chart.js/Chart.min.js') }}"></script>
	<!-- JQVMap -->
	<script src="{{ asset('assets/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
	<!-- jQuery Knob Chart -->
	<script src="{{ asset('assets/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
	<!-- date picker -->
	<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
	<!-- daterange picker -->
	<script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
	<!-- Summernote -->
	<script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
	<!-- overlayScrollbars -->
	<script src="{{ asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('assets/dist/js/adminlte.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('assets/dist/js/demo.js') }}"></script>
<!-- for audio sound------>
<!--<script src=" http://www.createjs.com/#!/SoundJS/demos/testSuite"></script>-->
<!--<script src="https://code.createjs.com/1.0.0/easeljs.min.js"></script>-->
<script src="{{ asset('assets/plugins/audio/audio.min.js') }}"></script>
<script src="https://code.createjs.com/1.0.0/soundjs.min.js"></script>
	<script>
	  $(function () {
		$("#example1").DataTable({
			'ordering'    : false,
		});
		$('#example2').DataTable({
		  "sScrollX": true,
		  'paging'      : false,
		  'lengthChange': false,
		  'searching'   : false,
		  'ordering'    : false,
		  'info'        : false,
		  'autoWidth'   : false
		})
		$('#example3').DataTable({
		  "sScrollX": true,
		  'paging'      : false,
		  'lengthChange': false,
		  'searching'   : false,
		  'ordering'    : false,
		  'info'        : false,
		  'autoWidth'   : false
		})
		$("#exampleProduct").DataTable({
			'ordering'    : false,
		});
	  });
	</script>
	<script>
	$(document).ready(function() {
		//$("#datepicker").datepicker();
		$('#datepicker').datepicker({
			format: 'dd/mm/yyyy'
		});
		$('#dueDate').datepicker({
			format: 'dd/mm/yyyy'
		});
	});
</script>
</head>
<!--1<style> 
	#loader { 
		border: 12px solid #f3f3f3; 
		border-radius: 50%; 
		border-top: 12px solid #444444; 
		width: 70px; 
		height: 70px; 
		animation: spin 1s linear infinite; 
	} 
	  
	@keyframes spin { 
		100% { 
			transform: rotate(360deg); 
		} 
	} 
	  
	.center { 
		position: absolute; 
		top: 0; 
		bottom: 0; 
		left: 0; 
		right: 0; 
		margin: auto; 
	} 
</style>--> 
<body>
<!--<div id="loader" class="center"></div> -->
    <div id="app">
		<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
			<!-- Left navbar links -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
				</li>
			</ul>
			<!-- Right navbar links -->
			<ul class="navbar-nav ml-auto">
				<!-- Authentication Links -->
				@guest
					<!--@if (Route::has('login'))
						<li class="nav-item">
							<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
						</li>
					@endif
					
					@if (Route::has('register'))
						<li class="nav-item">
							<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
						</li>
					@endif-->
				@else
					<li class="nav-item dropdown">
						<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
							{{ Auth::user()->name }}
						</a>

						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="{{ route('logout') }}"
							   onclick="event.preventDefault();
											 document.getElementById('logout-form').submit();">
								{{ __('Logout') }}
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
								@csrf
							</form>
						</div>
					</li>
				@endguest
			</ul>
		</nav>
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
			<!-- Sidebar -->
			<div class="sidebar">
			<a href="" class="brand-link text-center">
				<h3 class="brand-text font-weight-light">Purchase Order</h3>
			</a>
			<!-- Sidebar Menu -->
			<nav class="mt-2">
				<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false"> 
					
					<li class="nav-item has-treeview<?php echo (Request::segment(1)=='create-order') || (Request::segment(1)=='draft') || (Request::segment(1)=='orders') || (Request::segment(1)=='edit-order') || (Request::segment(1)=='sent') || (Request::segment(1)=='prepare-order') || (Request::segment(1)=='received') || (Request::segment(1)=='partial-received') || (Request::segment(1)=='cancel') || (Request::segment(1)=='order_view') ? ' menu-open ' : '';?>">
						<a href="#" class="nav-link">
							<i class="nav-icon fas fa-shopping-basket" aria-hidden="true"></i>
							<p>Purchase Order<i class="right fa fa-angle-left"></i></p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="{{ route('order.orders') }}" class="nav-link<?php echo Request::segment(1)=='orders' ? ' active' : ''; ?>">
									<i class="fas fa-list" aria-hidden="true"></i>
									<p>Purchase Orders</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('order.sent') }}" class="nav-link<?php echo Request::segment(1)=='sent' ? ' active' : ''; ?>">
									<i class="fas fa-share-square" aria-hidden="true"></i>
									<p>Placed</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('po.create_order') }}" class="nav-link<?php echo Request::segment(1)=='create-order' ? ' active' : ''; ?>">
									<i class="fas fa-plus-square" aria-hidden="true"></i>
									<p>Create New</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('order.draft') }}" class="nav-link<?php echo Request::segment(1)=='draft' ? ' active' : ''; ?>">
									<i class="fab fa-firstdraft" aria-hidden="true"></i>
									<p>Draft</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('order.partial-received') }}" class="nav-link<?php echo Request::segment(1)=='partial-received' ? ' active' : ''; ?>">
									<i class="fas fa-clipboard-list" aria-hidden="true"></i>
									<p>Partially Waiting</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('order.received') }}" class="nav-link<?php echo Request::segment(1)=='received' ? ' active' : ''; ?>">
									<i class="fas fa-receipt" aria-hidden="true"></i>
									<p>Received</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('order.cancel') }}" class="nav-link<?php echo Request::segment(1)=='cancel' ? ' active' : ''; ?>">
									<i class="fas fa-window-close" aria-hidden="true"></i>
									<p>Cancelled</p>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item">
						<a href="{{ route('supplier.index') }}" class="nav-link<?php echo (Request::segment(1)=='suppliers') || (Request::segment(1)=='supplier_create') || (Request::segment(1)=='supplier_edit') || (Request::segment(1)=='supplier_view') ? ' active' : ''; ?>">
							<i class="nav-icon fa fa-users"></i>
							<p>Supplier</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{ route('product.index') }}" class="nav-link<?php echo (Request::segment(1)=='products') ||(Request::segment(1)=='create-product') ||(Request::segment(1)=='product-view') ? ' active' : ''; ?>">
							<i class="nav-icon fab fa-product-hunt"></i>
							<p>Products</p>
						</a>
						<!--<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="{{ route('product.create') }}" class="nav-link<?php //echo Request::segment(1)=='create-product' ? ' active' : ''; ?>">
									<i class="fas fa-plus-square" aria-hidden="true"></i>
									<p>Add Product</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('product.index') }}" class="nav-link<?php //echo Request::segment(1)=='products' ? ' active' : ''; ?>">
									<i class="fas fa-list" aria-hidden="true"></i>
									<p>List Product</p>
								</a>
							</li>
						</ul>-->
					</li>
					<li class="nav-item">
						<a href="{{ route('email.index') }}" class="nav-link<?php echo (Request::segment(1)=='emails') || (Request::segment(1)=='create-email') || (Request::segment(1)=='edit-email') ? ' active' : ''; ?>">
							<i class="nav-icon far fa-envelope"></i>
							<p>Email Management</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{ route('xero.auth.success') }}" class="nav-link<?php echo Request::segment(1)=='home' ? ' active' : ''; ?>">
							<i class="nav-icon fa fa-cog"></i>
							<p>Settings</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{ route('company.index') }}" class="nav-link<?php echo Request::segment(1)=='company' ? ' active' : ''; ?>">
							<i class="nav-icon fas fa-laptop-house"></i>
							<p>Company</p>
						</a>
					</li>
					<!-- Logout Start -->
					<li class="nav-item">
						<a href="{{ route('logout') }}"
							   onclick="event.preventDefault();
											 document.getElementById('logout-form').submit();" class="nav-link">
							<i class="nav-icon fas fa-power-off"></i>
							<p>Logout</p>
						</a>
					</li>
					<!-- Logout End -->
				</ul>
			</nav>
			<!-- /.sidebar-menu -->
			</div>
			<!-- /.sidebar -->
		</aside>

        <main class="content-wrapper">
            @yield('content')
        </main>
    </div> <!-- /.content-wrapper -->
	<footer class="main-footer">
		<strong>Copyright &copy; 2021.</strong>
		Purchase Order.
	</footer>
	
	<!-- Delete Common Popup Module End -->
	<script>
	$(document).ready(function(){
		$('.select2').select2();
	});
	</script>
</body>
</html>
