@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Edit Product</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Product</a></li>
                    <li class="breadcrumb-item active">Edit Product</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit  Product</h3>
                    </div>
					@if (session('msg'))
                        <div class="alert alert-{{session('msgType')}}" role="alert">
                            {{session('msg')}}
                        </div>
                    @endif
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmclient" method="post" action="{{route('product.update',[$product->id])}}" enctype="multipart/form-data">
					@csrf
                        <div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<div class="">
										<label>Id : </label> {{$product->product_id}}
										<input type="hidden" id="product_id" name="product_id" value="{{$product->product_id}}">
									</div>
								</div>
							</div>
							<div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Name" value="{{$product->name}}" required>
										<input type="hidden" id="table_name" name="table_name" value="{{$product->table_name}}">
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Sku</label>
                                        <input type="text" class="form-control" name="sku" placeholder="Sku" value="{{$product->sku}}" required>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
										<label>Supplier name</label>
										<select name="supplier_id" id="supplier_id" class="form-control select2" onchange="GetsDetails()" required tabindex="1">
											<option value="">Select...</option>
											@foreach($supplierArr as $supplierVal)
											<option value="{{$supplierVal->id}}"<?php echo($supplierVal->name == $product->supplier_name ? 'selected' : '');?>>({{$supplierVal->supplier_code}}) {{$supplierVal->name}}</option>
											@endforeach
										</select>
									</div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Supplier Product Code</label>
                                        <input type="text" class="form-control" id="supplier_code" name="supplier_code" value="{{$product->supplier_code}}" placeholder="Supplier Code" required>
                                        <input type="hidden" id="supplier_name" name="supplier_name" value="{{$product->supplier_name}}">
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Purchase Cost</label>
                                        <input type="text" class="form-control" name="purchase_cost" value="{{$product->purchase_cost}}" placeholder="Purchase Cost">
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Uom</label>
                                        <input type="text" class="form-control" name="uom" value="{{$product->uom}}" placeholder="Uom">
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Regular Price</label>
                                        <input type="text" class="form-control" name="regular_price" value="{{$product->regular_price}}" placeholder="Regular Price" required>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Barcode</label>
                                        <input type="text" class="form-control" name="barcode" value="{{$product->barcode}}" placeholder="Barcode" required>
                                    </div>
                                </div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Manage Stock</label>
										<select name="manage_stock" class="form-control" required>
											<option value="">Select...</option>
											<option value="yes"<?php echo($product->manage_stock == 'yes' ? 'selected' : '');?>>Yes</option>
											<option value="no"<?php echo($product->manage_stock == 'no' ? 'selected' : '');?>>No</option>
										</select>
									</div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Threshold</label>
                                        <input type="text" class="form-control" name="threshold" value="{{$product->threshold}}" placeholder="Threshold">
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Bin Location</label>
                                        <input type="text" class="form-control" name="bin_location" value="{{$product->bin_location}}" placeholder="Bin Location" required>
                                    </div>
                                </div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Status</label>
										<select name="status" class="form-control" required>
											<option value="">Select...</option>
											<option value="0"<?php echo($product->status == '0' ? 'selected' : '');?>>Active</option>
											<option value="1"<?php echo($product->status == '1' ? 'selected' : '');?>>Inactive</option>
										</select>
									</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="btnSubmit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<script>
function GetsDetails() 
{
	var supplier_id = $('#supplier_id').val();
	//alert(supplier_id);
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 }
	});
	$.ajax({
		url: "{{url('/getSCode')}}",
		method: "POST",
		data: {supplier_id:supplier_id},
		dataType: 'html',
		success: function(response) {
			var obj = jQuery.parseJSON(response);
			$('#supplier_code').val(obj.sCode);
			$('#supplier_name').val(obj.supplier_name);
		}
	});
	//console.log($("meta[name='csrf-token']").attr('content'));
}
</script>
@endsection