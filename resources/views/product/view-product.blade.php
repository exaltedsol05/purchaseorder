@extends('layouts.app')

@section('content')
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #ffc107;
}

input:focus + .slider {
  box-shadow: 0 0 1px #ffc107;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">View Product</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Product</a></li>
                    <li class="breadcrumb-item active">View Product</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">View  Product</h3>
                    </div>
					@if (session('msg'))
                        <div class="alert alert-{{session('msgType')}}" role="alert">
                            {{session('msg')}}
                        </div>
                    @endif
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmclient" method="post" action="" enctype="multipart/form-data">
					@csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="">
                                        <label>Id : </label> <?PHP echo($product->product_id != '' ? $product->product_id : 'NA'); ?>
										<input type="hidden" id="table_name" value="{{$product->table_name}}">
                                    </div>
                                    <div class="">
                                        <label>Name : </label> <?PHP echo($product->name != '' ? $product->name : 'NA'); ?>
                                    </div>
                                    <div class="">
                                        <label>Sku : </label> <?PHP echo($product->sku != '' ? $product->sku : 'NA'); ?>
                                    </div>
                                    <div class="">
                                        <label>Brand Name : </label> <?PHP echo($product->brand_name != '' ? $product->brand_name : 'NA'); ?>
                                    </div>
                                    <div class="">
                                        <label>Supplier Id : </label>
										<?php
										if($product->table_name == 'product'){
											if(in_array($product->supplier_name, $supplierArr)) {
												$sName = array_search($product->supplier_name,$supplierArr);
											}else{
												$sName = $product->supplier_name;
											}
										}else{
											$sName = $product->supplier_name;
										}
										
										?>
										<?PHP echo($sName != '' ? $sName : 'NA'); ?>
                                    </div>
                                    <div class="">
                                        <label>Supplier Code : </label> <?PHP echo($product->supplier_code != '' ? $product->supplier_code : 'NA'); ?>
                                    </div>
                                    <div class="">
                                        <label>Purchase Price : </label> <?PHP echo($product->purchase_cost != '' ? $product->purchase_cost : 'NA'); ?>
                                    </div>
                                    <div class="">
                                        <label>UOM : </label> <?PHP echo($product->uom != '' ? $product->uom : 'NA'); ?>
                                    </div>
                                    <div class="">
                                        <label>Barcode : </label> <?PHP echo($product->barcode != '' ? $product->barcode : 'NA'); ?>
                                    </div>
                                    <div class="">
                                        <label>Manage Stock : </label> <?PHP echo($product->manage_stock != '' ? $product->manage_stock : 'NA'); ?>
                                    </div>
                                    <div class="">
                                        <label>QTY : </label> <?PHP echo($product->current_stock != '' ? $product->current_stock : 'NA'); ?>
                                    </div>
                                    <div class="">
                                        <label>Bin Location : </label> <?PHP echo($product->bin_location != '' ? $product->bin_location : 'NA'); ?>
                                    </div>
                                    <div class="">
                                        <label>Threshold : </label> <?PHP echo($product->threshold != '' ? $product->threshold : 'NA'); ?>
                                    </div>
                                </div>
								<div class="col-md-6">
									<img class="" style="height:80%;width:80%;" src="{{$product->images}}">
                                </div>
                            </div>
                        </div>
						<?php
							if($product->table_name=='product'){
								$table = 1;
							}if($product->table_name=='product_custom'){
								$table = 2;
							}
						?>
                        <!-- /.card-body--> 
                        <div class="card-footer">
                            <a href="{{route('product.index')}}"><button type="button" class="btn btn-primary">Back</button></a>
                            <a href="{{route('product.edit',['id'=> $product->id,'table'=>$table])}}"><button type="button" class="btn btn-success">Edit</button></a>
							<?php if($product->table_name=='product_custom' && $product->delete_status==0){ ?>
								<a href="{{route('product.destroy',['id'=> $product->id,'table'=>$table])}}"><button type="button" class="btn btn-danger">Delete</button></a>
							<?php }?>
							<label class="switch" style="float:right;">
								<input type="checkbox" class="changeStatus" data-id="{{$product->id}}" <?php echo $product->status==0 ? 'checked' : '';?>>
								<span class="slider round"></span>
							</label>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<script>
	//function changeStatus(id, val) {
	$('.changeStatus').change(function() {
		var id = $(this).data('id'); 
		var table_name = $('#table_name').val(); 
		var val = $(this).prop('checked') == true ? 0 : 1; 
		//alert(id);
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			 }
		});
		$.ajax({
			url: "{{url('/changePStatus')}}",
			method: "POST",
			data: {id:id,val:val,table_name:table_name},
			dataType: 'html',
			success: function(response) {
				/*if(response==1){
					if(val==1){
						$('.changeStatus'+id).html("<img src='{{ asset('assets/dist/img/inactive.png') }}' height='25'>").attr('onclick', 'changeStatus(\''+id+'\',\'0\')');
					}else{
						$('.changeStatus'+id).html("<img src='{{ asset('assets/dist/img/active.png') }}' height='25'>").attr('onclick', 'changeStatus(\''+id+'\',\'1\')');
					}
				}*/
			}
		});
	})
</script>
@endsection