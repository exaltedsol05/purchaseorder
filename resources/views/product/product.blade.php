@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Product List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Home</a></li>
              <li class="breadcrumb-item active">Product List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
	<section class="content">
	<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 pb-2 pr-2">
			<a href="{{ route('product.create') }}"><button type="button" class="btn btn-warning">Add</button></a>
		</div>
	</div>
	</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<!-- left column -->
				<div class="col-md-6">
					<!-- general form elements -->
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Fetch Products</h3>
						</div>
						@if (session('msg'))
							<div class="alert alert-{{session('msgType')}}" role="alert">
								{{session('msg')}}
							</div>
						@endif
						<!-- /.card-header -->
						<!-- form start -->
						<form role="form" name="frmclient" method="post" action="{{ route('product.woocomerceData') }}" enctype="multipart/form-data">
						@csrf
							<div class="card-body">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>CSV Link</label>
											<input type="text" class="form-control" name="csv_input_link" value="" placeholder="CSV Link">
										</div>
									</div>
								</div>
								<!--<div class="row" id="sDetails">
								</div>-->
							</div>
							<!-- /.card-body -->
							<div class="card-footer">
								<button type="submit" name="btnSubmit" class="btn btn-primary">Fetch Products</button>
								<a href="{{ route('product.woocomerceVariationData') }}"><button type="button" class="btn btn-success">Auto Fetch Products</button></a>
								<a href="{{ url('public/upload_csv/purchaseorder_product_demo.csv') }}" class="btn btn-danger pull-right"><i class="fas fa-file-download"></i> Demo CSV</a>
							</div>
						</form>
					</div>
					<!-- /.card -->
				</div>
				<div class="col-md-6">
					<!-- general form elements -->
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Search Products</h3>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<select id="find_type" class="form-control" onchange="filterProduct()">
										<option value="">Type of product</option>
										<option value="0">Woocomerce Product</option>
										<option value="1">Custom Product</option>
									</select>
								</div>
								<div class="col-md-6">
									<select id="find_status" class="form-control" onchange="filterProduct()">
										<option value="0">Active</option>
										<option value="1">Inactive</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- /.row -->
		</div><!-- /.container-fluid -->
	</section>
 <!-- Main content -->
    <section class="content">
    <section class="container-fluid">
      <div class="row">
		<div class="col-12">
		<div class="card">
            <!-- /.card-header -->
            <div class="card-body table-responsive" id="getProductList">
				<table id="exampleProduct" class="table table-bordered table-striped">
                <thead>
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Barcode</th>
					<th>SKU</th>
					<th>Brand Name</th>
					<th>Supplier Id</th>
					<th>Purchase Price</th>
					<!--<th style="width:80px">Action</th>-->
                </tr>
                </thead>
                <tbody>
				@foreach($productArr as $product)
				<?php
					if($product->table_name=='product'){
						$table = 1;
					}if($product->table_name=='product_custom'){
						$table = 2;
					}
				?>
				<tr>
					<td><a href="{{route('product.view',['id'=> $product->id,'table'=>$table])}}">{{$product->product_id}}</a></td>
					<td><a class="mHover" data-id="{{$product->product_id}}" data-table="{{$product->table_name}}">{{$product->name}}</a></td>
					<td>{{$product->barcode}}</td>
					<td>{{$product->sku}}</td>
					<td>{{$product->brand_name}}</td>
					<td>
					<?php
						if(in_array($product->supplier_name, $supplierArr)) {
							$sName = array_search($product->supplier_name,$supplierArr);
						}else{
							$sName = $product->supplier_name;
						}
						echo $sName;
					?></td>
					<td>{{$product->purchase_cost}}</td>
					<!--<td style="width:80px">
						<a href="" data-toggle="tooltip" title="Delete" class="delIcon"><i class="fas fa-trash" aria-hidden="true"></i></a>&nbsp;
						<a href="" data-toggle="tooltip" title="Edit" class="editIcon">Edit</a>
						
					</td>-->
                </tr>
				@endforeach
                </tbody>
				</table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    </section>
	<div class="modal fade" id="pDetailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Product Details</h4>
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				</div>
				<div class="modal-body">
					<div class="row" id="pDetailsBody">
						
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- /.content -->
	<script>
		function filterProduct() 
		{
			var find_type = $('#find_type').val();
			var find_status = $('#find_status').val();
			if(find_type!=''){
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					 }
				});
				$.ajax({
					url: "{{url('/filterProduct')}}",
					method: "POST",
					data: {find_type:find_type,find_status:find_status},
					dataType: 'html',
					success: function(response) {
						var obj = jQuery.parseJSON(response);
						$('#getProductList').html(obj.getProductList);
						$("#exampleProduct").DataTable({
							'ordering'    : false,
						});
					}
				});
			}else{
				alert('Select Type of product');
			}
		}
		//$(document).ready(function(){
		$(document).on('click', '.mHover', function () {
			//$('.mHover').click(function() {
				var id = $(this).data('id');
				var table = $(this).data('table');
				//alert(id);
				//alert(table);
				$('#pDetailsModal').modal("show");
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					 }
				});
				$.ajax({
					url: "{{url('/getPDetails')}}",
					method: "POST",
					data: {id:id,table:table},
					dataType: 'html',
					success: function(response) {
						var obj = jQuery.parseJSON(response);
						$('#pDetailsBody').html(obj.pDetailsBody);
					}
				});
			//});
		});
		$('#woocomerceDataBtn').click(function(){
			$('#woocomerceDataBtn').html('<i class="fa fa-spinner fa-spin"></i> Loading').attr('disabled', true);
		});
	</script>
@endsection