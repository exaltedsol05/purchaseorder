@extends('layouts.app')

@section('content')
<style>
.woocomerceDataBtn {
	border: 1px #666666 solid;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	height: 20px;
	font-weight: bold;
	padding-left: 5px;
}
</style>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Settings</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Settings</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Settings</h3>
                    </div>
					@if (session('msg'))
                        <div class="alert alert-{{session('msgType')}}" role="alert">
                            {{session('msg')}}
                        </div>
                    @endif
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmclient" method="post" action="{{ route('home.store') }}" enctype="multipart/form-data">
					@csrf
                        <div class="card-body">
                            <div class="row">
								<div class="col-md-5">
									<div class="row">
										<div class="col-md-12">
											@if($error)
												<h3>Your connection to Xero failed</h3>
												<p>{{ $error }}</p>
												<a href="{{ route('xero.auth.authorize') }}" class="btn btn-success btn-large mb-4">
													Reconnect to Xero
												</a>
											@elseif($connected)
												<h3>You are connected to Xero</h3>
												<p>{{ $organisationName }} via {{ $username }}</p>
												<a href="{{ route('xero.auth.authorize') }}" class="btn btn-success btn-large mb-4">
													Reconnect to Xero
												</a>
											@else
												<h3>You are not connected to Xero</h3>
												<a href="{{ route('xero.auth.authorize') }}" class="btn btn-success btn-large mb-4">
													Connect to Xero
												</a>
											@endif
										</div>
										
										<div class="col-md-12">
											<div class="form-group">
												<label>Xero Theme</label>
												<select name="xero_theme_id" class="form-control" id="xero_theme" onchange="getval(this);">
												<option value="">Select...</option>
												@foreach($result as $k=>$resultVal)
												<option value="{{$resultVal['BrandingThemeID']}}"<?php if($settings->xero_theme_id==$resultVal['BrandingThemeID']){echo 'selected';}?>>{{$resultVal['Name']}}</option>
												@endforeach
												</select>
											</div>
											<input type="hidden" name="xero_theme_name" id="xero_theme_name" value="{{$settings->xero_theme_name}}">
										</div>
										<!--<div class="col-md-12">
											<h3>Load Woocomerce Products</h3>
											<a href="{{ route('product.woocomerceData') }}"><button type="button" id="woocomerceDataBtn" class="btn btn-warning">Load Products</button></a>
										</div>-->
									</div>
                                </div>
                            
                                <div class="col-md-7">
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label>Admin email</label>
												<input type="text" class="form-control" name="admin_email" value="{{$settings->admin_email}}" placeholder="Admin email" required>
											</div>
										</div>
										<div class="col-md-8">
											<div class="form-group">
												<label>CC Email (Comma[,] separated email)</label>
												<input type="text" class="form-control" name="cc_email" value="{{$settings->cc_email}}" placeholder="CC Email" required>
											</div>
										</div>
										<div class="col-md-8">
											<div class="form-group">
												<label>BCC Email (Comma[,] separated email)</label>
												<input type="text" class="form-control" name="bcc_email" value="{{$settings->bcc_email}}" placeholder="CC Email" required>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label>Gst (%)</label>
												<input type="number" class="form-control" name="gst" value="{{$settings->gst}}" placeholder="Gst" required>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                            <!--<div class="row" id="sDetails">
                            </div>-->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" name="btnSubmit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
function getval(sel)
{
    var xero_theme_name = $('#xero_theme option:selected').text();
	$('#xero_theme_name').val(xero_theme_name);
}
$('#woocomerceDataBtn').click(function(){
	$('#woocomerceDataBtn').html('<i class="fa fa-spinner fa-spin"></i> Loading').attr('disabled', true);
});
</script>
@endsection