<style type="text/css" media="all">
	body {
		font-family: 'Roboto', sans-serif;
		-webkit-print-color-adjust: exact;
	}
	.textRight {
		text-align: right;
	}
	.textCenter {
		text-align: center;
	}
	.myHeading {
		background: #f8f8f8;
		color: #000;
	}
	.mySubSection {
		font-size: 12.5px;
		border-collapse: separate;
		padding: 4px;
		border-width: 1px 1px 1px 1px;
		border-color: #000 #000 #000 #000;
		border-style: solid;
	}
	/*.mySubSection:first-child {
		border-left-width: 1px;
	}
	.mySubSection:last-child {
		border-right-width: 1px;
	}*/
	.mySubSectionTwo {
		border-width: 1px;
		border-color: #000 #000 #fff #000;
		border-style: solid;
		padding: 4px;
		font-size: 12.5px;
		text-align: center;
	}
	.mytableOnlyBdr .mySubSectionTwo {
		border-bottom-color: #000;
	}
	.mySubSubsection {
		background: #ececee;
		border-width: 1px 1px 1px 1px;
		border-color: #000 #000 #000 #000;
		border-style: solid;
	}
	.mySubSubsection td {
		border-top: 1px solid #bbb;
		border-bottom: 1px solid #000;
		border-right: 1px solid #000;
		font-size: 13px;
	}
	.mySubPartOnly {
		display: table;
		width: 100%;
		min-height: 100%;
		background-color: red;
	}
	.mySubSectionTwo.mySubSectionTwoRight {
		text-align: right;
	}
	.boldText {
		font-weight: bold;
	}
	.myBodySection {
		width: 100%;
	}
	.myBodySection .mytableOnly {
		background: #ededed;
		border-color: #000;
		border-width: 1px 1px 0 1px;
		border-style: solid;
	}
	/*.myBodySection tr:nth-child(odd) {
		background: #ededed;
	}
	.myBodySection tr:nth-child(even) {
		background: #ececee;
	}*/
	.myTax,
	.myTotal {
		background: #fff !important;
	}
	.myTax.myTaxTopBdr {
		border-top: 2px solid #000;
	}
	.myTax td {
		padding: 5px;
	}
	.myTotal td {
		font-weight: bold;
		font-size: 25px;
		border-width: 2px 0;
		border-color: #000;
		border-style: solid;
		padding: 5px 5px 5px 15px;
	}
	.myTax td:first-child,
	.myTotal td:first-child {
		text-align: right;
	}
	.bdrBottom {
		border-bottom: 2px solid #000;
		padding-bottom: 5px;
	}
	.productImg {
		float: left;
		width: 39%;
	}
	.productNameSl {
		text-align: right;
		font-size: 30px;
		font-weight: bold;
		width: 60%;
	}
	.pNameSlOnly {
		font-size: 20px;
		font-weight: bold;
		width: 225px;
		float: left;
	}
	.fontsize16 {
		font-size: 16px;
	}
	.topThead {
		font-weight: bold;
		float: left;
		margin-right: 15px;
		font-size: 15px;
	}
	.topThead span {
		font-weight: normal;
	}
	.topTheadNext {
		float: left;
		font-size: 15px;
	}
	.topTheadLabel {
		float: left;
		padding: 10px;
		border-width: 0 2px 0 0;
		border-color: #fff;
		border-style: solid;
		background: #f8f8f8;
		padding-left: 15px;
	}
	.topTheadLabel:last-child {
		border-right-width: 0;
	}
	.topTheadLabel:first-child {
		padding-left: 0;
	}
	.topTheadNextLabel {
		float: left;
		padding: 10px;
		border: 2px solid #fff;
		background: #f8f8f8;
	}
	.theadTop {
		border-bottom: 2px solid #000;
		float: left;
		width: 100%;
		padding-bottom: 5px;
	}
	.theadTop tr {
		width: 100%;
		display: block;
	}
	.mytableDetails {
		width: 100%;
		background: #f8f8f8;
		margin-bottom: 10px;
		border-collapse: collapse;
		border: 1px solid #f8f8f8;
	}
	.mytableDetails th {
		padding: 5px;
		border-bottom: 2px solid #fff;
		border-right: 2px solid #fff;
		width: 50%;
	}
	.mytableDetails td {
		padding: 5px;
		border-right: 2px solid #fff;
		vertical-align: top;
	}
	.mytableDetails th.rightBdrNone,
	.mytableDetails td.rightBdrNone {
		border-right: 0;
	}
	.mytableDetails td {
		position: realtive;
	}
	.mytableDetails td b {
		/* height: 8px;
		width: 8px;
		background-color: #020202;
		border-radius: 10%;
		-moz-border-radius: 10%;
		-webkit-border-radius: 10%;
		display: inline-block;
		margin-right: 5px;
		margin-top: 5px;
		vertical-align: middle; */
		width: 8px;
		height: 8px;
		line-height: 8px;
		border-radius: 50%;
		-moz-border-radius: 50%;
		-webkit-border-radius: 50%;
		text-align: center;
		color: #000;
		font-size: 16px;
		text-transform: uppercase;
		font-weight: 700;
		margin: 0 auto 40px;
	}
	.mytableDetails td ul {
		padding-left: 15px;
	   list-style-type: none;
	   margin: 0;
	}
	.mytableDetails td ul li {
	   position: relative;
	   margin-bottom: 5px;
	}
	.mytableDetails td ul li:last-child {
	   margin-bottom: 0;
	}
	.mytableDetails td ul li:before {
		content: "•";
		font-size: 35px;
		position: absolute;
		top: -12px;
		left: -15px;	   
	}
	
	@page { margin: 80px 50px 80px 50px;; }
    #header { position: fixed; left: 0px; top: -80px; right: 0px; height: 80px; text-align: center;}
    #footer { position: fixed; left: 0px; bottom: -80px; right: 0px; height: 150px;}
    #footer .page:after { content: counter(page, upper-roman); }
	
	/*@page { margin: 80px 50px; }
    #header { position: fixed; left: 0px; top: -80px; right: 0px; height: 80px; text-align: center;}
    #footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 80px;}
    #footer .page:after { content: counter(page, upper-roman); }*/
</style>
<body>
<div id="header">
	<table style="width: 100%;padding: 5px 0;border-bottom: 2px solid #000;">	
		<tr  style="float: left;width: 100%;padding-bottom: 5px;">
			<td class="productImg" style="float: left;"><img src="{{url('public/images').'/'.$company->logo}}"></td>
			<td class="productNameSl" style="float: right;text-align:right;">PO no : <span>{{$po->ref_no}}</span></td>
		</tr>
	
	</table>
</div>
<div id="footer">
<?php $pdf_footer = str_replace(array("[PO]"), array($po->ref_no), $company->pdf_footer); ?>
{!!$pdf_footer!!}
    <p class="page"><a href="ibmphp.blogspot.com"></a></p>
</div>

<div id="content">
<table style="width: 100%;padding: 5px 0;">
    <tbody>
		<tr>
			<td class="pNameSlOnly">
			<?php
			if($po->order_status==1){
				echo 'DRAFT';
			}else if($po->order_status==2){
				echo '';
			}else if($po->order_status==3){
				echo 'RECEIVED';
			}else if($po->order_status==4){
				echo 'PARTIAL WAITING';
			}else if($po->order_status==5){
				echo 'CANCELED';
			}
			?>
			</td>
			<!-- <td class="productNameSl">PO no :<span>{{$po->ref_no}}</span></td> -->
		</tr>
    </tbody>
    <tbody>
		<tr>
			<td class="topThead">Order Details</td>
		</tr>
		<!-- <tr>
			<td class="topThead">PO no : </td>
			<td>{{$po->ref_no}}</td>
		</tr> -->
		<tr>
			<td class="topThead">Supplier Name : <span>{{$po->sName}}</span></td>
			<!-- <td class="topTheadNext">{{$po->sName}}</td> -->
		</tr>
		<tr>
			<td class="topThead">Delivery Address : <span>{{$po->delivery_address}}</span></td>
			<!-- <td class="topTheadNext">{{$po->delivery_address}}</td> -->
		</tr>
		<?php if($po->note!=''){?>
		<tr>
			<td class="topThead">Note : <span>{{$po->note}}</span></td>
			<!-- <td class="topTheadNext">{{$po->note}}</td> -->
		</tr>
		<?php }?>
    </tbody>
</table>
<table class="mytableDetails">
	<tr>
		<th>Received</th>
		<th class="rightBdrNone">Remaining</th>
	</tr>
	<tr>
		<td>
			<ul>
				<li>0001</li>
				<li>0002</li>
				<li>0003</li>
				<li>0004</li>
			</ul>
		</td>
		<td class="rightBdrNone">
			<ul>
				<li>0001</li>
			</ul>
		</td>
	</tr>
</table>
<table style="width: 100%;border-collapse: collapse;border-top: 0;margin-bottom: 10px;">	
    <thead>
		<tr>
			<th class="topTheadLabel">Create Date : {{date('d/m/Y',strtotime($po->current_date))}}</th>
			<th class="topTheadLabel">Due Date : {{date('d/m/Y',strtotime($po->due_date))}}</th>
		</tr>
	</thead>
</table>
<table style="width: 100%;border-collapse: collapse;border-top: 0;">
    <thead>
      <tr class="myHeading">
        <th class="mySubSection" style="border-right: 1px solid #fff;">Sr No</th>
        <th class="mySubSection" style="border-right: 1px solid #fff;">Image</th>
        <th class="mySubSection" style="border-right: 1px solid #fff;">Name</th>
        <th class="mySubSection" style="border-right: 1px solid #fff;">Supplier Code</th>
        <th class="mySubSection" style="border-right: 1px solid #fff;">Brand Name</th>
        <th class="mySubSection" style="border-right: 1px solid #fff;">Barcode</th>
        <th class="mySubSection" style="border-right: 1px solid #fff;">Uom</th>
        <th class="mySubSection" style="border-right: 1px solid #fff;">Quantity</th>
        <th class="mySubSection" style="border-right: 1px solid #fff;">Cost</th>
        <th class="mySubSection">Price</th>
      </tr>
    </thead>
	<tbody class="myBodySection">
		<?php 
			$item_total = 0;
			$cost_total = 0;
			$i= 1;
			$subTotal = 0;
		?>
		@foreach($poArr as $poArrVal)
		<tr class="mytableOnly <?php if($poArrVal->comment==''){echo 'mytableOnlyBdr';}?>">
			<td <?php if($poArrVal->comment!=''){echo 'rowspan="2"';}?> class="mySubSectionTwo" style="border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #fff;">{{$i}}</td>
			<td <?php if($poArrVal->comment!=''){echo 'rowspan="2"';}?> class="mySubSectionTwo" style="border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #fff;"><img style="height:50px;width:50px;" src="{{$poArrVal->images}}"></td>
			<td class="mySubSectionTwo boldText" style="border-right: 1px solid #fff;">{{$poArrVal->product_name}}<?php if ($poArrVal->sku!='') { ?>({{$poArrVal->sku}})<?php } ?></td>
			<td class="mySubSectionTwo" style="border-right: 1px solid #fff;">{{$poArrVal->supplier_code}}</td>
			<td class="mySubSectionTwo" style="border-right: 1px solid #fff;">{{$poArrVal->brand_name}}</td>
			<td class="mySubSectionTwo" style="border-right: 1px solid #fff;">{{$poArrVal->barcode}}</td>
			<td class="mySubSectionTwo" style="border-right: 1px solid #fff;">{{$poArrVal->uom}}</td>
			<td class="mySubSectionTwo" style="border-right: 1px solid #fff;">{{$poArrVal->quantity}}</td>
			<td class="mySubSectionTwo mySubSectionTwoRight" style="border-right: 1px solid #fff;">${{$poArrVal->cost}}</td>
			<td class="mySubSectionTwo mySubSectionTwoRight">${{$poArrVal->quantity*$poArrVal->cost}}</td>
			<?php if($poArrVal->comment!='') {?>
			<tr class="mySubSubsection">
				<td colspan="8" class="textCenter">Comment</td>
			</tr>
			<?php } ?>
		</tr>
		<?php 
			if($poArrVal->pType==0){
				$item_total = $item_total+($poArrVal->quantity*$poArrVal->cost);
			}
			if($poArrVal->pType==1){
				$cost_total = $cost_total+($poArrVal->quantity*$poArrVal->cost);
			}
			$i++;
			$subTotal = $subTotal+($poArrVal->quantity*$poArrVal->cost);
		?>
		@endforeach
		<!--<tr class="mytableOnly mytableOnlyBdr" style="width: 100%;">
			<td class="mySubSectionTwo" style="border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #fff;">5</td>
			<td class="mySubSectionTwo" style="border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #fff;"><img style="height:50px;width:50px;" src="https://dev.nappies.co.nz/wp-content/uploads/2021/01/GR550S010_zoom__47510.1602613140.jpg"></td>
			<td class="mySubSectionTwo boldText" style="border-right: 1px solid #fff;">Purchase Order Purchase Order</td>
			<td class="mySubSectionTwo" style="border-right: 1px solid #fff;">GR550S010XXXXX999</td>
			<td class="mySubSectionTwo" style="border-right: 1px solid #fff;">The Gro Company Test</td>
			<td class="mySubSectionTwo" style="border-right: 1px solid #fff;">1234567890</td>
			<td class="mySubSectionTwo" style="border-right: 1px solid #fff;"></td>
			<td class="mySubSectionTwo" style="border-right: 1px solid #fff;">15</td>
			<td class="mySubSectionTwo mySubSectionTwoRight" style="border-right: 1px solid #fff;">$150</td>
			<td class="mySubSectionTwo mySubSectionTwoRight">$250</td>
			
		</tr>-->
		<tr class="myTax myTaxTopBdr">
			<td colspan="10" class="textRight">Items Subtotal : $ {{number_format($item_total,2)}}</td>
		</tr>
		<tr class="myTax">
			<td colspan="10" class="textRight">Extra cost Subtotal : $ {{number_format($cost_total,2)}}</td>
		</tr>
		<tr class="myTax">
			<td colspan="10" class="textRight">Tax : $ {{number_format($po->tax,2)}}</td>
		</tr>
		<tr class="myTotal">
			<td colspan="10" class="textRight">Total amount : $ {{number_format($po->tot_amt+$po->tax,2)}}</td>
		</tr>
    </tbody>
</table>
</div>
</body>