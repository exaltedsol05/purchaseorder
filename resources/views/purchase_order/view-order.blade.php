@extends('layouts.app')

@section('content')
<style>
.modal {
  overflow-y:auto;
}
</style>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">View Order</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="">View Order</a></li>
                    <li class="breadcrumb-item active">View Order</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">View Order</h3>
                    </div>
					@if (session('msg'))
                        <div class="alert alert-{{session('msgType')}}" role="alert">
                            {{session('msg')}}
                        </div>
                    @endif
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmclient" method="post" action="{{ route('order.duplicate') }}" enctype="multipart/form-data">
					@csrf
                        <div class="card-body">
                            <div class="row">
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Ref No : </label> {{$po->ref_no}}
										<input type="hidden" id="ref_no" name="ref_no" value="{{$po->ref_no}}">
										<input type="hidden" name="t_id" value="{{$po->id}}">
                                    </div>
                                </div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Supplier name : </label> {{$po->sName}}
										<input type="hidden" id="supplier_id" value="{{$po->supplier_id}}">
									</div>
								</div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Created date : </label> {{date('d/m/Y',strtotime($po->current_date))}}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Due date : </label> {{date('d/m/Y',strtotime($po->due_date))}}
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Delivery address : </label> {{$po->delivery_address}}
                                    </div>
                                </div>
								<?php if($po->note!=''){?>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Note : </label> {{$po->note}}
                                    </div>
                                </div>
								<?php }?>
								<?php 
								if(sizeof($related_ref))
								{
								?>
								<div class="col-md-12">
                                    <div class="form-group">
                                        <label>Related Orders : </label>
										<ul class="pl-3">
											<?php
											foreach($related_ref as $related_ref_val)
											{
											?>
												<li><a href="{{route('order.view',[$related_ref_val->id])}}">{{$related_ref_val->ref_no}}</a></li>
											<?php
											}
											?>
										</ul>
                                    </div>
                                </div>
								<?php } ?>
                            </div>
                        </div>
						<div class="card">
							<!-- /.card-header -->
							<div class="card-body table-responsive orderViewTable">
								<table id="example2" class="table table-bordered table-striped">
								<thead>
								<tr>
									<th>Sr No</th>
									<th>Image</th>
									<th>Name</th>
									<th>Supplier Code</th>
									<th>Brand Name</th>
									<th>Barcode</th>
									<th>Uom</th>
									<th>Quantity</th>
									<th>Cost</th>
									<th>Total Price</th>
									<!--<th>Comment</th>-->
								</tr>
								</thead>
								<tbody>
								<?php
									$item_total = 0;
									$cost_total = 0;
									$i = 1;
									$subTotal = 0;
								?>
								@foreach($poArr as $poArrVal)
								<tr id="dataRow{{$poArrVal->id}}">
									<td>{{$i}}</td>
									<td class="positionRelative"><div style="height:50px;width:50px;"><?php if($poArrVal->images!=''){ ?><a href="{{$poArrVal->images}}" target="_blank"><img class="" style="height:50px;width:50px;" src="{{$poArrVal->images}}"><?php } ?></div></td>
									<td>{{$poArrVal->product_name}}<?php if ($poArrVal->sku!='') { ?>({{$poArrVal->sku}})<?php } ?></td>
									<td>{{$poArrVal->supplier_code}}</td>
									<td>{{$poArrVal->brand_name}}</td>
									<td>{{$poArrVal->barcode}}</td>
									<td>{{$poArrVal->uom}}</td>
									<td>{{$poArrVal->quantity}}</td>
									<td>${{$poArrVal->cost}}</td>
									<td>${{$poArrVal->quantity*$poArrVal->cost}}</td>
									<!--<td>{{$poArrVal->comment}}</td>-->
								</tr>
								<?php 
									if($poArrVal->pType==0){
										$item_total = $item_total+($poArrVal->quantity*$poArrVal->cost);
									}
									if($poArrVal->pType==1){
										$cost_total = $cost_total+($poArrVal->quantity*$poArrVal->cost);
									}
									$i++;
									$subTotal = $subTotal+($poArrVal->quantity*$poArrVal->cost);
								?>
								@endforeach
								
								</tbody>
								</table>
							</div>
							<div class="card-body pt-0">
								<div class="row">
									<div class="col-md-12">
										<div class="text-right"><span>Items Subtotal : $ <span id="item_total">{{number_format($item_total,2)}}</span></span></div>		
										<div class="text-right"><span>Extra cost Subtotal : $ <span id="cost_total">{{number_format($cost_total,2)}}</span></span></div>
										<!--<div class="text-right"><span>Sub Total : $<span class="sub_total"></span></span></div>-->
										<div class="text-right"><span>Tax : $ <span class="tax">{{number_format($po->tax,2)}}</span></span><input type="hidden" id="tax" name="tax" value="{{$po->tax}}"></div>
										
										<div class="text-right" style="margin-top: 15px;">
											<strong style="font-size:25px;border-width: 2px 0;border-color: #000;border-style: solid;padding: 5px 5px 5px 15px;">Total amount : $ <span class="tot_amt">{{number_format($po->tot_amt+$po->tax,2)}}</span></strong>
											<input type="hidden" id="tot_amt" value="{{$po->tot_amt}}" name="tot_amt">
										</div>			
									</div>
								</div>
								<!--<div class="text-right">
									<strong>Sub Total : ${{$subTotal}}</strong>
								</div>
								<div class="text-right">
									<strong>Tax : ${{$subTotal*$settings->gst/100}}</strong>
								</div>
								<div class="text-right">
									<strong>Total : ${{$subTotal+($subTotal*$settings->gst/100)}}</strong>
								</div>-->
							</div>
							<!-- /.card-body -->
						</div>
                        <!-- /.card-body -->
                        <div class="card-footer">
							<a href="{{ url()->previous() }}"><button type="button" class="btn btn-info">Back</button></a>
                            <button type="submit" name="action" class="btn btn-primary" value="clone">Clone</button>
                            <button type="submit" name="action" class="btn btn-warning" value="pdf">Print</button>
							<?php if($po->order_status!=5) { ?>
                            <button type="button" id="emailBtn" class="btn btn-success">Email</button>
							<?php } if(!in_array($po->order_status, array(3,5))) { ?>
                            <button type="submit" name="action" class="btn btn-danger" value="cancel">Cancel</button>
							<?php } if($po->order_status==2 || $po->order_status==4) { ?>
							<a href="{{route('po.prepare_order',[$po->id])}}"><button type="button" class="btn btn-info">Prepare</button></a>
							<?php } if($po->order_status==1) { ?>
							<a href="javascript:void(0)" class="sentToPlaced" data-id="{{$po->id}}" style="float:right;"><button type="button" class="btn btn-info">Mark as Placed</button></a>
							<?php } ?>
                        </div>
						<div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="myModalLabel">Order Sent</h4>
										<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>Email Subject</label>
													<select name="email_subject" class="form-control" id="email_subject" onchange="GetsDetails()">
													<option value="">Select...</option>
													@foreach($email_management as $eManagement)
													<option value="{{$eManagement->emailSubject}}">{{$eManagement->emailSubject}}</option>
													@endforeach
													</select>
												</div>
											</div>
										</div>
										<div class="row" id="sMailDetails">
											
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>Sender Mail</label>
													<input type="text" class="form-control mb-1" name="admin_email" value="{{$settings->admin_email}}" placeholder="Supplier Email" required>
												</div>
											</div>
											<div class="col-md-12">
												<input type="checkbox" class="" name="sent_cc" value="1"> <label>Send me as copy</label>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="submit" name="action" id="emailSent" class="btn btn-success" value="email">Sent</button>
										<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="myModalLabel1">Order Sent</h4>
										<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									</div>
									<div class="modal-body">
										<strong>Mark as Placed without sending email?</strong>
										<div class="row mt-1">
										<div class="col-sm-6">
											<!-- radio -->
											<div class="form-group clearfix">
											  <div class="icheck-primary d-inline">
												<input type="radio" id="yes" name="pOrder" value="yes">
												<label for="yes">Yes
												</label>
											  </div>
											  <div class="icheck-primary d-inline">
												<input type="radio" id="no" name="pOrder" value="no">
												<label for="no">No
												</label>
											  </div>
											  <div id="oNote" style="color: red;"></div>
											</div>
										</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="submit" name="action" class="btn btn-success" id="sent" value="sentNoEmail">Sent</button>
										<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</div>
						</div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
function GetsDetails() 
{
	var supplier_id = $('#supplier_id').val();
	var ref_no = $('#ref_no').val();
	var delivery_address = $('#delivery_address').val();
	var email_subject = $('#email_subject').val();
	//alert(supplier_id);
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 }
	});
	$.ajax({
		url: "{{url('/getSDetails')}}",
		method: "POST",
		data: {supplier_id:supplier_id,ref_no:ref_no,delivery_address:delivery_address,email_subject:email_subject},
		dataType: 'html',
		success: function(response) {
			var obj = jQuery.parseJSON(response);
			$('#sMailDetails').html(obj.sMailDetails);
			$("#emailSent").prop("disabled", false);
			
			CKEDITOR.replace( 'email_pre_body', {
				allowedContent:true,
			});
		}
	});
	//console.log($("meta[name='csrf-token']").attr('content'));
}
$(document).ready(function(){
	$("#emailSent").prop("disabled", true);
	$("#emailBtn").click(function () {
        $('#emailModal').modal("show");
	});
	$(".sentToPlaced").click(function () {
        $('#confirmModal').modal("show");
	});
	//$("#sent").click(function (e) {
	$("#sent").click(function (e) {
		if ($('input[name="pOrder"]:checked').length == 0){
			e.preventDefault();
			
			$( "#oNote" ).text( "Please choose yes or no" ).show().fadeOut(10000);
			return false;
		}else if($('input[name="pOrder"]:checked').val()=='no'){
			e.preventDefault();
			$('#confirmModal').modal("hide");
			$('#emailModal').modal("show");
		} else {
			return true;
		}
	});
});
</script>
@endsection