@extends('layouts.app')

@section('content')
<style>
/*.modal {
  display: none;
  position: fixed;
  padding-top: 100px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgb(0,0,0);
  background-color: rgba(0,0,0,0.9);
}
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}
.modal-content {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}
.imageclose {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.imageclose:hover,
.imageclose:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}*/
</style>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Create Order</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="">Purchase Order</a></li>
                    <li class="breadcrumb-item active">Create Order</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Purchase Order</h3>
                    </div>
					@if (session('msg'))
                        <div class="alert alert-{{session('msgType')}}" role="alert">
                            {{session('msg')}}
                        </div>
                    @endif
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmclient" id="frm" method="post" action="{{ route('order.store') }}" enctype="multipart/form-data">
					@csrf
                        <div class="card-body">
                            <div class="row">
								<div class="col-md-6">
									<div class="row">
										<!--<div class="col-md-3">
											<div class="form-group">
												<label>PO No</label>
												<input type="text" class="form-control" name="po_no" placeholder="PO No" required>
											</div>
										</div>-->
										<div class="col-md-12">
											<div class="form-group">
												<label>Supplier name</label>
												<select name="supplier_id" id="supplier_id" class="form-control select2" onchange="GetsDetails()" required tabindex="1">
													<option value="">Select...</option>
													@foreach($supplierArr as $supplierVal)
													<option value="{{$supplierVal->id}}">({{$supplierVal->supplier_code}}) {{$supplierVal->name}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Create date</label>
												<input type="text" class="form-control" id="datepicker" name="current_date" value="{{date('d/m/Y')}}" placeholder="Due Date" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Due date</label>
												<input type="text" class="form-control" id="dueDate" name="due_date" value="{{date('d/m/Y', strtotime('+1 day'))}}" placeholder="Due Date" required>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label>Delivery address</label>
												<!--<textarea class="form-control" name="delivery_address" placeholder="Delivery Address" required></textarea>-->
												<select name="delivery_address" id="delivery_address" class="form-control" required tabindex="1">
													<option value="">Select...</option>
													@foreach($delivery_address as $delivery_addressVal)
													<option value="{{$delivery_addressVal->delivery_address}}" <?PHP echo($delivery_addressVal->is_default == 1? 'selected' : ''); ?>>{{$delivery_addressVal->delivery_address}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label>Note</label>
												<textarea  class="form-control" name="note" placeholder="Note"></textarea>
											</div>
										</div>
									</div>
                                </div>
								<div class="col-md-6">
									<div class="row" id="sDetails">
									</div>
								</div>
                            </div>
                        </div>
						<div class="card-footer">
							<div class="row">
								<!--<div class="col-md-3">
									<div class="form-group">
										<input type="text" class="form-control search" id="search">
										<div class="suggesstion-box" id="suggesstion"></div>
									</div>
								</div>-->
								<div class="col-md-6">
									<div class="form-group">
										<input list="browsers" name="search" id="search" class="custom-select custom-select-md">
										<datalist id="browsers">
											
										</datalist>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-11">
									<div class="row">
										<div class="col95">
											<div class="row">
												<div class="col5">
													<label>Image</label>
												</div>
												<div class="col30">
													<label>Name</label>
												</div>
												<div class="col15">
													<label>Supplier Code</label>
												</div>
												<div class="col10">
													<label>Barcode</label>
												</div>
												<div class="col10">
													<label>Threshold</label>
												</div>
												<div class="col10">
													<label>Cur. stock</label>
												</div>
												<div class="col10">
													<label>Qty</label>
												</div>
												<div class="col10">
													<label>Cost ($)</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-11">
									<div class="row">
										<div class="col95">
											<div class="row" id="SearchProduct">
											</div>
										</div>
										<div class="col5 text-right">
											<img src="https://po.nappies.co.nz/assets/dist/img/inactive.png" id="removeOrderBtn" onclick="removeOrderBtn()" style="display:none;" height="25">
										</div>
									</div>
								</div>
								<div class="col-md-1"><button type="button" name="btnSubmit" id='addOrderButton' class="btn btn-success hidden-btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add</button>
								</div>
							</div>
						</div>
						<div class="card-body" id="">
							<h3>List Product</h3>
							<div class="row">
								<div class="col5">
									<div class="form-group">
										<label>Sr. No</label>
									</div>
								</div>
								<div class="col5">
									<div class="form-group">
										<label>Image</label>
									</div>
								</div>
								<div class="col30">
									<div class="form-group">
										<label>Name</label>
									</div>
								</div>
								<div class="col15">
									<div class="form-group">
										<label>Supplier Code</label>
									</div>
								</div>
								<div class="col10">
									<div class="form-group">
										<label>Barcode</label>
									</div>
								</div>
								<div class="col5">
									<div class="form-group">
										<label>Uom</label>
									</div>
								</div>
								<div class="col10">
									<div class="form-group">
										<label>Qty</label>
									</div>
								</div>
								<div class="col10">
									<div class="form-group">
										<label>Cost ($)</label>
									</div>
								</div>
								<div class="col10">
									<div class="form-group">
										<label>Total ($)</label>
									</div>
								</div>
								<!--<div class="col80">
									<div class="form-group">
										<label>Comment</label>
									</div>
								</div>-->
							</div>
							<div id="tRow">
							</div>
							<div id="sRow">
							</div>
							<div class="row">
								<div class="col-md-11">
									<div class="text-right"><span>Items Subtotal : $ <span id="item_total">0</span></span></div>			
									<div class="text-right"><span>Extra cost Subtotal : $ <span id="cost_total">0</span></span></div>			
									<!--<div class="text-right"><span>Sub Total : $<span class="sub_total">0</span></span></div>-->		
									<div class="text-right"><span>Tax : $ <span class="tax">0</span></span><input type="hidden" id="tax" name="tax"></div>			
									<div class="text-right" style="margin-top: 15px;">
										<strong style="font-size:25px;border-width: 2px 0;border-color: #000;border-style: solid;padding: 5px 5px 5px 15px;">Total amount : $ <span class="tot_amt">0</span></strong>
										<input type="hidden" id="tot_amt" name="tot_amt">
									</div>			
								</div>
							</div>
						</div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" name="action" class="btn btn-primary chkmin" value="save">SAVE as a DRAFT</button>
							<button type="submit" name="action" class="btn btn-warning chkmin" value="pdf">Print</button>
                            <button type="submit" name="action" class="btn btn-success chkmin" value="email">Email</button>
                        </div>
						<div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="myModalLabel">Order Sent</h4>
										<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>Email subject</label>
													<input type="text" class="form-control" name="email_pre_subject" value="{{$settings->email_pre_subject}}" placeholder="Email subject pre field" required>
												</div>
											</div>
										</div>
										<div class="row" id="sMailDetails">
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>Sender Mail</label>
													<input type="text" class="form-control mb-1" name="admin_email" value="{{$settings->admin_email}}" placeholder="Supplier Email" required>
												</div>
											</div>
											<div class="col-md-12">
												<input type="checkbox" class="" name="sent_cc" value="1"> <label>Send me as copy</label>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="submit" name="action" class="btn btn-success chkmin" value="email">Sent</button>
										<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</div>
						</div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
	<!-- The Modal -->
<div id="imageModal" class="modal">
  <span class="imageclose" onclick="close_image()">&times;</span>
  <img class="modal-content" onclick="close_image()" id="img01">
</div>
</section>    
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>  -->

<script>
$("#frm").submit(function(event) {
	$('#SearchProduct').html('');
	totalAmount();
	return true;
});
function GetsDetails() 
{
	var supplier_id = $('#supplier_id').val();
	//alert(supplier_id);
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		 }
	});
	$.ajax({
		url: "{{url('/getSDetails')}}",
		method: "POST",
		data: {supplier_id:supplier_id},
		dataType: 'html',
		success: function(response) {
			var obj = jQuery.parseJSON(response);
			$('#sDetails').html(obj.sDetails);
			$('#sRow').html(obj.sCost);
			$('#sMailDetails').html(obj.sMailDetails);
			
			totalAmount();
			/*CKEDITOR.replace( 'mailBody', {
				allowedContent:true,
			});*/
		}
	});
	//console.log($("meta[name='csrf-token']").attr('content'));
}
var i;
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
	checkMinimumProduct();
	$("#emailBtn").click(function () {
        $('#emailModal').modal("show");
	});
	$("#addOrderButton").prop("disabled", true);
	$("#removeOrderBtn").hide();
	var i=1;
	$("#addOrderButton").click(function () {
		var withOutPro = $('.product_id').map(function() { 
			return this.value; 
		}).get().join(',');
		//alert(withOutPro);
		var id = $('#search_product_id').val();
		var hasProduct = withOutPro.indexOf(id) != -1;
		if(hasProduct==false){
			var SearchProduct = $('#SearchProduct').html();
			var search_quantity = $('#search_quantity').val();
			var search_total_amount = $('#search_total_amount').val();
			if(SearchProduct!=''){
				var p_id = $('#search_product_id').val();
				var p_table = $('#search_product_table').val();
				$.ajax({
					url: "{{url('/getProductList')}}",
					method: "POST",
					data: {id:p_id,p_table:p_table,rowId:i},
					dataType: 'html',
					success: function(response) {
						var obj = jQuery.parseJSON(response);
							
						$('#tRow').append('<div id="dataRow'+i+'" class="row"><div class="col5">'+i+'</div>'+obj.pDetails+''+obj.sTotal+'<div class="col95"><div class="form-group"><input type="text" class="form-control" name="comment[]" placeholder="Comment"></div></div><div class="col5"><button onclick="remRow('+i+')" type="button" name="btnSubmit" class="btn btn-danger pull-right btn_remove"><i class="fa fa-trash" style="color:#fff"></i></button></div></div>');
						$('#quantity'+i).val(search_quantity);
						$('#total_amount'+i).val(search_total_amount);
						totalAmount();
						i++;
						window.siteRoot = i;
						$('#SearchProduct').html('');
						$("#addOrderButton").prop("disabled", true);
						$("#removeOrderBtn").hide();
						$( "#search").focus();
					}
				});
			}
		}else{
			alert('This product already added in the list. Please choose another product');
		}
	});
	$(document).keydown(function(k) {
		if(k.which == 16 || k.which == 13 || k.which == 17) { 
			var withOutPro = $('.product_id').map(function() { 
				return this.value; 
			}).get().join(',');
			var id = $('#search_product_id').val();
			var hasProduct = withOutPro.indexOf(id) != -1;
			if(hasProduct==false){
				var SearchProduct = $('#SearchProduct').html();
				var search_quantity = $('#search_quantity').val();
				var search_total_amount = $('#search_total_amount').val();
				if(SearchProduct!=''){
					var p_id = $('#search_product_id').val();
					var p_table = $('#search_product_table').val();
					$.ajax({
						url: "{{url('/getProductList')}}",
						method: "POST",
						data: {id:p_id,p_table:p_table,rowId:i},
						dataType: 'html',
						success: function(response) {
							var obj = jQuery.parseJSON(response);
								
							$('#tRow').append('<div id="dataRow'+i+'" class="row"><div class="col5">'+i+'</div>'+obj.pDetails+''+obj.sTotal+'<div class="col95"><div class="form-group"><input type="text" class="form-control" name="comment[]" placeholder="Comment"></div></div><div class="col5"><button onclick="remRow('+i+')" type="button" name="btnSubmit" class="btn btn-danger pull-right btn_remove"><i class="fa fa-trash" style="color:#fff"></i></button></div></div>');
							$('#quantity'+i).val(search_quantity);
							$('#total_amount'+i).val(search_total_amount);
							totalAmount();
							i++;
							window.siteRoot = i;
							$('#SearchProduct').html('');
							$("#addOrderButton").prop("disabled", true);
							$("#removeOrderBtn").hide();
							$( "#search").focus();
						}
					});
				}
			}else{
				alert('This product already added in the list. Please choose another product');
			}
		}
	})
	window.siteRoot = i;
	/*$( "#search" ).autocomplete({
        source: function( request, response ) {
          // Fetch data
          $.ajax({
            url:"{{url('/searchProduct')}}",
            type: 'post',
            dataType: "json",
            data: {
               _token: CSRF_TOKEN,
               search: request.term
            },
            success: function( data ) {
               response( data );
            }
          });
        },
        select: function (event, ui) {
           // Set selection
           $('#search').val(ui.item.label); // display the selected text
           //$('#employeeid').val(ui.item.value); // save selected id to input
           return false;
        }
     });*/
	/*$(document).on('keyup', "#search", function() {
		var searchVal = $(this).val();
		if(searchVal!=''){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url: "{{url('/searchProduct')}}",
				method: "POST",
				data: {searchVal:searchVal},
				dataType: 'html',
				success: function(response) {
					//var obj = jQuery.parseJSON(response);
					//alert(response);
					$("#suggesstion").show().html(response);
				}
			});
		}
	});*/
	$(document).on('keyup', "#search", function(h) {
		if(h.which == 38 || h.which == 40 || h.which == 13){
			//alert(11);
		}else{
			var searchVal = $(this).val();
			if(searchVal!=''){
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url: "{{url('/searchProduct')}}",
					method: "POST",
					data: {searchVal:searchVal},
					dataType: 'html',
					success: function(response) {
						//var obj = jQuery.parseJSON(response);
						//alert(response);
						$("#browsers").html(response);
					}
				});
			}			
		}
	});
	$("#search").on('input', function () {
		var val = this.value;
		if($('#browsers option').filter(function(){
			return this.value.toUpperCase() === val.toUpperCase();        
		}).length) {
			//send ajax request
			//alert(this.value);
			var data = this.value;
			var arr = data.split('|');
			var rowId = window.siteRoot;
			var id = arr[0];
			var table_name = arr[1];
			$.ajax({
				url: "{{url('/getProduct')}}",
				method: "POST",
				data: {id:id,rowId:rowId,table_name:table_name},
				dataType: 'html',
				success: function(response) {
					var obj = jQuery.parseJSON(response);
					//alert(response);
					$("#SearchProduct").html(obj.pDetails);
					$("#suggesstion").hide();
					$("#search").val('');
					$("#addOrderButton").prop("disabled", false);
					$("#removeOrderBtn").show();
					
					$( "#search_quantity").focus();
				}
			});
		}
	});
	/*var path = "{{ url('searchProduct') }}";
    $('input.search').typeahead({
        source:  function (query, process) {
        return $.get(path, { searchVal: query }, function (data) {
                return process(data);
            });
        }
    });*/
});
/*function selectProduct(id,table_name) {
	var rowId = window.siteRoot;
	$.ajax({
		url: "{{url('/getProduct')}}",
		method: "POST",
		data: {id:id,rowId:rowId,table_name:table_name},
		dataType: 'html',
		success: function(response) {
			var obj = jQuery.parseJSON(response);
			//alert(response);
			$("#SearchProduct").html(obj.pDetails);
			$("#suggesstion").hide();
			$("#search").val('');
			$("#addOrderButton").prop("disabled", false);
			$("#removeOrderBtn").show();
			
			$( "#search_quantity").focus();
		}
	});
}*/
$(document).on('focus', '.cost,.quantity,.uom', function() {
	var inputId = $(this).attr("id");
	var c = inputId.replace ( /[^\d.]/g, '' );
	//alert(c);
	$(document).on('keyup', "#cost"+c+",#quantity"+c+"", function() {
		/*var thisVal = $(this).val();
		$(this).attr('value', thisVal);*/
		var quantity = $("#quantity"+c).val();
		var cost = $("#cost"+c).val();
		var tAmount = (quantity*cost);
		$("#total_amount"+c).val(tAmount.toFixed(2));
		totalAmount();
	});
	$(document).on('keyup', "#uom"+c+"", function() {
		/*var thisVal = $(this).val();
		$(this).attr('value', thisVal);*/
	});
});
$(document).on('keyup', "#search_quantity,#search_cost", function() {
	var search_quantity = $("#search_quantity").val();
	var search_cost = $("#search_cost").val();
	var search_total_amount = (search_quantity*search_cost);
	$("#search_total_amount").val(search_total_amount.toFixed(2));
});
$(document).on('keyup', ".purchaseCost", function() {
	totalAmount();
});
function remRow(c) {
	$("#dataRow" + c).remove();
	totalAmount();
}
function eCostRow(g) {
	//alert(11);
	$("#rowCost" + g).remove();
	totalAmount();
}
function removeOrderBtn() {
	$('#SearchProduct').html('');
	$("#addOrderButton").prop("disabled", true);
	$( "#search").focus();
	$("#removeOrderBtn").hide();
	totalAmount();
}
function totalAmount()
{
	var sum = 0;
	$(".total_amount").each(function(){
		sum += +$(this).val();
	});
	
	var item_total = 0;
	$(".item_total").each(function(){
		item_total += +$(this).val();
	});
	$("#item_total").html((item_total).toFixed(2));
	var cost_total = 0;
	$(".cost_total").each(function(){
		cost_total += +$(this).val();
	});
	$("#cost_total").html((cost_total).toFixed(2));
	//alert(sum);
	
	var tax = sum*<?php echo $settings->gst/100;?>;
	$(".tax").html(tax.toFixed(2));
	$("#tax").val(tax.toFixed(2));
	
	//$(".tot_amt").html(sum.toFixed(2));
	//$(".sub_total").html((sum).toFixed(2));
	$(".tot_amt").html((sum+tax).toFixed(2));
	$("#tot_amt").val(sum.toFixed(2));
	
	checkMinimumProduct();
}
function checkMinimumProduct()
{
	if ($('.product_id')[0]) {
		$('.chkmin').prop('disabled', false);
	}else{
		$('.chkmin').prop('disabled', true);
	}
}
//FOR ONLY NUMBER AND DOT CHECKIN START(Basically balance section)
$('.cost,.quantity').keypress(function(event) {
	if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
		event.preventDefault();
	}
});
//FOR ONLY NUMBER AND DOT CHECKIN END
$(window).keydown(function(e) {
	if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
	// if (e.which == 37 || e.which == 38 || e.which == 39 || e.which == 40) { 
	if (e.which == 37 || e.which == 39) { 
		$('#search').val('');
		$('#search').focus();
	}
})
</script>
@endsection