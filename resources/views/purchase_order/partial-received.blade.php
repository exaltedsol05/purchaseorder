@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Partial Received Order</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="">Home</a></li>
              <li class="breadcrumb-item active">Partial Received Order</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
	<div class="text-right pb-2 pr-2">
		<a href="{{ route('po.create_order') }}"><button type="button" class="btn btn-primary">Create New</button></a>
	</div>
 <!-- Main content -->
    <section class="content">
      <div class="row">
		<div class="col-12">
		@if (session('msg'))
			<div class="alert alert-{{session('msgType')}}" role="alert">
				{{session('msg')}}
			</div>
		@endif
		<div class="card">
            <!-- /.card-header -->
            <div class="card-body table-responsive">
				<table id="example1" class="table table-bordered table-striped">
                <thead>
				<tr>
					<th>Ref no</th>
					<th>Create Date</th>
					<th>Delivery Date</th>
					<th>Supplier Name</th>
					<th>Total Value</th>
					<th>Action</th>
                </tr>
                </thead>
                <tbody>
				@foreach($poArr as $po)
				<tr id="dataRow{{$po->id}}">
					<td><a href="{{route('order.view',[$po->id])}}">{{$po->ref_no}}</a></td>
					<td>{{date('d/m/Y',strtotime($po->current_date))}}</td>
					<td>{{date('d/m/Y',strtotime($po->due_date))}}</td>
					<td>{{$po->sName}}</td>
					<td>{{$po->tot_amt}}</td>
					<td class="text-center" style="width:80px">
						<a href="{{route('po.prepare_order',[$po->id])}}" data-toggle="tooltip" title="" class="prepareButton" data-original-title="View">Prepare</a>
						<!--<a href="" data-toggle="tooltip" title="Delete" class="delIcon"><i class="fas fa-trash" aria-hidden="true"></i></a>&nbsp;
						<a href="" data-toggle="tooltip" title="Edit" class="editIcon">Edit</a>-->
					</td>
                </tr>
				@endforeach
                </tbody>
				</table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection