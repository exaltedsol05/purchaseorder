@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Edit Purchase Order</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="">Purchase Order</a></li>
                    <li class="breadcrumb-item active">Edit Purchase Order</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Purchase Order</h3>
                    </div>
					@if (session('msg'))
                        <div class="alert alert-{{session('msgType')}}" role="alert">
                            {{session('msg')}}
                        </div>
                    @endif
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmclient" method="post" action="{{ route('po.edit_order_post',[$po->id]) }}" enctype="multipart/form-data">
					@csrf
                        <div class="card-body">
                            <div class="row">
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>PO No</label>
                                        <input type="text" class="form-control" name="po_no" value="{{$po->po_no}}" placeholder="PO No" required>
                                    </div>
                                </div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Supplier name</label>
										<select name="supplier_id" id="supplier_id" class="form-control select2" required tabindex="1">
											<option value="">Select...</option>
											@foreach($supplierArr as $supplierVal)
											<option value="{{$supplierVal->id}}" <?PHP echo($po->supplier_id == $supplierVal->id? 'selected' : ''); ?>>{{$supplierVal->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Due date</label>
                                        <input type="text" class="form-control" id="datepicker" name="due_date" value="{{date('d/m/Y',strtotime($po->due_date))}}" placeholder="Due Date" required>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Delivery address</label>
                                        <input type="text" class="form-control" name="delivery_address" value="{{$po->delivery_address}}" placeholder="Delivery Address" required>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Note</label>
                                        <input type="text" class="form-control" name="note" value="{{$po->note}}" placeholder="Note" required>
                                    </div>
                                </div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Bill generated status</label>
										<select name="bill_status" id="bill_status" class="form-control select2" required>
											<option value="">Select...</option>
											<option value="Yes" <?PHP echo($po->bill_status == 'Yes'? 'selected' : ''); ?>>Yes</option>
											<option value="No" <?PHP echo($po->bill_status == 'No'? 'selected' : ''); ?>>No</option>
											
										</select>
									</div>
								</div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Threshold</label>
                                        <input type="text" class="form-control" name="threshold" value="{{$po->threshold}}" placeholder="Threshold" required>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="card-footer">
							<button type="button" name="btnSubmit" id='addOrderButton' class="btn btn-success hidden-btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add More</button>
						</div>
						<div class="card-body" id="tRow">
							<div class="row" id="">
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Barcode</label>
                                    </div>
                                </div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Product name</label>
									</div>
								</div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Product Quantity</label>
                                    </div>
                                </div>
							</div>
							<?php $i=1;?>
							@foreach($poArr as $poArrVal)
							<div class="row" id="dataRow{{$i}}">
								<div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="barcode[]" value="{{$poArrVal->barcode}}" placeholder="Barcode" required>
                                        <input type="hidden" name="odId[]" value="{{$poArrVal->id}}">
                                    </div>
                                </div>
								<div class="col-md-4">
									<div class="form-group">
										<select name="product_id[]" class="form-control select2" required tabindex="1">
											<option value="">Select...</option>
											@foreach($productArr as $productVal)
											<option value="{{$productVal->id}}" <?PHP echo($poArrVal->product_id == $productVal->id? 'selected' : ''); ?>>{{$productVal->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <input type="number" class="form-control" name="product_quantity[]" value="{{$poArrVal->product_quantity}}" placeholder="Product Quantity" required>
                                    </div>
                                </div>
								<div class="col-md-2 col-sm-2 col-xs-2">
									<a href="{{route('order.od_delete',['id'=> $poArrVal->id,'pId'=>$po->id])}}"><button id="'+i+'" type="button" name="btnSubmit" class="btn btn-danger pull-right btn_remove"><i class="fa fa-trash" style="color:#fff"></i></button></a>
								</div>
							</div>
							<?php $i++;?>
							@endforeach
						</div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" name="btnSubmit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<script>
	$(document).ready(function(){
		if(<?php echo $poArr !=''?>){
			var i = <?php echo count($poArr);?>;
		}else{
			var i=1;
		}
		$("#addOrderButton").click(function () {
			i++;
			 $('#tRow').append('<div id="dataRow'+i+'" class="row"><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" name="barcode[]" placeholder="Barcode" required><input type="hidden" name="odId[]" value=""></div></div><div class="col-md-4"><div class="form-group"><select name="product_id[]" class="form-control select2" required tabindex="1"><option value="">Select...</option>@foreach($productArr as $productVal)<option value="{{$productVal->id}}">{{$productVal->name}}</option>@endforeach</select></div></div><div class="col-md-3"><div class="form-group"><input type="number" class="form-control" name="product_quantity[]" placeholder="Product Quantity" required></div></div><div class="col-md-2 col-sm-2 col-xs-2"><button id="'+i+'" onclick="remRow('+i+')" type="button" name="btnSubmit" class="btn btn-danger pull-right btn_remove"><i class="fa fa-trash" style="color:#fff"></i></button></div></div>');
		});
	});
	function remRow(c) {
		$("#dataRow" + c).remove();
	}
</script>
@endsection