@extends('layouts.app')

@section('content')
<style>
    #barcode {
        border: none;
        color: transparent;
        display: inline-block;
        opacity: "50";
        font-size:20px;
        font-weight:bold;
        background:transparent;
		pointer-events:none;
    }
    #barcode:focus {
        outline: none;
        color: transparent;
        text-shadow: 0px 0px 0px #666;
    }
    #barcodeMsg {
        font-size:20px;
        font-weight:bold;
    }
</style>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Prepare Purchase Order</h1>
				<input type="text" id="barcode" autofocus>
                <span id="barcodeMsg"></span>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="">Prepare Order</a></li>
                    <li class="breadcrumb-item active">Prepare Purchase Order</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Prepare Purchase Order</h3>
                    </div>
					@if (session('msg'))
                        <div class="alert alert-{{session('msgType')}}" role="alert">
                            {{session('msg')}}
                        </div>
                    @endif
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmclient" method="post" action="{{ route('order.partial',[$po->id]) }}" enctype="multipart/form-data">
					@csrf
                        <div class="card-body">
                            <div class="row">
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>PO No : </label>{{$po->po_no}}
										<input type="hidden" id="po_no" name="po_no" value="{{$po->po_no}}">
										<input type="hidden" id="ref_no" name="ref_no" value="{{$po->ref_no}}">
                                    </div>
                                </div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Supplier name : </label>{{$po->sName}}
										<input type="hidden" name="supplier_id" value="{{$po->supplier_id}}">
									</div>
								</div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Due date : </label>{{date('d/m/Y',strtotime($po->due_date))}}
										<input type="hidden" name="due_date" value="{{$po->due_date}}">
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Delivery address : </label>{{$po->delivery_address}}
										<input type="hidden" name="delivery_address" value="{{$po->delivery_address}}">
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Note : </label>{{$po->note}}
										<input type="hidden" name="note" value="{{$po->note}}">
                                    </div>
                                </div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Bill generated status : </label>{{$po->bill_status}}
										<input type="hidden" name="bill_status"  value="{{$po->bill_status}}">
									</div>
								</div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Threshold</label> : {{$po->threshold}}
										<input type="hidden" name="threshold" value="{{$po->threshold}}">
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="card">
							<!-- /.card-header -->
							<div class="card-body table-responsive">
								<table id="" class="table table-bordered table-striped">
								<thead>
								<tr>
									<th>Barcode</th>
									<th>Product name</th>
									<th class="text-center">Product Quantity</th>
									<th class="text-center">Quantity Received</th>
								</tr>
								</thead>
								<tbody>
								<?php $order_details_total_quantity = 0;?>
								@foreach($poArr as $poArrVal)
								<tr id="dataRow{{$poArrVal->id}}">
									<td>{{$poArrVal->barcode}}
										<input type="hidden" value="{{$poArrVal->barcode}}" name="barcode[]" id="getBarcode{{$poArrVal->id}}">
										<input type="hidden" value="{{$poArrVal->id}}" name="odId[]">
									</td>
									<td>{{$poArrVal->pName}}
										<input type="hidden" value="{{$poArrVal->product_id}}" name="product_id[]" id="getBarcode{{$poArrVal->id}}">
									</td>
									<td class="text-center">{{$poArrVal->product_quantity}}
										<input type="hidden" value="{{$poArrVal->product_quantity}}" name="product_quantity[]" id="orderedQty{{$poArrVal->id}}" class="orderedQty">
									</td>
									<td class="text-center">
										
										<input type="button" value="--" id="mtySubs{{$poArrVal->id}}" class="btn btn-danger" onclick="mtySubsFunction({{$poArrVal->id}})" style="margin-right:5px;" disabled="disabled"/>

										<input type="button" value="-" class="btn btn-danger" onclick="subsFunction({{$poArrVal->id}})" id="subOne{{$poArrVal->id}}" disabled="disabled"/>

										&nbsp;
										<input type="text" value="0" id="noQty{{$poArrVal->id}}" name="noQty[]" style="width: 45px; height: 35px; border-radius: 5px; text-align: center;" readonly>
										&nbsp;
										
										<input type="button" value="+" class="btn btn-success" onclick="addsFunction({{$poArrVal->id}})" id="addOne{{$poArrVal->id}}" style="margin-right:5px;"/>	

										<input type="button" value="++" id="mtyAdds{{$poArrVal->id}}" class="btn btn-success" onclick="mtyAddsFunction({{$poArrVal->id}})"/>	
										
									</td>
								</tr>
								<?php $order_details_total_quantity = $order_details_total_quantity+$poArrVal->product_quantity;?>
								@endforeach
								<input type="hidden" value="<?php echo $order_details_total_quantity; ?>" name="totalQty" id="totalQty">
								<input type="hidden" value="0" name="subtotalQty" id="subtotalQty">
								</tbody>
								</table>
							</div>
							<!-- /.card-body -->
						</div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" name="btnSubmit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		var val = $('#barcode').val();
		//alert(val);
		var po_no = $('#po_no').val();
		var urlParam = po_no+"?"+val;
		$.ajax({
			//type: 'POST',
			url: "{{ route('order.getBarcode',["+urlParam+"]) }}",
			data: {barcode: val, po_no: po_no},
			type: "GET",
			dataType: 'html',
			
			success: function (response) {
				var obj = jQuery.parseJSON(response);
				var id = obj.id;
				if (id > 0)
				{
					$('#barcodeMsg').css('color', 'green').text('success');
                    $('#barcode').val('');
					$("#subOne" + id).attr("disabled", false);
					$("#mtySubs" + id).attr("disabled", false);
					var ordQty = $("#noQty" + id).val();
					if (ordQty == $("#orderedQty" + id).val()) {
						$('#barcodeMsg').css('color', 'red').text('Exceed Limit');
						$('#barcode').focus();
						
					} else {
						var a = parseInt(ordQty) + 1;
                        $("#noQty" + id).val(a);
						if ($("#orderedQty" + id).val() == a) {
							//alert('ok1');
							$("#addOne" + id).attr("disabled", true);
							$("#mtyAdds" + id).attr("disabled", true);
							$("#subOne" + id).attr("disabled", false);
							$("#mtySubs" + id).attr("disabled", false);
						} else {
							//
						}
					}
				}else{
					$('#barcodeMsg').css('color', 'red').text('Incorrect');
					$('#barcode').val('');
					$('#barcode').focus();
				}
			}
		});
	}
});
function addsFunction(id, orderWeight) {
	////// For orderWeight section start	//////
	var pw = $("#orderWeight").val();
	$("#orderWeight").val(parseInt(pw) + parseInt(orderWeight));
	////// For orderWeight section end //////
	
	$("#subOne" + id).attr("disabled", false);
	$("#mtySubs" + id).attr("disabled", false);
	var ordQty = $("#noQty" + id).val();
	var a = parseInt(ordQty) + 1;
	//alert(a);
	$("#noQty" + id).val(a);
	$("#subtotalQty").val(parseInt($("#subtotalQty").val())+1);
	if ($("#orderedQty" + id).val() == a) {
		$("#addOne" + id).attr("disabled", true);
		$("#mtyAdds" + id).attr("disabled", true);
		$("#mtyAdds_p" + id).attr('class', 'btn btn-success');
		$("#mtyAdds_p" + id).val('Packed');
	} else {
		var remaining = parseInt($("#orderedQty" + id).val()) - parseInt(a);
		$("#mtyAdds_p" + id).attr('class', 'btn btn-warning');
		var data = 'Remain ' + remaining;
		$("#mtyAdds_p" + id).val(data);
	}
}
function subsFunction(id, orderWeight) {
	////// For orderWeight section start //////	
	var pw = $("#orderWeight").val();
	$("#orderWeight").val(parseInt(pw) - parseInt(orderWeight));
	////// For orderWeight section end	//////
	
	$("#addOne" + id).attr("disabled", false);
	$("#mtyAdds" + id).attr("disabled", false);
	var ordQty = $("#noQty" + id).val();
	if ($("#noQty" + id).val() != 0) {
		var a = parseInt(ordQty) - 1;
		//alert(a);
		$("#noQty" + id).val(a);
		$("#subtotalQty").val(parseInt($("#subtotalQty").val())-1);
		if (a == 0) {
			$("#subOne" + id).attr("disabled", true);
			$("#mtySubs" + id).attr("disabled", true);
			$("#mtyAdds_p" + id).attr('class', 'btn btn-danger');
			$("#mtyAdds_p" + id).val('Unpacked');
		} else {
			var remaining = parseInt($("#orderedQty" + id).val()) - parseInt(a);
			$("#mtyAdds_p" + id).attr('class', 'btn btn-warning');
			var data = 'Remain ' + remaining;
			$("#mtyAdds_p" + id).val(data);
			//$("#mtyAdds_p"+id).val('Packed');
		}
	}
}
function mtySubsFunction(id, orderWeight) {
	////// For orderWeight section start //////	
	var pw = $("#orderWeight").val();
	var ordQty = $("#noQty" + id).val();
	$("#subtotalQty").val(parseInt($("#subtotalQty").val()) - parseInt(ordQty));
	$("#orderWeight").val(parseInt(pw)-(parseInt(ordQty)*parseInt(orderWeight)));
	////// For orderWeight section end //////
	
	$('#noQty' + id).val('0');
	$("#mtyAdds_p" + id).attr('class', 'btn btn-danger');
	$("#mtyAdds_p" + id).val('Unpacked');
	$("#addOne" + id).attr("disabled", false);
	$("#mtyAdds" + id).attr("disabled", false);
	$("#subOne" + id).attr("disabled", true);
	$("#mtySubs" + id).attr("disabled", true);
}
function mtyAddsFunction(id, orderWeight) {	
	$("#subOne" + id).attr("disabled", false);
	$("#mtySubs" + id).attr("disabled", false);
	var ordQty = $("#noQty" + id).val();
	
	$("#subtotalQty").val(parseInt($("#orderedQty" + id).val()) + parseInt($("#subtotalQty").val())-parseInt(ordQty));
	var a = $('#noQty' + id).val($("#orderedQty" + id).val());
	
	////// For orderWeight section start //////
	var pw = $("#orderWeight").val();
	var totOrdQty = $("#orderedQty" + id).val();
	var orderPackQty = parseInt(totOrdQty)-parseInt(ordQty);
	$("#orderWeight").val(parseInt(pw)+(parseInt(orderPackQty)*parseInt(orderWeight)));
	////// For orderWeight section end //////
	
	$("#mtyAdds_p" + id).attr('class', 'btn btn-success');
	$("#mtyAdds_p" + id).val('Packed');
	$("#addOne" + id).attr("disabled", true);
	$("#mtyAdds" + id).attr("disabled", true);
}
$(document).keydown(function(e) {
	if (e.which == 37 || e.which == 38 || e.which == 39 || e.which == 40) { 
		$('#barcode').val('');
		$('#barcode').focus();
		$('#barcodeMsg').css('color', 'red').text('');
	}
})
</script>
@endsection