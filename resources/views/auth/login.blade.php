@extends('layouts.applogin')

@section('content')
<div class="card">
	<div class="card-body login-card-body">
		<form method="POST" action="{{ route('login') }}">
			@csrf
			<div class="input-group mb-2">
				<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
				<div class="input-group-prepend">
					<span class="input-group-text input-group-addon rWidth">
						<i class="fa fa-user"></i>
					</span>
				</div>
				@error('email')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
			</div>
			<div class="input-group mb-2">
				<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
				<div class="input-group-prepend">
					<span class="input-group-text input-group-addon rWidth">
						<i class="fa fa-lock"></i>
					</span>
				</div>
				@error('password')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
				@enderror
			</div>
			<div class="row">
				<!--<div class="col-md-8">
				  <div class="checkbox icheck">
					<label><input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>{{ __('Remember Me') }}</label>
				  </div>
				</div>-->
				<div class="col-md-12">
				  <button type="submit" class="btn btn-success">{{ __('Login') }}</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
