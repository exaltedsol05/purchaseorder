@extends('layouts.app')

@section('content')
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #ffc107;
}

input:focus + .slider {
  box-shadow: 0 0 1px #ffc107;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">View Supplier</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('supplier.index') }}">Supplier</a></li>
                    <li class="breadcrumb-item active">View Supplier</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!--<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
				<div class="text-right">
					<a href="{{route('supplier.destroy',[$supplierArr->id])}}"><button type="submit" class="btn btn-danger">Delete</button></a>
				</div>
			</div>
        </div>
    </div>
</section>-->
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">View Supplier</h3>
                    </div>
					@if (session('msg'))
                        <div class="alert alert-{{session('msgType')}}" role="alert">
                            {{session('msg')}}
                        </div>
                    @endif
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmclient" method="post" action="" enctype="multipart/form-data">
					@csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Xero contact code : </label> <?PHP echo($supplierArr->xeroContactId != '' ? $supplierArr->xeroContactId : 'NA'); ?>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Supplier Name : </label> {{$supplierArr->name}}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Supplier Code : </label> {{$supplierArr->supplier_code}}
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Contact Person Name</label> {{$supplierArr->person_name}}
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Contact Phone : </label> {{$supplierArr->person_phone}}
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email : </label> {{$supplierArr->email}}
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label>CC Email : </label> {{$supplierArr->cc_email}}
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label>Notes : </label> {{$supplierArr->notes}}
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank details : </label> {{$supplierArr->bank_details}}
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address : </label> {{$supplierArr->address}}
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="card-body" id="tRow">
							<div class="row" id="">
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Cost Label</label>
                                    </div>
                                </div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Cost Value</label>
									</div>
								</div>
							</div>
							<?php $i=1;?>
							@foreach($costArr as $costArrVal)
							<div class="row" id="dataRow{{$i}}">
								<div class="col-md-3">
                                    <div class="form-group">
                                        {{$costArrVal->cost_label}}
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        {{$costArrVal->cost_value}}
                                    </div>
                                </div>
							</div>
							<?php $i++;?>
							@endforeach
						</div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{route('supplier.index')}}"><button type="button" class="btn btn-primary">Back</button></a>
							<a href="{{route('supplier.edit',[$supplierArr->id])}}"><button type="button" class="btn btn-success">Edit</button></a>
							<?php if($supplierArr->isDeleted==0){ ?>
								<a href="{{route('supplier.destroy',[$supplierArr->id])}}"><button type="button" class="btn btn-danger">Delete</button></a>
							<?php }?>
							<label class="switch" style="float:right;">
								<input type="checkbox" class="changeStatus" data-id="{{$supplierArr->id}}" <?php echo $supplierArr->isActive==0 ? 'checked' : '';?>>
								<span class="slider round"></span>
							</label>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<script>
	//function changeStatus(id, val) {
	$('.changeStatus').change(function() {
		var id = $(this).data('id'); 
		var val = $(this).prop('checked') == true ? 0 : 1; 
		//alert(id);
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			 }
		});
		$.ajax({
			url: "{{url('/changeStatus')}}",
			method: "POST",
			data: {id:id,val:val},
			dataType: 'html',
			success: function(response) {
				/*if(response==1){
					if(val==1){
						$('.changeStatus'+id).html("<img src='{{ asset('assets/dist/img/inactive.png') }}' height='25'>").attr('onclick', 'changeStatus(\''+id+'\',\'0\')');
					}else{
						$('.changeStatus'+id).html("<img src='{{ asset('assets/dist/img/active.png') }}' height='25'>").attr('onclick', 'changeStatus(\''+id+'\',\'1\')');
					}
				}*/
			}
		});
	})
</script>
@endsection