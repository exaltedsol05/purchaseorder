@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Supplier List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('supplier.index') }}">Home</a></li>
              <li class="breadcrumb-item active">Supplier List</li>
            </ol>
          </div>
        </div>
		<div class="row">
			<div class="col-sm-6">
				<select id="find_status" class="form-control" onchange="getSupplier()">
					<option value="0">Active</option>
					<option value="1">Inactive</option>
				</select>
			</div>
			<div class="col-sm-6">
				<div class="text-right pb-2 pr-2">
					<a href="{{ route('supplier.create') }}"><button type="submit" class="btn btn-primary">Add Supplier</button></a>
				</div>
			</div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
	
 <!-- Main content -->
    <section class="content">
      <div class="row">
		<div class="col-12">
		@if (session('msg'))
			<div class="alert alert-{{session('msgType')}}" role="alert">
				{{session('msg')}}
			</div>
		@endif
		<div class="card">
            <!-- /.card-header -->
            <div class="card-body table-responsive" id="getSupplier">
				<table id="example1" class="table table-bordered table-striped">
                <thead>
				<tr>
					<th>Supplier Name</th>
					<th>Supplier Code</th>
					<th>Contact Name</th>
					<th>Phone</th>
					<th>Email</th>
					<th class="text-center" style="width:80px">Action</th>
                </tr>
                </thead>
                <tbody>
				@foreach($supplierArr as $supplier)
				<tr>
					<td><a href="{{route('supplier.view',[$supplier->id])}}">{{$supplier->name}}</a></td>
					<td>{{$supplier->supplier_code}}</td>
					<td>{{$supplier->person_name}}</td>
					<td>{{$supplier->person_phone}}</td>
					<td>{{$supplier->email}}</td>
					<td class="text-center" style="width:80px">
						<a href="{{route('supplier.edit',[$supplier->id])}}" data-toggle="tooltip" title="Edit" style="text-decoration:none;color:#333;"><i class="fas fa-edit" aria-hidden="true"></i></a>
					</td>
                </tr>
				@endforeach
                </tbody>
				</table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
	<script>
		function getSupplier() 
		{
			var find_status = $('#find_status').val();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				 }
			});
			$.ajax({
				url: "{{url('/supplierList')}}",
				method: "POST",
				data: {status:find_status},
				dataType: 'html',
				success: function(response) {
					var obj = jQuery.parseJSON(response);
					$('#getSupplier').html(obj.html);
					$("#example1").DataTable({
						'ordering'    : false,
					});
				}
			});
		}
	</script>
@endsection