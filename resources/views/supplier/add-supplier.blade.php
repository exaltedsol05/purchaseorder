@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Add Supplier</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('supplier.index') }}">Supplier</a></li>
                    <li class="breadcrumb-item active">Add Supplier</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Supplier</h3>
                    </div>
					@if (session('msg'))
                        <div class="alert alert-{{session('msgType')}}" role="alert">
                            {{session('msg')}}
                        </div>
                    @endif
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmclient" method="post" action="{{ route('supplier.store') }}" enctype="multipart/form-data">
					@csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Supplier Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Supplier Name" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Supplier Code</label>
                                        <input type="text" class="form-control" name="supplier_code" placeholder="Supplier Code" required>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
										<label>Woocomerce Id</label>
										<select name="woocomerce_supplier_id" id="woocomerce_supplier_id" class="form-control select2" required tabindex="1">
											<option value="">Select...</option>
											@foreach($wSupplierId as $wSupplierIdVal)
											<option value="{{$wSupplierIdVal->supplier_name}}">{{$wSupplierIdVal->supplier_name}}</option>
											@endforeach
										</select>
									</div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Contact Person Name</label>
                                        <input type="text" class="form-control" name="person_name" placeholder="Contact Person Name" required>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Contact Phone</label>
                                        <input type="text" class="form-control" name="person_phone" placeholder="Contact Phone" required>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email (Comma[,] separated email)</label>
                                        <input type="text" class="form-control" name="email" placeholder="Email" required>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label>CC Email (Comma[,] separated email)</label>
                                        <input type="text" class="form-control" name="cc_email" placeholder="CC Email">
                                    </div>
                                </div>
								<!--<div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email 3</label>
                                        <input type="text" class="form-control" name="email_3" placeholder="Email">
                                    </div>
                                </div>-->
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label>Notes</label>
										<textarea class="form-control" name="notes" placeholder="Notes" required></textarea>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank details</label>
										<textarea class="form-control" name="bank_details" placeholder="Bank details" required></textarea>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address</label>
										<textarea class="form-control" name="address" placeholder="Address" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="card-footer">
							<button type="button" name="btnSubmit" id='addcostButton' class="btn btn-success hidden-btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add More</button>
						</div>
						<div class="card-body" id="tRow">
							<div class="row" id="dataRow1">
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cost Label</label>
                                        <input type="text" class="form-control" name="cost_label[]" placeholder="Cost Label" required>
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cost Value</label>
                                        <input type="text" class="form-control" name="cost_value[]" placeholder="Cost Value" required>
                                    </div>
                                </div>
							</div>
						</div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" name="btnSubmit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<script>
	$(document).ready(function(){
		var i=1;
		$("#addcostButton").click(function () {
			i++;
			 $('#tRow').append('<div id="dataRow'+i+'" class="row"><div class="col-md-4"><div class="form-group"><input type="text" class="form-control" name="cost_label[]" placeholder="Cost Label" required></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control" name="cost_value[]" placeholder="Cost Value" required></div></div><div class="col-md-2 col-sm-2 col-xs-2"><button id="'+i+'" onclick="remRow('+i+')" type="button" name="btnSubmit" class="btn btn-danger pull-right btn_remove"><i class="fa fa-trash" style="color:#fff"></i></button></div></div>');
		});
	});
	function remRow(c) {
		$("#dataRow" + c).remove();
	}
</script>
@endsection