@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Edit Email</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="">Email</a></li>
                    <li class="breadcrumb-item active">Edit Email</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Email</h3>
                    </div>
					@if (session('msg'))
                        <div class="alert alert-{{session('msgType')}}" role="alert">
                            {{session('msg')}}
                        </div>
                    @endif
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmclient" id="frm" method="post" action="{{ route('email.edit_email_post',[$email->id]) }}" enctype="multipart/form-data">
					@csrf
                        <div class="card-body">
                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Email Subject</label>
										<input type="text" class="form-control" name="emailSubject" value="{{$email->emailSubject}}" placeholder="Email Subject" required>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Email Body</label>
										<textarea  class="form-control" name="emailBody" id="email_pre_body" placeholder="Email Body">{{$email->emailBody}}</textarea>
									</div>
								</div>
								<div class="col-md-12"><strong>Note: [NAME], [PO], [ADDRESS] cannot be change !</strong></div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" name="submit" class="btn btn-primary" value="save">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
$(document).ready(function(){
	CKEDITOR.replace( 'email_pre_body', {
		allowedContent:true,
	});
});
</script>
@endsection