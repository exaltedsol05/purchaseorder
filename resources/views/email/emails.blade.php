@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Email Management</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="">Home</a></li>
              <li class="breadcrumb-item active">Email Management</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
	<div class="text-right pb-2 pr-2">
		<a href="{{route('email.create_email')}}"><button type="button" class="btn btn-primary">Create New</button></a>
	</div>
 <!-- Main content -->
    <section class="content">
      <div class="row">
		<div class="col-12">
		@if (session('msg'))
			<div class="alert alert-{{session('msgType')}}" role="alert">
				{{session('msg')}}
			</div>
		@endif
		<div class="card">
            <!-- /.card-header -->
            <div class="card-body table-responsive">
				<table id="example1" class="table table-bordered table-striped">
                <thead>
				<tr>
					<th>Email Subject</th>
					<th class="text-center" style="width:80px">Action</th>
                </tr>
                </thead>
                <tbody>
				@foreach($emails as $emailsVal)
				<tr id="dataRow{{$emailsVal->id}}">
					<td>{{$emailsVal->emailSubject}}</td>
					<td class="text-center" style="width:80px">
					<a href="{{route('email.edit_email',[$emailsVal->id])}}" data-toggle="tooltip" title="Edit" style="text-decoration:none;color:#333;"><i class="fas fa-edit" aria-hidden="true"></i></a>
					</td>
                </tr>
				@endforeach
                </tbody>
				</table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection