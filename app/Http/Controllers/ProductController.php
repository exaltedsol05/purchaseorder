<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Product_custom;
use App\Models\Supplier;
use Illuminate\Http\Request;
use DB;
use File;

class ProductController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
		//$productArr =  array();
		$product = DB::table('product')
				->select('*')
				->where('status', 0)
				->where('delete_status', 0)
				->orderBy('id')
				->get();
		$supplierArr = DB::table('supplier')
				->select('supplier.*')
				->where('isActive', 0)
				->where('isDeleted', 0)
				->get();	
		$data =array();
		foreach($supplierArr as $val){
			if($val->woocomerce_supplier_id!=''){
				$data[$val->name] = $val->woocomerce_supplier_id;
			}
		}
		/*$product = DB::table('product')
			->join('supplier', 'product.supplier_name', '=', 'supplier.woocomerce_supplier_id')
			->select( 'product.*', 'supplier.name as sName')
			->where('product.status', 0)
			->where('product.delete_status', 0)
			->orderBy('product.id')
			//->where('purchase_order_details.pType', 0)
			->get();*/
							
		$productCustom = DB::table('product_custom')
				->select('*')
				->where('status', 0)
				->where('delete_status', 0)
				->orderBy('id')
				->get();
		$productArr = array_merge(json_decode($product),json_decode($productCustom));
        return view('product/product')->with('productArr',$productArr)->with('supplierArr',$data);
		//return $productArr;
    }
	
    public function create()
    {
		$supplierArr = DB::table('supplier')
				->select('supplier.*')
				->where('isActive', 0)
				->where('isDeleted', 0)
				->get();
		
        return view('product/add-product')->with('supplierArr',$supplierArr);
    }
	
	public function view(Product $Product, $id, $table)
    {
		if($table==1){
			$table_name = 'product';
		}if($table==2){
			$table_name = 'product_custom';
		}
		$product = DB::table($table_name)
			->where('id', $id)
			->first();
			
		$supplierArr = DB::table('supplier')
				->select('supplier.*')
				->where('isActive', 0)
				->where('isDeleted', 0)
				->get();	
		$data =array();
		foreach($supplierArr as $val){
			if($val->woocomerce_supplier_id!=''){
				$data[$val->name] = $val->woocomerce_supplier_id;
			}
		}
		return view('product/view-product')->with('product',$product)->with('supplierArr',$data);
	}
	
    public function store(Request $request)
    {
		/*$sku = preg_split("/\s+/", $request->input('name'));
		$getSku = "MB-";

		foreach ($sku as $s) {
		  $getSku .= $s[0];
		}*/
		$getSku = "MB-".$request->input('sku');
		$noImagePath = asset('assets/dist/img/noimage.png');
		//echo $noImagePath; exit;
		$res=new product_custom;
		$res->table_name='product_custom';
		$res->product_id=rand(00000000, 99999999);
		$res->name=$request->input('name');
		$res->sku=$getSku;
		$res->supplier_name=$request->input('supplier_name');
		$res->supplier_code=$request->input('supplier_code');
		$res->images=$noImagePath;
		$res->purchase_cost=$request->input('purchase_cost');
		$res->uom=$request->input('uom');
		$res->regular_price=$request->input('regular_price');
		$res->barcode=$request->input('barcode');
		$res->manage_stock=$request->input('manage_stock');
		//$res->current_stock=$request->input('current_stock');
		$res->threshold=$request->input('threshold');
		$res->bin_location=$request->input('bin_location');
		$res->status=$request->input('status');
		if($res->save()){
			session()->flash('msg', 'Product addded successfully.');
			session()->flash('msgType', 'success');
			return redirect('products');
		}
		/*if($res->save()){
				
			$consumer_key = 'ck_85cf3eb73d3eeed9085350345008c6ad2c3baffa'; 
			$consumer_secret = 'cs_e061027c217ba8efc646f6396bb9857fe4214b4d'; 
			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://dev.nappies.co.nz/wp-json/wc/v3/products/?consumer_key='.$consumer_key.'&consumer_secret='.$consumer_secret,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			// CURLOPT_POSTFIELDS => "{\n  \"name\": \"Premium Quality\",\n  \"type\": \"variable\",\n  \"sku\" : \"123450000\"\n}",
			//CURLOPT_POSTFIELDS => "{\n  \"name\": \"".$request->input('name')."\",\n  \"type\": \"variable\",\n  \"sku\" : \"".$getSku."\"\n}",
			//CURLOPT_POSTFIELDS => "{\n  \"name\": \"".$request->input('name')."\",\n  \"sku\": \"".$getSku."\",\n  \"purchase_price\": \"".$request->input('purchase_cost')."\",\n  \"regular_price\": \"".$request->input('regular_price')."\",\n  \"manage_stock\": \"".$request->input('manage_stock')."\",\n  \"stock_quantity\": \"".$request->input('current_stock')."\",\n  \"out_stock_threshold\": \"".$request->input('threshold')."\"\n}",
			//CURLOPT_POSTFIELDS => "{\n  \"name\": \"".$request->input('name')."\",\n  \"sku\": \"".$getSku."\",\n  \"purchase_price\": \"".$request->input('purchase_cost')."\",\n  \"regular_price\": \"".$request->input('regular_price')."\",\n  \"manage_stock\": \"".$request->input('manage_stock')."\",\n  \"stock_quantity\": \"".$request->input('current_stock')."\"\n}",
			CURLOPT_POSTFIELDS => "{\n  \"name\": \"".$request->input('name')."\",\n  \"sku\": \"".$getSku."\",\n  \"purchase_price\": \"".$request->input('purchase_cost')."\",\n  \"regular_price\": \"".$request->input('regular_price')."\",\n  \"manage_stock\": \"".$request->input('manage_stock')."\"\n}",
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic ",
				"Content-Type: application/json",
				"cache-control: no-cache"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);
			if ($err) {
				session()->flash('msg', 'Product addded successfully.');
				session()->flash('msgType', 'success');
				return redirect('products');
			} else {
				session()->flash('msg', 'Product not added.');
				session()->flash('msgType', 'danger');
				return redirect('products');
			}
		}*/
    }

    public function show(Supplier $supplier)
    {
        //
    }

    public function edit(Product $Product, $id, $table)
    {
        if($table==1){
			$table_name = 'product';
		}if($table==2){
			$table_name = 'product_custom';
		}
		$product = DB::table($table_name)
			->where('id', $id)
			->first();
		$supplierArr = DB::table('supplier')
				->select('supplier.*')
				->where('isActive', 0)
				->where('isDeleted', 0)
				->get();
		return view('product/edit-product')->with('product',$product)->with('supplierArr',$supplierArr);
    }
	
	public function update(Request $request)
    {
		$getSku = $request->input('sku');
		$noImagePath = asset('assets/dist/img/noimage.png');
		//echo $noImagePath; exit;
		//$res=new product_custom;
		if($request->input('table_name')=='product'){
			$table_no = 1;
			$res=Product::find($request->id);
		}else{
			$table_no = 2;
			$res=Product_custom::find($request->id);
		}
		$res->table_name=$request->input('table_name');
		$res->product_id=$request->input('product_id');
		$res->name=$request->input('name');
		$res->sku=$getSku;
		$res->supplier_name=$request->input('supplier_name');
		$res->supplier_code=$request->input('supplier_code');
		//$res->images=$noImagePath;
		$res->purchase_cost=$request->input('purchase_cost');
		$res->uom=$request->input('uom');
		$res->regular_price=$request->input('regular_price');
		$res->barcode=$request->input('barcode');
		$res->manage_stock=$request->input('manage_stock');
		//$res->current_stock=$request->input('current_stock');
		$res->threshold=$request->input('threshold');
		$res->bin_location=$request->input('bin_location');
		$res->status=$request->input('status');
		if($res->save()){
			session()->flash('msg', 'Product updated successfully.');
			session()->flash('msgType', 'success');
			return redirect('product-edit/'.$request->id.'/'.$table_no);
		}
	}
	
	public function destroy(Product $Product, $id, $table)
    {
        if($table==1){
			$table_name = 'product';
		}if($table==2){
			$table_name = 'product_custom';
		}
		DB::table($table_name)->where('id', $id)->update(array('delete_status' => 1));
        //supplier::destroy(array('id',$id));
		//DB::table('supplier_cost_details')->where('supplier_id', $id)->delete();
		session()->flash('msg', 'Data deleted successfully');
		session()->flash('msgType', 'success');
		return redirect('products');
    }
	
    public function woocomerceVariationData()
    {
		/*$url = public_path('images');
		echo $url.'<br>';

		// $files = File::files('po.nappies.co.nz/public/images');
		$files = File::files($url);
		print_r($files);exit;*/
		//$files = glob("https://media.nappies.co.nz/wp-content/uploads/2020/07/product-l-i-living-textiles-3pc-storage-set-grey-white-83a-1-1-1-300x300.jpg");
		$files = glob(public_path('upload_csv').'/*.csv');
		//$files=scandir(public_path('images'));
		
		$files = scandir(public_path('upload_csv'), SCANDIR_SORT_DESCENDING);
		$newest_file = $files[0];
		$fileData  = url('public/upload_csv').'/'.$newest_file;
		//echo $fileData;exit;
		return $this->uploadProduct($fileData);
		
	}
    public function woocomerceData(Request $request)
    {
		/*$data1 = array();                                                                    
		$data_string1 = json_encode($data1);                                                                                   

		$consumer_key = 'ck_85cf3eb73d3eeed9085350345008c6ad2c3baffa'; 
		$consumer_secret = 'cs_e061027c217ba8efc646f6396bb9857fe4214b4d'; 

		$ch = curl_init('https://dev.nappies.co.nz/wp-json/wc/v3/products/50093?consumer_key='.$consumer_key.'&consumer_secret='.$consumer_secret);
		curl_setopt($ch, CURLOPT_USERPWD, $consumer_key . ":" . $consumer_secret);                                  
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string1);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string1))                                                                       
		); 
		$data1 = curl_exec($ch);
		$data1 = json_decode($data1);
		$data1 = response()->json([$data1]);
		return $data1;*/
		/*//set_time_limit(0);
		$consumer_key = 'ck_85cf3eb73d3eeed9085350345008c6ad2c3baffa'; 
		$consumer_secret = 'cs_e061027c217ba8efc646f6396bb9857fe4214b4d'; 
		// URLs we want to retrieve
		$urls = array();
		for($i=1; $i<=100; $i++) {
			$urls[] ='https://dev.nappies.co.nz/wp-json/wc/v3/products?per_page=100&page='.$i.'&consumer_key='.$consumer_key.'&consumer_secret='.$consumer_secret;
		}

		// initialize the multihandler
		$mh = curl_multi_init();

		$channels = array();
		foreach ($urls as $key => $url) {
		// initiate individual channel
		$channels[$key] = curl_init();
		curl_setopt_array($channels[$key], array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => ''
		));
	
		// add channel to multihandler
		curl_multi_add_handle($mh, $channels[$key]);
		}
	
		// execute - if there is an active connection then keep looping
		$active = null;
		do {
		$status = curl_multi_exec($mh, $active);
		}
		while ($active && $status == CURLM_OK);

		// echo the content, remove the handlers, then close them
		$final_data = array();
		$barcode = '';
		$keyImage = '';
		$variations = '';
		$noImagePath = asset('assets/dist/img/noimage.png');
		foreach ($channels as $chan) {
			$result = curl_multi_getcontent($chan);
			$dd = json_decode($result,true);
//echo '<pre>'; print_r($dd); echo'</pre>';exit;
			foreach($dd as $i=>$key){  
				//array_push($final_data,$key[$i]);
				if(!empty($key['images'])){
					$keyImage = $key['images'][0]['src'];
				}else{
					$keyImage = $noImagePath;
				}
				if(!empty($key['variations'])){
					$variations = 1;
				}else{
					$variations = 0;
				}
				$brand = '';
				foreach($key['meta_data'] as $metadata) {
					if($metadata['key']=='_ywbc_barcode_display_value') {
						$barcode = $metadata['value'];
					}
					if($metadata['key']=='brand') {
						$brand = $metadata['value'];
					}
				}
				$arr = array(
					'id'=>$key['id'],
					'name'=>$key['name'],
					'sku'=>$key['sku'],
					'brand'=>$brand,
					'supplier_id'=>$key['supplier_id'],
					'supplier_sku'=>$key['supplier_sku'],
					'purchase_price'=>$key['purchase_price'],
					'uom'=>'',
					'regular_price'=>$key['regular_price'],
					'barcode'=>$barcode,
					'manage_stock'=>$key['manage_stock'],
					'stock_quantity'=>$key['stock_quantity'],
					'out_stock_threshold'=>$key['out_stock_threshold'],
					'bin_location'=>'',
					'variations'=>$variations,
					'image'=>$keyImage,
				);
			   array_push($final_data,$arr);
			}
//echo '<pre>'; print_r($final_data); echo'</pre>';exit;
//echo 12;exit;
			curl_multi_remove_handle($mh, $chan);
			curl_close($chan);
		}
		curl_multi_close($mh);
		product::query()->truncate();
		foreach($final_data as $val){
			$res=new product;
			$res->product_id=$val['id'];
			$res->name=$val['name'];
			$res->sku=$val['sku'];
			$res->brand_name=$val['brand'];
			$res->images=$val['image'];
			$res->supplier_name=$val['supplier_id'];
			$res->supplier_code=$val['supplier_sku'];
			$res->purchase_cost=$val['purchase_price'];
			$res->uom=$val['uom'];
			$res->regular_price=$val['regular_price'];
			$res->barcode=($val['barcode'] != ''? $val['barcode'] : rand(0000, 9999).rand(0000, 9999).rand(0000, 9999));
			$res->manage_stock=$val['manage_stock'];
			$res->current_stock=$val['stock_quantity'];
			$res->threshold=$val['out_stock_threshold'];
			$res->bin_location=$val['bin_location'];
			$res->variations=$val['variations'];
			$res->save();
		}
		//Add data in produt table end
		session()->flash('msg', 'Data addded successfully.');
		session()->flash('msgType', 'success');
		return redirect('products');
		//return $meta_data;
		//return $data;*/
		$filename = $request->input('csv_input_link');
		return $this->uploadProduct($filename);
    }
	public function uploadProduct($filename)
    {
		$row = 1;
		if (($handle = fopen($filename, "r")) !== FALSE) {
			$i = 1;
			product::query()->truncate();
			$noImagePath = asset('assets/dist/img/noimage.png');
			while (($data = fgetcsv($handle)) !== FALSE) {
				if ($i > 1) {
					if($data[17]!=''){ 
						$img = explode('|',$data[17]);
						$imgPath = $img[0];
					}else{
						$imgPath = $noImagePath;
					}
					$woocomerce_status = 0;
					if($data[18]!=''){
						$visible = explode('|',$data[18]);
						if((array_key_exists(0,$visible) && $visible[0]!='' && ($visible[0]=='exclude-from-catalog:8736:0' || $visible[0]=='exclude-from-search:8735:0' || $visible[0]=='outofstock:9:0')) && (array_key_exists(1,$visible) && $visible[1]!='' && ($visible[1]=='exclude-from-catalog:8736:0' || $visible[1]=='exclude-from-search:8735:0' || $visible[1]=='outofstock:9:0')) && (array_key_exists(2,$visible) && $visible[2]!='' && ($visible[2]=='exclude-from-catalog:8736:0' || $visible[2]=='exclude-from-search:8735:0' || $visible[2]=='outofstock:9:0'))){
							$woocomerce_status = 1;
						}else{
							$woocomerce_status = 0;
						}
					}else{
						$woocomerce_status = 0;
					}
					//echo '('.$i.')'.$woocomerce_status.'=';
					$res=new product;
					$res->table_name='product';
					$res->product_id=$data[1];
					$res->parent_id=$data[0];
					$res->name=$data[2];
					$res->sku=$data[4];
					$res->brand_name=$data[5];
					$res->images=$imgPath;
					$res->supplier_name=$data[6];
					$res->supplier_code=$data[8];
					$res->purchase_cost=$data[9];
					$res->uom=$data[10];
					$res->regular_price=$data[11];
					$res->barcode=($data[12] != ''? $data[12] : rand(0000, 9999).rand(0000, 9999).rand(0000, 9999));
					$res->manage_stock=$data[13];
					$res->current_stock=$data[14];
					$res->threshold=$data[15];
					$res->bin_location=$data[16];
					$res->backorder=$data[19];
					$res->status=$woocomerce_status;
					$res->save();
				}
				$i++;
			}
			fclose($handle);
			session()->flash('msg', 'Data addded successfully.');
			session()->flash('msgType', 'success');
			return redirect('products');
		}
	}
	public function getSCode(Request $request)
    {
		$poArr = DB::table('supplier')
				->select('*')
				->where('id', $request->supplier_id)
				->first();
		$sCode = $poArr->supplier_code;
		$supplier_name = $poArr->name;
		return response()->json(['sCode'=>$sCode,'supplier_name'=>$supplier_name]);
    }
	public function getPDetails(Request $request)
    {
		$product = DB::table($request->table)
			->where('product_id', $request->id)
			->first();
		$product_id = $product->product_id != '' ? $product->product_id : 'NA';
		$name = $product->name != '' ? $product->name : 'NA';
		$sku = $product->sku != '' ? $product->sku : 'NA';
		$brand_name = $product->brand_name != '' ? $product->brand_name : 'NA';
		$supplier_name = $product->supplier_name != '' ? $product->supplier_name : 'NA';
		$supplier_code = $product->supplier_code != '' ? $product->supplier_code : 'NA';
		$purchase_cost = $product->purchase_cost != '' ? $product->purchase_cost : 'NA';
		$uom = $product->uom != '' ? $product->uom : 'NA';
		$barcode = $product->barcode != '' ? $product->barcode : 'NA';
		$manage_stock = $product->manage_stock != '' ? $product->manage_stock : 'NA';
		$current_stock = $product->current_stock != '' ? $product->current_stock : 'NA';
		$bin_location = $product->bin_location != '' ? $product->bin_location : 'NA';
		$threshold = $product->threshold != '' ? $product->threshold : 'NA';
		$html = '<div class="col-md-12">
					<img class="" style="height:80%;width:80%;" src="'.$product->images.'">
				</div>
				<div class="col-md-12">
					<div class="">
						<label>Id : </label> '.$product_id.'
					</div>
					<div class="">
						<label>Name : </label> '.$name.'
					</div>
					<div class="">
						<label>Sku : </label> '.$sku.'
					</div>
					<div class="">
						<label>Brand Name : </label> '.$brand_name.'
					</div>
					<div class="">
						<label>Supplier Id : </label> '.$supplier_name.'
					</div>
					<div class="">
						<label>Supplier Code : </label> '.$supplier_code.'
					</div>
					<div class="">
						<label>Purchase Price : </label> '.$purchase_cost.'
					</div>
					<div class="">
						<label>UOM : </label> '.$uom.'
					</div>
					<div class="">
						<label>Barcode : </label> '.$barcode.'
					</div>
					<div class="">
						<label>Manage Stock : </label> '.$manage_stock.'
					</div>
					<div class="">
						<label>QTY : </label> '.$current_stock.'
					</div>
					<div class="">
						<label>Bin Location : </label> '.$bin_location.'
					</div>
					<div class="">
						<label>Threshold : </label> '.$threshold.'
					</div>
				</div>';
		return response()->json(['pDetailsBody'=>$html]);
    }
	public function filterProduct(Request $request)
    {
		if($request->find_type!='' && $request->find_type==0){
			$productArr = DB::table('product')
				->where('status', $request->find_status)
				->where('delete_status', 0)
				->get();
		}
		if($request->find_type!='' && $request->find_type==1){
			$productArr = DB::table('product_custom')
				->where('status', $request->find_status)
				->where('delete_status', 0)
				->get();
		}
		$html = '<table id="exampleProduct" class="table table-bordered table-striped">
                <thead>
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Barcode</th>
					<th>SKU</th>
					<th>Brand Name</th>
					<th>Supplier Id</th>
					<th>Purchase Price</th>
					<!--<th style="width:80px">Action</th>-->
                </tr>
                </thead>
                <tbody>';
		foreach($productArr as $product){
			if($product->table_name=='product'){
				$table = 1;
			}if($product->table_name=='product_custom'){
				$table = 2;
			}
			$html .= '<tr>
					<td><a href="'.route('product.view',['id'=> $product->id,'table'=>$table]).'">'.$product->product_id.'</a></td>
					<td><a class="mHover" data-id="'.$product->product_id.'" data-table="'.$product->table_name.'">'.$product->name.'</a></td>
					<td>'.$product->barcode.'</td>
					<td>'.$product->sku.'</td>
					<td>'.$product->brand_name.'</td>
					<td>'.$product->supplier_name.'</td>
					<td>'.$product->purchase_cost.'</td>
                </tr>';
		}
		$html .= '</tbody>
				</table>';
		return response()->json(['getProductList'=>$html]);
    }
	public function changePStatus(Request $request)
    {
		$change = DB::table($request->table_name)->where('id', $request->id)->update(array('status' => $request->val));
		if($change){
			return response()->json(['success'=>'Status change successfully.']);
		}else{
			return response()->json(['success'=>'Status not change.']);
		}
    }
}
