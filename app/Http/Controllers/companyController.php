<?php

namespace App\Http\Controllers;

use App\Models\company;
use App\Models\delivery_address;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\File;

class companyController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
	public function index()
    {
		$company = DB::table('company')
				->select('*')
				->where('id', '1')
				->first();
		$delivery_address = delivery_address::all();	
		return view('company/company')->with('company',$company)->with('delivery_address',$delivery_address);
	}
	public function store(Request $request)
    {
		$request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
		/*if($request->hasFile('image')){

		 $file = $request->image;

		$file_new_name =  $file->move(url('public/images1'), $file->getClientOriginalName());

		$post->image = $file_new_names;

	   }*/
		//return $request;
		$res=company::find(1);
		if ($request->has('image')) {
			//$existing_image_path = 'https://po.nappies.co.nz/public/images/logo_image.png';
			$existing_image_path = 'public/images/'.$request->input('logo');
			
			if(File::exists($existing_image_path)) {
				File::delete($existing_image_path);
			}
			$imageName = time().'.'.$request->image->extension();  
			$request->image->move('public/images', $imageName);
			
			$res->logo=$imageName;
		}
		$res->name=$request->input('name');
		$res->phone=$request->input('phone');
		$res->email=$request->input('email');
		
		$res->pdf_prefix=$request->input('pdf_prefix');
		$res->pdf_footer=$request->input('pdf_footer');
		if($res->save()){
			foreach ($request->input('odId') as $n => $key){
				$odId = $request->input('odId')[$n];
				
				if($odId!=''){
					$res1=delivery_address::find($odId);
					$res1->delivery_address=$request->input('delivery_address')[$n];
					$res1->save();
				}else{
					$res1=new delivery_address;
					$res1->delivery_address=$request->input('delivery_address')[$n];
					$res1->is_default=0;
					$res1->save();
				}
			}
			session()->flash('msg', 'Data updated successfully');
			session()->flash('msgType', 'success');
			return redirect('company');
		}
	}
}
