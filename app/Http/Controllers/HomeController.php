<?php

namespace App\Http\Controllers;

use App\Models\settings;
use App\Models\delivery_address;
use Illuminate\Http\Request;
use Webfox\Xero\OauthCredentialManager;

use XeroAPI\XeroPHP\Api\AccountingApi;
use Webfox\Xero\Oauth2Provider;
use XeroAPI\XeroPHP\Api\IdentityApi;
use Illuminate\Support\Facades\Event;
use Webfox\Xero\Events\XeroAuthorized;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(OauthCredentialManager $xeroCredentials, AccountingApi $apiInstance)
    {
		$settings = DB::table('settings')
				->select('*')
				->where('id', '1')
				->first();
		try {
            // Check if we've got any stored credentials
            if ($xeroCredentials->exists()) {
                /* 
                 * We have stored credentials so we can resolve the AccountingApi, 
                 * If we were sure we already had some stored credentials then we could just resolve this through the controller
                 * But since we use this route for the initial authentication we cannot be sure!
                 */
                $xero             = resolve(\XeroAPI\XeroPHP\Api\AccountingApi::class);
                $organisationName = $xero->getOrganisations($xeroCredentials->getTenantId())->getOrganisations()[0]->getName();
                $user             = $xeroCredentials->getUser();
                $username         = "{$user['given_name']} {$user['family_name']} ({$user['username']})";
            }
        } catch (\throwable $e) {
            // This can happen if the credentials have been revoked or there is an error with the organisation (e.g. it's expired)
            $error = $e->getMessage();
        }
		//get tenantid and access token start
			$xeroTenantId = $xeroCredentials->getTenantId();
			$re = $apiInstance->getBrandingThemes($xeroTenantId);
			$result = json_decode(json_encode($re), true);
			//echo $re;exit;
			//echo '<pre>';print_r($result);echo '</pre>'; exit;
		//get tenantid and access token end
        return view('home/home', [
            'connected'        => $xeroCredentials->exists(),
            'error'            => $error ?? null,
            'organisationName' => $organisationName ?? null,
            'username'         => $username ?? null
        ])->with('settings',$settings)->with('result',$result);
		//return view('home/home')->with('settings',$settings);
    }
	public function store(Request $request)
    {
		//return $request;
		$res=settings::find(1);
		$res->admin_email=$request->input('admin_email');
		$res->cc_email=$request->input('cc_email');
		$res->bcc_email=$request->input('bcc_email');
		$res->gst=$request->input('gst');
		$res->xero_theme_id=$request->input('xero_theme_id');
		$res->xero_theme_name=$request->input('xero_theme_name');
		if($res->save()){
			session()->flash('msg', 'Data updated successfully');
			session()->flash('msgType', 'success');
			return redirect('home/');
		}
    }
	public function address_delete(Request $request , $id)
    {
		if(delivery_address::destroy(array('id',$id))){
			session()->flash('msg', 'Data deleted successfully');
			session()->flash('msgType', 'success');
		}else{
			session()->flash('msg', 'Data not deleted.');
			session()->flash('msgType', 'danger');
		}
		return redirect('home/');
	}
	public function DefaultAddress(Request $request)
    {
		$unsetdefault = DB::table('delivery_address')->update(array('is_default' => 0));
		$setdefault = DB::table('delivery_address')->where('id', $request->id)->update(array('is_default' => 1));
		if($setdefault){
			return 1;
		}else{
			return 2;
		}
    }
	public function callback(Request $request, OauthCredentialManager $oauth, IdentityApi $identity, Oauth2Provider $provider)
    {
		try {
            $this->validate($request, [
                'code'  => ['required', 'string'],
                'state' => ['required', 'string', "in:{$oauth->getState()}"]
            ]);

            $accessToken = $provider->getAccessToken('authorization_code', $request->only('code'));
            $identity->getConfig()->setAccessToken((string)$accessToken->getToken());
            $tenantId = $identity->getConnections()[0]->getTenantId();

            $oauth->store($accessToken, $tenantId);
            Event::dispatch(new XeroAuthorized($oauth->getData()));

            return $this->onSuccess();
        } catch (\throwable $e) {
            return $this->onFailure($e);
        }
	}
	public function onSuccess()
    {
        return Redirect::route(config('xero.oauth.redirect_on_success'));
    }

    public function onFailure(\throwable $e)
    {
        throw $e;
    }
}
