<?php

namespace App\Http\Controllers;

use App\Http\Controllers\messageLog;
use App\Models\Purchase_order_details;
use App\Models\email_management;
use App\Models\Purchase_order;
use App\Models\Supplier;
use App\Models\Product;
use App\Models\Product_custom;
use App\Models\delivery_address;
use Webfox\Xero\OauthCredentialManager;
use XeroAPI\XeroPHP\Api\AccountingApi;
use XeroAPI\XeroPHP\Models\Accounting\Contact;
use XeroAPI\XeroPHP\Models\Accounting\Contacts;
use XeroAPI\XeroPHP\Models\Accounting\ContactPerson;
//use Webfox\Xero\Events\XeroAuthorized;


use Illuminate\Http\Request;
use DB;
use PDF;
use Mail;
use File;

class PurchaseOrderController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function store_log($msg,$ref)
    {
        $messageLog = new messageLog;
		$result = $messageLog->store_log($msg,$ref);
		return $result;
		if($result==1){
			return 'success';
		}else{
			return 'error';
		}
    }
	
    public function create()
    {
		
		$settings = DB::table('settings')
				->select('*')
				->where('id', '1')
				->first();
		$supplierArr = DB::table('supplier')
				->select('supplier.*')
				->where('isActive', 0)
				->where('isDeleted', 0)
				->get();
        return view('purchase_order/create-order')->with('supplierArr',$supplierArr)->with('productArr',product::all())->with('delivery_address',delivery_address::all())->with('settings',$settings);
    }

    public function duplicate(Request $request)
    {
		switch ($request['action']) {
			case 'clone':
				$purchaseOrderTable = DB::table('purchase_order')
					->select('*')
					->where('ref_no', $request->input('ref_no')) 
					->first();
				
				$orderDetailsTable = DB::table('purchase_order_details')
					->select('*')
					->where('ref_no', $request->input('ref_no')) 
					->get();
					
				
				$settings = DB::table('settings')
					->select('*')
					->where('id', '1')
					->first();	
				$tCost = 0;
				
				$highestRef = DB::table('purchase_order')->max('ref_no');
				$highestRef = explode("-",$highestRef);
				if($highestRef[0]!=''){
					$ref_no = $highestRef[0]+1;
					$length = strlen($ref_no);
					if($length==1){
						$ref_no = '000'.$ref_no;
					}else if($length==2){
						$ref_no = '00'.$ref_no;
					}else if($length==3){
						$ref_no = '0'.$ref_no;
					}
				}else{
					$ref_no = '0001';
				}
				
				foreach($orderDetailsTable as $detailsVal){
					$res1=new purchase_order_details;
					$res1->ref_no=$ref_no;
					$res1->woocomerceId=$detailsVal->woocomerceId;
					$res1->product_name=$detailsVal->product_name;
					$res1->sku=$detailsVal->sku;
					$res1->supplier_code=$detailsVal->supplier_code;
					$res1->brand_name=$detailsVal->brand_name;
					$res1->barcode=$detailsVal->barcode;
					$res1->images=$detailsVal->images;
					$res1->uom=$detailsVal->uom;
					$res1->comment=$detailsVal->comment;
					$res1->quantity=$detailsVal->quantity;
					$res1->cost=$detailsVal->cost;
					$res1->total_cost=$detailsVal->total_cost;
					$res1->pType=$detailsVal->pType;
					$res1->save();
					
					$tCost = $tCost+$detailsVal->total_cost;
				}
				$expPoNo = explode('-',$request->input('ref_no'));
				if(!empty($expPoNo[1])){
					$supplierDetails = DB::table('supplier')
						->select('*')
						->where('id', $purchaseOrderTable->supplier_id)
						->first();		
					$supplierCost = DB::table('supplier_cost_details')
						->select('*')
						->where('supplier_id', $purchaseOrderTable->supplier_id) 
						->get();
					$noImagePath = asset('assets/dist/img/noimage.png');
					foreach($supplierCost as $supplierVal){
						$res2=new purchase_order_details;
						$res2->ref_no=$ref_no;
						$res2->product_name=$supplierVal->cost_label;
						$res2->images=$noImagePath;
						$res2->quantity=1;
						$res2->cost=$supplierVal->cost_value;
						$res2->total_cost=$supplierVal->cost_value;
						$res2->pType=1;
						$res2->save();
						
						$tCost = $tCost+$supplierVal->cost_value;
					}
				}
				
				$purchaseOrderNew = DB::table('purchase_order_details')
					->where('ref_no', $ref_no)
					->sum('total_cost');
				
				$res=new purchase_order;
				$res->ref_no=$ref_no;
				$res->parent_ref_no=$ref_no;
				$res->current_date=$purchaseOrderTable->current_date;
				$res->due_date=$purchaseOrderTable->due_date;
				$res->supplier_id=$purchaseOrderTable->supplier_id;
				$res->delivery_address=$purchaseOrderTable->delivery_address;
				$res->note=$purchaseOrderTable->note;
				$res->order_status=1;
				$res->tax=$tCost*$settings->gst/100;
				$res->tot_amt=$purchaseOrderNew;
				$res->save();
				
				//echo $purchaseOrderTable->order_status;exit;
				/*if($purchaseOrderTable->order_status===1){
					//pdf start
					$id = $res->id;
					$po = DB::table('purchase_order')
							->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
							->select('supplier.name as sName', 'purchase_order.*')
							->where('purchase_order.id', $id)
							->first();
					$poArr = DB::table('purchase_order')
							->join('purchase_order_details', 'purchase_order_details.ref_no', '=', 'purchase_order.ref_no')
							->where('purchase_order.id', $id)
							//->where('purchase_order_details.pType', 0)
							->get();
					$supplierArr = supplier::all();
					$productArr = product::all();
					$company = DB::table('company')
							->select('*')
							->where('id', '1')
							->first();
					//return view('purchase_order/pdf-order',compact('po','poArr','supplierArr','productArr','settings'));
					$pdf = \App::make('dompdf.wrapper');
					//$pdf->loadHTML('<h3>Customer Data</h3>');
					$pdf->loadView('purchase_order/pdf-order',compact('po','poArr','supplierArr','productArr','settings','company'));
					//return $pdf->stream();
					
				$company = DB::table('company')
					->select('*')
					->where('id', '1')
					->first();
					return $pdf->download($company->pdf_prefix.'-'.$ref_no.'.pdf');
					//pdf end
				}*/
				$this->store_log(0,0);
				$request->session()->flash('msg', 'Order created successfully');
				$request->session()->flash('msgType', 'success');
				return redirect('draft');
			break;
			case 'email':
				$getStatus = DB::table('purchase_order')
					->select('*')
					->where('ref_no', $request->input('ref_no')) 
					->first();
				if($getStatus->order_status==1){
					$update = DB::table('purchase_order')
						->where('ref_no', $request->input('ref_no'))
						->update(['order_status' => 2]);
				}
				//mail start
				$settings = DB::table('settings')
					->select('*')
					->where('id', '1')
					->first();
					//pdf start
				$id = $request->input('t_id');
				$po = DB::table('purchase_order')
						->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
						->select('supplier.name as sName', 'purchase_order.*')
						->where('purchase_order.id', $id)
						->first();
				$poArr = DB::table('purchase_order')
						->join('purchase_order_details', 'purchase_order_details.ref_no', '=', 'purchase_order.ref_no')
						->where('purchase_order.id', $id)
						//->where('purchase_order_details.pType', 0)
						->get();
				$supplierArr = supplier::all();
				$productArr = product::all();
				$company = DB::table('company')
						->select('*')
						->where('id', '1')
						->first();
				$pdf = \App::make('dompdf.wrapper');
				$pdf->loadView('purchase_order/pdf-order',compact('po','poArr','supplierArr','productArr','settings','company'));
				$path = public_path('pdf');
				$rand = rand(00000000, 99999999);
				$fileName =  $rand . '.' . 'pdf' ;
				$pdf->save($path . '/' . $fileName);
					//pdf end
				$adminEmail = $request->input('admin_email');
				$subject = $request->input('email_subject').' '.$request->input('ref_no');
				$body = $request->input('email_pre_body');
				$year = date('Y');
				
				$supplierMailList = DB::table('supplier')
					->select('*')
					->where('id', $getStatus->supplier_id) 
					->first();
				$emailArr = array();
				$getEmail = rtrim($request->input('email_one'), ',');
				$emailArr = explode(',', $getEmail);
				
				$cc_email = array();
				$ccEmail = rtrim($request->input('cc_email'), ',');
				$cc_email = explode(',', $ccEmail);
				
				$bcc_email = array();
				$bccEmail = rtrim($request->input('bcc_email'), ',');
				$bcc_email = explode(',', $bccEmail);
				
				
				if($request->input('sent_cc')){
					$adminCCEmail = $request->input('cc_email');
					$admin_cc = array();
					$admin_ccEmail = rtrim($adminCCEmail, ',');
					$admin_cc = explode(',', $admin_ccEmail);
					$cc_email = array_push($admin_cc,$cc_email);
				}
				/*$emailArr = array();
				if($request->input('email_one')!='') {
					$emailArr[] = $request->input('email_one');
				}
				if($request->input('email_two')!='') {
					$emailArr[] = $request->input('email_two');
				}
				if($request->input('email_three')!='') {
					$emailArr[] = $request->input('email_three');
				}*/
				$data["email"] = $emailArr;
				$data["cc_email"] = $cc_email;
				$data["bcc_email"] = $bcc_email;
				$data["title"] = $subject;
				$data["body"] = $body;
				$data["year"] = $year;
				$data["s_name"] = $supplierMailList->name;
				$data["adminEmail"] = $adminEmail;
				
				$files = [
					$path . '/' . $fileName,
				];
				Mail::send('purchase_order.sendmail', $data, function($message)use($data, $files, $adminEmail, $request){
					$message->to($data["email"])
							->subject($data["title"]);
					
					//if($request->input('sent_cc')){
					if($request->input('cc_email')!=''){
						$message->cc($data["cc_email"]);
					}
					if($request->input('bcc_email')!=''){
						$message->bcc($data["bcc_email"]);
					}
					//}	
					foreach($files as $file){
						$message->attach($file);
					}
				});
				if(File::exists($path . '/' . $fileName)) File::delete($path . '/' . $fileName);
				//mail end
				$request->session()->flash('msg', 'Order sent successfully');
				$request->session()->flash('msgType', 'success');
				return redirect('sent');
			break;
			case 'pdf':
				$id = $request->input('t_id');
				$settings = DB::table('settings')
						->select('*')
						->where('id', '1')
						->first();
				$po = DB::table('purchase_order')
						->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
						->select('supplier.name as sName', 'purchase_order.*')
						->where('purchase_order.id', $id)
						->first();
				$poArr = DB::table('purchase_order')
						->join('purchase_order_details', 'purchase_order_details.ref_no', '=', 'purchase_order.ref_no')
						->where('purchase_order.id', $id)
						//->where('purchase_order_details.pType', 0)
						->get();
				$supplierArr = supplier::all();
				$productArr = product::all();	
				$company = DB::table('company')
					->select('*')
					->where('id', '1')
					->first();
				return view('purchase_order/pdf-order',compact('po','poArr','supplierArr','productArr','settings','company'));
				$pdf = \App::make('dompdf.wrapper');
				//$pdf->loadHTML('<h3>Customer Data</h3>');
				$pdf->loadView('purchase_order/pdf-order',compact('po','poArr','supplierArr','productArr','settings','company'));
				//return $pdf->stream();
				
				return $pdf->download($company->pdf_prefix.'-'.$request->input('ref_no').'.pdf');
			break;
			case 'cancel':
				$ref = $request->input('ref_no');
				$id = $request->input('t_id');
				$setdefault = DB::table('purchase_order')->where('ref_no', $ref)->where('id', $id)->update(array('order_status' => 5));
				if($setdefault){
					$request->session()->flash('msg', 'Order cancel successfully');
					$request->session()->flash('msgType', 'success');
					return redirect('cancel');
				}else{
					$request->session()->flash('msg', 'Order not cancel');
					$request->session()->flash('msgType', 'danger');
					return redirect('order_view/'.$id);
				};
			break;
		}
	}
    public function store(Request $request)
    {
		switch ($request['action']) {
			case 'save':
				$getDate = str_replace('/', '-', $request->input('due_date'));
				$date=date('Y-m-d',strtotime($getDate));
				$getcDate = str_replace('/', '-', $request->input('current_date'));
				$datec=date('Y-m-d',strtotime($getcDate));
				
				$highestRef = DB::table('purchase_order')->max('ref_no');
				$highestRef = explode("-",$highestRef);
				if($highestRef[0]!=''){
					$ref_no = $highestRef[0]+1;
					$length = strlen($ref_no);
					if($length==1){
						$ref_no = '000'.$ref_no;
					}else if($length==2){
						$ref_no = '00'.$ref_no;
					}else if($length==3){
						$ref_no = '0'.$ref_no;
					}
				}else{
					$ref_no = '0001';
				}
				//echo $highestRef.'</br>';
				//echo $ref_no;exit;
				$res=new purchase_order;
				$res->ref_no=$ref_no;
				$res->parent_ref_no=$ref_no;
				$res->current_date=$datec;
				$res->due_date=$date;
				$res->supplier_id=$request->input('supplier_id');
				$res->delivery_address=$request->input('delivery_address');
				$res->note=$request->input('note');
				$res->order_status=1;
				$res->tot_amt=$request->input('tot_amt');
				$res->tax=$request->input('tax');
				if($res->save()){
					//$product_id = Input::get('product_id');
					foreach ($request->input('product_id') as $n => $key){
						$res1=new purchase_order_details;
						//$res1->purchase_order_id=$res->id;
						$res1->ref_no=$ref_no;
						$res1->woocomerceId=$request->input('product_id')[$n];
						$res1->product_name=$request->input('name')[$n];
						$res1->sku=$request->input('sku')[$n];
						$res1->supplier_code=$request->input('supplier_code')[$n];
						$res1->brand_name=$request->input('brand_name')[$n];
						$res1->barcode=$request->input('barcode')[$n];
						$res1->images=$request->input('images')[$n];
						$res1->uom=$request->input('uom')[$n];
						$res1->comment=$request->input('comment')[$n];
						$res1->quantity=$request->input('quantity')[$n];
						$res1->cost=$request->input('cost')[$n];
						$res1->total_cost=$request->input('total_cost')[$n];
						$res1->save();
					}
					$noImagePath = asset('assets/dist/img/noimage.png');
					foreach ($request->input('sName') as $s => $keyval){
						//echo $noImagePath;exit;
						$res2=new purchase_order_details;
						$res2->ref_no=$ref_no;
						$res2->product_name=$request->input('sName')[$s];
						$res2->images=$noImagePath;
						$res2->quantity=$request->input('sQuantity')[$s];
						$res2->cost=$request->input('sCost')[$s];
						$res2->total_cost=$request->input('sCost')[$s];
						$res2->pType=1;
						$res2->save();
					}
					$this->store_log(1,$ref_no);
					$request->session()->flash('msg', 'Data submitted successfully');
					$request->session()->flash('msgType', 'success');
					return redirect('draft');
				}else{
					$request->session()->flash('msg', 'Data not added');
					$request->session()->flash('msgType', 'danger');
					return redirect('draft');
				}
			break;
			case 'email':
				$getDate = str_replace('/', '-', $request->input('due_date'));
				$date=date('Y-m-d',strtotime($getDate));
				$getcDate = str_replace('/', '-', $request->input('current_date'));
				$datec=date('Y-m-d',strtotime($getcDate));
				
				$highestRef = DB::table('purchase_order')->max('ref_no');
				$highestRef = explode("-",$highestRef);
				if($highestRef[0]!=''){
					$ref_no = $highestRef[0]+1;
					$length = strlen($ref_no);
					if($length==1){
						$ref_no = '000'.$ref_no;
					}else if($length==2){
						$ref_no = '00'.$ref_no;
					}else if($length==3){
						$ref_no = '0'.$ref_no;
					}
					$parent_ref_no = $highestRef[0];
				}else{
					$ref_no = '0001';
					$parent_ref_no = $ref_no;
				}
				//echo $highestRef.'</br>';
				//echo $ref_no;exit;
				$res=new purchase_order;
				$res->ref_no=$ref_no;
				$res->parent_ref_no=$parent_ref_no;
				$res->current_date=$datec;
				$res->due_date=$date;
				$res->supplier_id=$request->input('supplier_id');
				$res->delivery_address=$request->input('delivery_address');
				$res->note=$request->input('note');
				$res->order_status=1;
				$res->tot_amt=$request->input('tot_amt');
				$res->tax=$request->input('tax');
				if($res->save()){
					//$product_id = Input::get('product_id');
					foreach ($request->input('product_id') as $n => $key){
						$res1=new purchase_order_details;
						//$res1->purchase_order_id=$res->id;
						$res1->ref_no=$ref_no;
						$res1->woocomerceId=$request->input('product_id')[$n];
						$res1->product_name=$request->input('name')[$n];
						$res1->sku=$request->input('sku')[$n];
						$res1->supplier_code=$request->input('supplier_code')[$n];
						$res1->brand_name=$request->input('brand_name')[$n];
						$res1->barcode=$request->input('barcode')[$n];
						$res1->images=$request->input('images')[$n];
						$res1->uom=$request->input('uom')[$n];
						$res1->comment=$request->input('comment')[$n];
						$res1->quantity=$request->input('quantity')[$n];
						$res1->cost=$request->input('cost')[$n];
						$res1->total_cost=$request->input('total_cost')[$n];
						$res1->save();
					}
					$noImagePath = asset('assets/dist/img/noimage.png');
					foreach ($request->input('sName') as $s => $keyval){
						$res2=new purchase_order_details;
						$res2->ref_no=$ref_no;
						$res2->product_name=$request->input('sName')[$s];
						$res2->images=$noImagePath;
						$res2->quantity=$request->input('sQuantity')[$s];
						$res2->cost=$request->input('sCost')[$s];
						$res2->total_cost=$request->input('sCost')[$s];
						$res2->pType=1;
						$res2->save();
					}
					$this->store_log(1,$ref_no);
					return redirect('edit-order-mail/'.$res->id);
				}else{
					$request->session()->flash('msg', 'Data no added');
					$request->session()->flash('msgType', 'danger');
					return redirect('sent');
				}
			break;
			case 'pdf':
				$getDate = str_replace('/', '-', $request->input('due_date'));
				$date=date('Y-m-d',strtotime($getDate));
				$getcDate = str_replace('/', '-', $request->input('current_date'));
				$datec=date('Y-m-d',strtotime($getcDate));
				
				$highestRef = DB::table('purchase_order')->max('ref_no');
				$highestRef = explode("-",$highestRef);
				if($highestRef[0]!=''){
					$ref_no = $highestRef[0]+1;
					$length = strlen($ref_no);
					if($length==1){
						$ref_no = '000'.$ref_no;
					}else if($length==2){
						$ref_no = '00'.$ref_no;
					}else if($length==3){
						$ref_no = '0'.$ref_no;
					}
					$parent_ref_no=$highestRef[0];
				}else{
					$ref_no = '0001';
					$parent_ref_no=$ref_no;
				}
				//echo $highestRef.'</br>';
				//echo $ref_no;exit;
				$res=new purchase_order;
				$res->ref_no=$ref_no;
				$res->parent_ref_no=$parent_ref_no;
				$res->current_date=$datec;
				$res->due_date=$date;
				$res->supplier_id=$request->input('supplier_id');
				$res->delivery_address=$request->input('delivery_address');
				$res->note=$request->input('note');
				$res->order_status=1;
				$res->tot_amt=$request->input('tot_amt');
				$res->tax=$request->input('tax');
				if($res->save()){
					//$product_id = Input::get('product_id');
					foreach ($request->input('product_id') as $n => $key){
						$res1=new purchase_order_details;
						//$res1->purchase_order_id=$res->id;
						$res1->ref_no=$ref_no;
						$res1->woocomerceId=$request->input('product_id')[$n];
						$res1->product_name=$request->input('name')[$n];
						$res1->sku=$request->input('sku')[$n];
						$res1->supplier_code=$request->input('supplier_code')[$n];
						$res1->brand_name=$request->input('brand_name')[$n];
						$res1->barcode=$request->input('barcode')[$n];
						$res1->images=$request->input('images')[$n];
						$res1->uom=$request->input('uom')[$n];
						$res1->comment=$request->input('comment')[$n];
						$res1->quantity=$request->input('quantity')[$n];
						$res1->cost=$request->input('cost')[$n];
						$res1->total_cost=$request->input('total_cost')[$n];
						$res1->save();
					}
					$noImagePath = asset('assets/dist/img/noimage.png');
					foreach ($request->input('sName') as $s => $keyval){
						//echo $noImagePath;exit;
						$res2=new purchase_order_details;
						$res2->ref_no=$ref_no;
						$res2->product_name=$request->input('sName')[$s];
						$res2->images=$noImagePath;
						$res2->quantity=$request->input('sQuantity')[$s];
						$res2->cost=$request->input('sCost')[$s];
						$res2->total_cost=$request->input('sCost')[$s];
						$res2->pType=1;
						$res2->save();
					}
					$this->store_log(2,$ref_no);
					$request->session()->flash('msg', 'Data submitted successfully');
					$request->session()->flash('msgType', 'success');
					return $this->createPdf($res->id,$ref_no);
					return redirect('draft');
				}else{
					$request->session()->flash('msg', 'Data not added');
					$request->session()->flash('msgType', 'danger');
					return redirect('draft');
				}
			break;
		}
    }
	
    public function createPdf($id,$ref_no)
	{
		$settings = DB::table('settings')
				->select('*')
				->where('id', '1')
				->first();
		$po = DB::table('purchase_order')
				->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
				->select('supplier.name as sName', 'purchase_order.*')
				->where('purchase_order.id', $id)
				->first();
		$poArr = DB::table('purchase_order')
				->join('purchase_order_details', 'purchase_order_details.ref_no', '=', 'purchase_order.ref_no')
				->where('purchase_order.id', $id)
				//->where('purchase_order_details.pType', 0)
				->get();
		$supplierArr = supplier::all();
		$productArr = product::all();
		$company = DB::table('company')
				->select('*')
				->where('id', '1')
				->first();
		//return view('purchase_order/pdf-order',compact('po','poArr','supplierArr','productArr','settings'));
		$pdf = \App::make('dompdf.wrapper');
		//$pdf->loadHTML('<h3>Customer Data</h3>');
		$pdf->loadView('purchase_order/pdf-order',compact('po','poArr','supplierArr','productArr','settings','company'));
		//return $pdf->stream();
					session()->flash('msg', 'Data submitted successfully');
					session()->flash('msgType', 'success');
					
		return $pdf->download($company->pdf_prefix.'-'.$ref_no.'.pdf');
	}
    public function edit_order_post(Request $request, Purchase_order $Purchase_order)
	{
		switch ($request['action']) {
			case 'save':
				$getDate = str_replace('/', '-', $request->input('due_date'));
				$date=date('Y-m-d',strtotime($getDate));
				$getcDate = str_replace('/', '-', $request->input('current_date'));
				$datec=date('Y-m-d',strtotime($getcDate));

				$res=purchase_order::find($request->id);
				$res->current_date=$datec;
				$res->due_date=$date;
				$res->supplier_id=$request->input('supplier_id');
				$res->delivery_address=$request->input('delivery_address');
				$res->note=$request->input('note');
				$res->order_status=1;
				$res->tot_amt=$request->input('tot_amt');
				$res->tax=$request->input('tax');
				$res->save();
				if($res->save()){
					foreach ($request->input('product_id') as $n => $key){
						$odId = $request->input('odId')[$n];
						if($odId!=''){
							$res1=purchase_order_details::find($odId);
							$res1->woocomerceId=$request->input('product_id')[$n];
							$res1->product_name=$request->input('name')[$n];
							$res1->sku=$request->input('sku')[$n];
							$res1->supplier_code=$request->input('supplier_code')[$n];
							$res1->brand_name=$request->input('brand_name')[$n];
							$res1->barcode=$request->input('barcode')[$n];
							$res1->images=$request->input('images')[$n];
							$res1->uom=$request->input('uom')[$n];
							$res1->comment=$request->input('comment')[$n];
							$res1->quantity=$request->input('quantity')[$n];
							$res1->cost=$request->input('cost')[$n];
							$res1->total_cost=$request->input('total_cost')[$n];
							$res1->save();
						}else{
							$res1=new purchase_order_details;
							//$res1->purchase_order_id=$res->id;
							$res1->ref_no=$request->input('ref_no');
							$res1->woocomerceId=$request->input('product_id')[$n];
							$res1->product_name=$request->input('name')[$n];
							$res1->sku=$request->input('sku')[$n];
							$res1->supplier_code=$request->input('supplier_code')[$n];
							$res1->brand_name=$request->input('brand_name')[$n];
							$res1->barcode=$request->input('barcode')[$n];
							$res1->images=$request->input('images')[$n];
							$res1->uom=$request->input('uom')[$n];
							$res1->comment=$request->input('comment')[$n];
							$res1->quantity=$request->input('quantity')[$n];
							$res1->cost=$request->input('cost')[$n];
							$res1->total_cost=$request->input('total_cost')[$n];
							$res1->save();
						}
					}
					DB::table('purchase_order_details')
						->where('ref_no', $request->input('ref_no'))
						->where('pType', 1)
						->delete();
					$noImagePath = asset('assets/dist/img/noimage.png');
					foreach ($request->input('sName') as $s => $keyval){
						$res2=new purchase_order_details;
						$res2->ref_no=$request->input('ref_no');
						$res2->product_name=$request->input('sName')[$s];
						$res2->images=$noImagePath;
						$res2->quantity=$request->input('sQuantity')[$s];
						$res2->cost=$request->input('sCost')[$s];
						$res2->total_cost=$request->input('sCost')[$s];
						$res2->pType=1;
						$res2->save();
					}
					$request->session()->flash('msg', 'Data updated successfully');
					$request->session()->flash('msgType', 'success');
					return redirect('edit-order/'.$request->id);
				}else{
					$request->session()->flash('msg', 'Data no added');
					$request->session()->flash('msgType', 'danger');
					return redirect('edit-order/'.$request->id);
				}
			break;
			case 'email':
				$getDate = str_replace('/', '-', $request->input('due_date'));
				$date=date('Y-m-d',strtotime($getDate));
				$getcDate = str_replace('/', '-', $request->input('current_date'));
				$datec=date('Y-m-d',strtotime($getcDate));

				$res=purchase_order::find($request->id);
				$res->current_date=$datec;
				$res->due_date=$date;
				$res->supplier_id=$request->input('supplier_id');
				$res->delivery_address=$request->input('delivery_address');
				$res->note=$request->input('note');
				$res->order_status=2;
				$res->tot_amt=$request->input('tot_amt');
				$res->tax=$request->input('tax');
				$res->save();
				if($res->save()){
					foreach ($request->input('product_id') as $n => $key){
						$odId = $request->input('odId')[$n];
						if($odId!=''){
							$res1=purchase_order_details::find($odId);
							$res1->woocomerceId=$request->input('product_id')[$n];
							$res1->product_name=$request->input('name')[$n];
							$res1->sku=$request->input('sku')[$n];
							$res1->supplier_code=$request->input('supplier_code')[$n];
							$res1->brand_name=$request->input('brand_name')[$n];
							$res1->barcode=$request->input('barcode')[$n];
							$res1->images=$request->input('images')[$n];
							$res1->uom=$request->input('uom')[$n];
							$res1->comment=$request->input('comment')[$n];
							$res1->quantity=$request->input('quantity')[$n];
							$res1->cost=$request->input('cost')[$n];
							$res1->total_cost=$request->input('total_cost')[$n];
							$res1->save();
						}else{
							$res1=new purchase_order_details;
							//$res1->purchase_order_id=$res->id;
							$res1->ref_no=$request->input('ref_no');
							$res1->woocomerceId=$request->input('product_id')[$n];
							$res1->product_name=$request->input('name')[$n];
							$res1->sku=$request->input('sku')[$n];
							$res1->supplier_code=$request->input('supplier_code')[$n];
							$res1->brand_name=$request->input('brand_name')[$n];
							$res1->barcode=$request->input('barcode')[$n];
							$res1->images=$request->input('images')[$n];
							$res1->uom=$request->input('uom')[$n];
							$res1->comment=$request->input('comment')[$n];
							$res1->quantity=$request->input('quantity')[$n];
							$res1->cost=$request->input('cost')[$n];
							$res1->total_cost=$request->input('total_cost')[$n];
							$res1->save();
						}
					}
					DB::table('purchase_order_details')
						->where('ref_no', $request->input('ref_no'))
						->where('pType', 1)
						->delete();
					$noImagePath = asset('assets/dist/img/noimage.png');
					foreach ($request->input('sName') as $s => $keyval){
						$res2=new purchase_order_details;
						$res2->ref_no=$request->input('ref_no');
						$res2->product_name=$request->input('sName')[$s];
						$res2->images=$noImagePath;
						$res2->quantity=$request->input('sQuantity')[$s];
						$res2->cost=$request->input('sCost')[$s];
						$res2->total_cost=$request->input('sCost')[$s];
						$res2->pType=1;
						$res2->save();
					}
					//mail start
					$settings = DB::table('settings')
						->select('*')
						->where('id', '1')
						->first();
						//pdf start
					$id = $res->id;
					$po = DB::table('purchase_order')
							->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
							->select('supplier.name as sName', 'purchase_order.*')
							->where('purchase_order.id', $id)
							->first();
					$poArr = DB::table('purchase_order')
							->join('purchase_order_details', 'purchase_order_details.ref_no', '=', 'purchase_order.ref_no')
							->where('purchase_order.id', $id)
							//->where('purchase_order_details.pType', 0)
							->get();
					$supplierArr = supplier::all();
					$productArr = product::all();
					
					$company = DB::table('company')
							->select('*')
							->where('id', '1')
							->first();
					$pdf = \App::make('dompdf.wrapper');
					$pdf->loadView('purchase_order/pdf-order',compact('po','poArr','supplierArr','productArr','settings','company'));
					$path = public_path('pdf');
					$rand = rand(00000000, 99999999);
					$fileName =  $rand . '.' . 'pdf' ;
					$pdf->save($path . '/' . $fileName);
						//pdf end
					$adminEmail = $request->input('admin_email');
					$subject = $request->input('email_subject').' '.$request->input('ref_no');
					$body = $request->input('email_pre_body');
					//$mailBody = str_replace(array("[NAME]","[PO]","[ADDRESS]"), array($poArr->name,'[PO]','[ADDRESS]'), $settings->email_pre_body);
					$year = date('Y');
					
					$supplierMailList = DB::table('supplier')
						->select('*')
						->where('id', $request->input('supplier_id')) 
						->first();
					//$body = str_replace(array("[NAME]","[PO]","[ADDRESS]"), array($supplierMailList->name,$po->ref_no,$request->input('delivery_address')), $settings->email_pre_body);
					$emailArr = array();
					$getEmail = rtrim($request->input('email_one'), ',');
					$emailArr = explode(',', $getEmail);
					
					$cc_email = array();
					$ccEmail = rtrim($request->input('cc_email'), ',');
					$cc_email = explode(',', $ccEmail);
					
					$bcc_email = array();
					$bccEmail = rtrim($request->input('bcc_email'), ',');
					$bcc_email = explode(',', $bccEmail);
					
					if($request->input('sent_cc')){
						$adminCCEmail = $request->input('cc_email');
						$admin_cc = array();
						$admin_ccEmail = rtrim($adminCCEmail, ',');
						$admin_cc = explode(',', $admin_ccEmail);
						$cc_email = array_push($admin_cc,$cc_email);
					}
					/*$emailArr = array();
					if($request->input('email_one')!='') {
						$emailArr[] = $request->input('email_one');
					}
					if($request->input('email_two')!='') {
						$emailArr[] = $request->input('email_two');
					}
					if($request->input('email_three')!='') {
						$emailArr[] = $request->input('email_three');
					}*/
					$data["email"] = $emailArr;
					$data["cc_email"] = $cc_email;
					$data["bcc_email"] = $bcc_email;
					$data["title"] = $subject;
					$data["body"] = $body;
					$data["year"] = $year;
					$data["s_name"] = $supplierMailList->name;
					$data["adminEmail"] = $adminEmail;
					
					$files = [
						$path . '/' . $fileName,
					];
					Mail::send('purchase_order.sendmail', $data, function($message)use($data, $files, $adminEmail, $request){
						$message->to($data["email"])
								->subject($data["title"]);
						
						//if($request->input('sent_cc')){
						if($request->input('cc_email')!=''){
							$message->cc($data["cc_email"]);
						}
						if($request->input('bcc_email')!=''){
							$message->bcc($data["bcc_email"]);
						}
						//}	
						foreach($files as $file){
							$message->attach($file);
						}
					});
					if(File::exists($path . '/' . $fileName)) File::delete($path . '/' . $fileName);
					//mail end
					$request->session()->flash('msg', 'Data sent successfully');
					$request->session()->flash('msgType', 'success');
					return redirect('sent');
				}else{
					$request->session()->flash('msg', 'Data no added');
					$request->session()->flash('msgType', 'danger');
					return redirect('edit-order/'.$request->id);
				}
			break;
			case 'pdf':
				$getDate = str_replace('/', '-', $request->input('due_date'));
				$date=date('Y-m-d',strtotime($getDate));
				$getcDate = str_replace('/', '-', $request->input('current_date'));
				$datec=date('Y-m-d',strtotime($getcDate));

				$res=purchase_order::find($request->id);
				$res->current_date=$datec;
				$res->due_date=$date;
				$res->supplier_id=$request->input('supplier_id');
				$res->delivery_address=$request->input('delivery_address');
				$res->note=$request->input('note');
				$res->order_status=1;
				$res->tot_amt=$request->input('tot_amt');
				$res->tax=$request->input('tax');
				$res->save();
				if($res->save()){
					foreach ($request->input('product_id') as $n => $key){
						$odId = $request->input('odId')[$n];
						if($odId!=''){
							$res1=purchase_order_details::find($odId);
							$res1->woocomerceId=$request->input('product_id')[$n];
							$res1->product_name=$request->input('name')[$n];
							$res1->sku=$request->input('sku')[$n];
							$res1->supplier_code=$request->input('supplier_code')[$n];
							$res1->brand_name=$request->input('brand_name')[$n];
							$res1->barcode=$request->input('barcode')[$n];
							$res1->images=$request->input('images')[$n];
							$res1->uom=$request->input('uom')[$n];
							$res1->comment=$request->input('comment')[$n];
							$res1->quantity=$request->input('quantity')[$n];
							$res1->cost=$request->input('cost')[$n];
							$res1->total_cost=$request->input('total_cost')[$n];
							$res1->save();
						}else{
							$res1=new purchase_order_details;
							//$res1->purchase_order_id=$res->id;
							$res1->ref_no=$request->input('ref_no');
							$res1->woocomerceId=$request->input('product_id')[$n];
							$res1->product_name=$request->input('name')[$n];
							$res1->sku=$request->input('sku')[$n];
							$res1->supplier_code=$request->input('supplier_code')[$n];
							$res1->brand_name=$request->input('brand_name')[$n];
							$res1->barcode=$request->input('barcode')[$n];
							$res1->images=$request->input('images')[$n];
							$res1->uom=$request->input('uom')[$n];
							$res1->comment=$request->input('comment')[$n];
							$res1->quantity=$request->input('quantity')[$n];
							$res1->cost=$request->input('cost')[$n];
							$res1->total_cost=$request->input('total_cost')[$n];
							$res1->save();
						}
					}
					DB::table('purchase_order_details')
						->where('ref_no', $request->input('ref_no'))
						->where('pType', 1)
						->delete();
					$noImagePath = asset('assets/dist/img/noimage.png');
					foreach ($request->input('sName') as $s => $keyval){
						$res2=new purchase_order_details;
						$res2->ref_no=$request->input('ref_no');
						$res2->product_name=$request->input('sName')[$s];
						$res2->images=$noImagePath;
						$res2->quantity=$request->input('sQuantity')[$s];
						$res2->cost=$request->input('sCost')[$s];
						$res2->total_cost=$request->input('sCost')[$s];
						$res2->pType=1;
						$res2->save();
					}
					$request->session()->flash('msg', 'Data updated successfully');
					$request->session()->flash('msgType', 'success');
					return $this->createPdf($res->id,$request->input('ref_no'));
					return redirect('edit-order/'.$request->id);
				}else{
					$request->session()->flash('msg', 'Data no added');
					$request->session()->flash('msgType', 'danger');
					return redirect('edit-order/'.$request->id);
				}
			break;
		}
	}
	public function partial(Request $request, Purchase_order $Purchase_order, OauthCredentialManager $oauth, AccountingApi $apiInstance, Contacts $ContactsApi, Contact $ContactApi, ContactPerson $ContactPerson)
	{
		$getDate = str_replace('/', '-', $request->input('due_date'));
		$date=date('Y-m-d',strtotime($getDate));
		$getcDate = str_replace('/', '-', $request->input('current_date'));
		$datec=date('Y-m-d',strtotime($getcDate));
		
		$expPoNo = explode('-',$request->input('ref_no'));
		if(empty($expPoNo[1])){
			$poNoSuffix = '-1';
		} else {
			$poNoSuffix = '-'.($expPoNo[1]+1);
		}
		
		$tCost = 0;
		$tSubCost = 0;
		$insert = '';
		foreach ($request->input('product_id') as $n => $key){
			$qtyToShip = $request->input('product_quantity')[$n];
            $packedQty = $request->input('noQty')[$n];
			if($qtyToShip!=$packedQty) {
				$res1=new purchase_order_details;
				//$res1->purchase_order_id=$res->id;
				$res1->ref_no=$expPoNo[0].$poNoSuffix;
				$res1->woocomerceId=$request->input('product_id')[$n];
				$res1->product_name=$request->input('product_name')[$n];
				$res1->sku=$request->input('sku')[$n];
				$res1->supplier_code=$request->input('supplier_code')[$n];
				$res1->brand_name=$request->input('brand_name')[$n];
				$res1->barcode=$request->input('barcode')[$n];
				$res1->images=$request->input('images')[$n];
				$res1->uom=$request->input('uom')[$n];
				$res1->quantity=$request->input('product_quantity')[$n]-$packedQty;
				$res1->cost=$request->input('cost')[$n];
				$res1->total_cost=($request->input('product_quantity')[$n]-$packedQty)*($request->input('cost')[$n]);
				$insert = $res1->save();
				if($insert){
					if($packedQty>0){
						$resUp=purchase_order_details::find($request->input('odId')[$n]);
						$resUp->quantity=$packedQty;
						$resUp->total_cost=$packedQty*$request->input('cost')[$n];
						$resUp->save();
					}else{
						purchase_order_details::destroy(array('id',$request->input('odId')[$n]));
					}
				}
				$tCost = $tCost+($request->input('product_quantity')[$n]-$packedQty)*($request->input('cost')[$n]);
				$tSubCost = $tSubCost+($packedQty*$request->input('cost')[$n]);
			}
					
			////woocomerce update start////
			//Fetch quantity start//
			$woocomercePId = $request->input('product_id')[$n];
			
			$data1 = array();                                                                    
			$data_string1 = json_encode($data1);                                                                                   

			$consumer_key = 'ck_85cf3eb73d3eeed9085350345008c6ad2c3baffa'; 
			$consumer_secret = 'cs_e061027c217ba8efc646f6396bb9857fe4214b4d'; 

			$ch = curl_init('https://dev.nappies.co.nz/wp-json/wc/v3/products/'.$woocomercePId.'?consumer_key='.$consumer_key.'&consumer_secret='.$consumer_secret);
			curl_setopt($ch, CURLOPT_USERPWD, $consumer_key . ":" . $consumer_secret);                                  
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string1);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                     
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string1))                                                                       
			); 
			$data1 = curl_exec($ch);
			$data1 = json_decode($data1);
			$data1 = response()->json([$data1]);
			$data1 = $data1->getData();
			$getQuantity = $data1[0]->stock_quantity;
			//Fetch quantity end//
			//Update quantity start//
			$updateQuantity = $getQuantity+$packedQty;
			
			$data='{"stock_quantity": "'.$updateQuantity.'"}';
			$ch = curl_init('https://dev.nappies.co.nz/wp-json/wc/v3/products/'.$woocomercePId.'?consumer_key='.$consumer_key.'&consumer_secret='.$consumer_secret);
			curl_setopt($ch, CURLOPT_USERPWD, $consumer_key . ":" . $consumer_secret);                                  
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                     
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data))                                                                       
			);                                                                                                                   

			$result = curl_exec($ch);
			//Update quantity end//
			////woocomerce update end////
		}
		$settings = DB::table('settings')
			->select('*')
			->where('id', '1')
			->first();
		if($insert){
			$totalQty = $request->input('totalQty');
            $subtotalQty = $request->input('subtotalQty');
			if($totalQty!=$subtotalQty){
				if($request->input('pOrder')=='yes'){
					$order_change_status = 4;
				}else if($request->input('pOrder')=='no'){
					$order_change_status = 5;
				}
			}
			$res2=new purchase_order;
			$res2->ref_no=$expPoNo[0].$poNoSuffix;
			$res2->parent_ref_no=$expPoNo[0];
			//$res2->ref_no=$request->input('ref_no');
			$res2->current_date=$datec;
			$res2->due_date=$date;
			$res2->supplier_id=$request->input('supplier_id');
			$res2->delivery_address=$request->input('delivery_address');
			$res2->note=$request->input('note');
			$res2->order_status=$order_change_status;
			$res2->tot_amt=$tCost;
			$res2->tax=$tCost*$settings->gst/100;
			$res2->save();
		}
		$sumAmnt = DB::table('purchase_order_details')
			->where('ref_no', $request->input('ref_no'))
			->sum('total_cost');
			
		$tax = $sumAmnt*$settings->gst/100;
		$res=purchase_order::find($request->id);
		$res->tot_amt=$sumAmnt;
		$res->tax=$tax;
		$res->order_status=3;
		$res->save();
		
	//create purchase order in xero start
		$receiveOrder = DB::table('purchase_order')
			->select('*')
			->where('ref_no', $request->input('ref_no'))
			->where('order_status', 3)
			->first();
		$receiveOrderDetails = DB::table('purchase_order_details')
			->select('*')
			->where('ref_no', $request->input('ref_no'))
			->get();
		//get tenantid and access token start
			$xeroTenantId = $oauth->getTenantId();
			$xeroAccessToken = $oauth->getAccessToken();
		//get tenantid and access token end
		//contact create start
			$supplierXeroContact = DB::table('supplier')
				->select('*')
				->where('id', $request->input('supplier_id')) 
				->first();
			if($supplierXeroContact->xeroContactId==''){
				$person = $ContactPerson->setFirstName($supplierXeroContact->person_name);
				$arr_persons = [];
				array_push($arr_persons, $person);
				
				$semail = array();
				$sEmail = rtrim($supplierXeroContact->email, ',');
				$semail = explode(',', $sEmail);
				$supplier_mail = $semail[0];
				//echo $supplier_mail;
				$c = $ContactApi->setName($supplierXeroContact->name)
					->setFirstName($supplierXeroContact->name)
					->setEmailAddress($supplier_mail)
					->setContactPersons($arr_persons);

				$arr_contacts = [];
				array_push($arr_contacts, $c);
				$contacts = $ContactsApi->setContacts($arr_contacts);
				$apiResponse = $apiInstance->createContacts($xeroTenantId,$contacts);
				$contactId = $apiResponse->getContacts()[0]->getContactId();
				//add xeroContactId in supplier table
				$supplierXero = DB::table('supplier')->where('id', $request->input('supplier_id'))->update(array('xeroContactId' => $contactId));
			}else{
				$contactId = $supplierXeroContact->xeroContactId;
			}
			//echo $xeroTenantId."==".$apiResponse;
		//contact create end
		//purchase order create start
			$purchaseOrderStrVal = array();
			$array = array();
			foreach($receiveOrderDetails as $receiveOrderVal){
				$purchaseOrderStrVal = array(
					//"ItemCode"=> $receiveOrderVal->product_name,
					"Description"=> $receiveOrderVal->product_name.' ('.$receiveOrderVal->product_name.')',
					"Quantity"=> $receiveOrderVal->quantity,
					"UnitAmount"=> $receiveOrderVal->cost,
					"TaxType"=> "INPUT2",
					"TaxAmount"=> $settings->gst
				);
				$array[]=$purchaseOrderStrVal;
			}
			$d= array(
				  "Contact"=> array( "ContactID"=> $contactId ),
				  "Date"=> $receiveOrder->current_date,
				  "DeliveryDate"=> $receiveOrder->due_date,
				  "LineAmountTypes"=> "Exclusive",
				  "LineItems"=> $array,
				  "DeliveryAddress"=> $receiveOrder->delivery_address
				);

			$purchaseOrderArr = json_encode($d);
			$purchaseOrderArr = json_decode($purchaseOrderArr);
			$result = $apiInstance->createPurchaseOrders($xeroTenantId,$purchaseOrderArr);
			//echo $result;exit;
		//purchase order create end
	//create purchase order in xero end
		$request->session()->flash('msg', 'Data received successfully');
		$request->session()->flash('msgType', 'success');
		return redirect('received');
	}
	
    public function draft()
	{
		$poArr = DB::table('purchase_order')
				->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
				->select('supplier.name as sName', 'purchase_order.*')
				->where('purchase_order.order_status', '1')
				->get();
				//return $poArr;
		return view('purchase_order/draft', compact('poArr'));
	}
    public function orders()
	{
		$poArr = DB::table('purchase_order')
				->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
				->select('supplier.name as sName', 'purchase_order.*')
				//->where('purchase_order.order_status', '1')
				->get();
				//return $poArr;
		return view('purchase_order/orders', compact('poArr'));
	}
    public function sent()
	{
		$poArr = DB::table('purchase_order')
				->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
				->select('supplier.name as sName', 'purchase_order.*')
				->where('purchase_order.order_status', '2')
				->get();
				//return $poArr;
		return view('purchase_order/sent', compact('poArr'));
	}
    public function received()
	{
		$poArr = DB::table('purchase_order')
				->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
				->select('supplier.name as sName', 'purchase_order.*')
				->where('purchase_order.order_status', '3')
				->get();
				//return $poArr;
		return view('purchase_order/received', compact('poArr'));
	}
    public function cancel()
	{
		$poArr = DB::table('purchase_order')
				->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
				->select('supplier.name as sName', 'purchase_order.*')
				->where('purchase_order.order_status', '5')
				->get();
				//return $poArr;
		return view('purchase_order/cancel', compact('poArr'));
	}
    public function partial_received()
	{
		/*$poArr = purchase_order::Where('order_status', "1")->get();
		//$product = $poArr->product_id;
		return view('purchase_order/draft', compact('poArr'));*/
		$poArr = DB::table('purchase_order')
				//->join('purchase_order_details', 'purchase_order_details.purchase_order_id', '=', 'purchase_order.id')
				//->join('product', 'product.id', '=', 'purchase_order_details.product_id')
				->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
				->select('supplier.name as sName', 'purchase_order.*')
				->where('purchase_order.order_status', '4')
				->get();
				//return $poArr;
		return view('purchase_order/partial-received', compact('poArr'));
	}
    public function edit_order($id)
	{
		$po = DB::table('purchase_order')
				->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
				->select('supplier.name as sName', 'purchase_order.*')
				->where('purchase_order.id', $id)
				->first();
		$poArr = DB::table('purchase_order')
				->join('purchase_order_details', 'purchase_order_details.ref_no', '=', 'purchase_order.ref_no')
				//->join('product', 'product.id', '=', 'purchase_order_details.product_id')
				->select('purchase_order_details.*')
				->where('purchase_order.id', $id)
				->where('purchase_order_details.pType', 0)
				->get();
		$poArrCost = DB::table('purchase_order')
				->join('purchase_order_details', 'purchase_order_details.ref_no', '=', 'purchase_order.ref_no')
				//->join('product', 'product.id', '=', 'purchase_order_details.product_id')
				->select('purchase_order_details.*')
				->where('purchase_order.id', $id)
				->where('purchase_order_details.pType', 1)
				->get();
		$settings = DB::table('settings')
				->select('*')
				->where('id', '1')
				->first();	
		$supplierArr = DB::table('supplier')
				->select('supplier.*')
				->where('isActive', 0)
				->where('isDeleted', 0)
				->get();
		$supplierDetails = DB::table('purchase_order')
						->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
						->select('supplier.*')
						->where('purchase_order.id', $id)
						->first();
		$email_management = email_management::all();
		return view('purchase_order/edit-order')->with('po',$po)->with('poArr',$poArr)->with('supplierArr',$supplierArr)->with('poArrCost',$poArrCost)->with('delivery_address',delivery_address::all())->with('settings',$settings)->with('supplierDetails',$supplierDetails)->with('email_management',$email_management);
	}
    public function edit_order_mail($id)
	{
		$po = DB::table('purchase_order')
				->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
				->select('supplier.name as sName', 'purchase_order.*')
				->where('purchase_order.id', $id)
				->first();
		$poArr = DB::table('purchase_order')
				->join('purchase_order_details', 'purchase_order_details.ref_no', '=', 'purchase_order.ref_no')
				//->join('product', 'product.id', '=', 'purchase_order_details.product_id')
				->select('purchase_order_details.*')
				->where('purchase_order.id', $id)
				->where('purchase_order_details.pType', 0)
				->get();
		$poArrCost = DB::table('purchase_order')
				->join('purchase_order_details', 'purchase_order_details.ref_no', '=', 'purchase_order.ref_no')
				//->join('product', 'product.id', '=', 'purchase_order_details.product_id')
				->select('purchase_order_details.*')
				->where('purchase_order.id', $id)
				->where('purchase_order_details.pType', 1)
				->get();
		$settings = DB::table('settings')
				->select('*')
				->where('id', '1')
				->first();	
		//$supplierArr = supplier::all();
		$supplierArr = DB::table('supplier')
				->select('supplier.*')
				->where('isActive', 0)
				->where('isDeleted', 0)
				->get();
		$supplierDetails = DB::table('purchase_order')
						->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
						->select('supplier.*')
						->where('purchase_order.id', $id)
						->first();
		$email_management = email_management::all();
		return view('purchase_order/edit-order-mail')->with('po',$po)->with('poArr',$poArr)->with('supplierArr',$supplierArr)->with('poArrCost',$poArrCost)->with('delivery_address',delivery_address::all())->with('settings',$settings)->with('supplierDetails',$supplierDetails)->with('email_management',$email_management);
	}
	public function view(Purchase_order $Purchase_order,$id)
    {	
		$settings = DB::table('settings')
				->select('*')
				->where('id', '1')
				->first();
		$po = DB::table('purchase_order')
				->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
				->select('supplier.name as sName', 'purchase_order.*')
				->where('purchase_order.id', $id)
				->first();
		
		$highestRef = explode("-",$po->ref_no);
		if($highestRef[0]!=''){
			$parent_ref_no = $highestRef[0];
			$related_ref = DB::table('purchase_order')
				->where('parent_ref_no', $parent_ref_no)
				->where('ref_no', '!=' , $po->ref_no)
				->get();
		}
				
		$poArr = DB::table('purchase_order')
				->join('purchase_order_details', 'purchase_order_details.ref_no', '=', 'purchase_order.ref_no')
				->where('purchase_order.id', $id)
				//->where('purchase_order_details.pType', 0)
				->get();
		$supplierArr = DB::table('purchase_order')
						->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
						->select('supplier.*')
						->where('purchase_order.id', $id)
						->first();
		//$supplierArr = supplier::all();
		$productArr = product::all();
		$email_management = email_management::all();
		return view('purchase_order/view-order')->with('po',$po)->with('poArr',$poArr)->with('supplierArr',$supplierArr)->with('productArr',$productArr)->with('related_ref',$related_ref)->with('settings',$settings)->with('email_management',$email_management);
    }
    public function prepare_order($id)
	{
		$po = DB::table('purchase_order')
				->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
				->select('supplier.name as sName', 'purchase_order.*')
				->where('purchase_order.id', $id)
				->first();
		$poArr = DB::table('purchase_order')
				->join('purchase_order_details', 'purchase_order_details.ref_no', '=', 'purchase_order.ref_no')
				//->join('product', 'product.id', '=', 'purchase_order_details.product_id')
				//->select('product.name as pName', 'purchase_order_details.*')
				->where('purchase_order.id', $id)
				->where('purchase_order_details.pType', 0)
				->get();
		$supplierArr = supplier::all();
		$productArr = product::all();
		//return $poArr;		
		/*$data['po']$po;
		$data['poArr']=$poArr;
		return view('purchase_order/sent',['data'=>$data]);*/
		return view('purchase_order/prepare-order')->with('po',$po)->with('poArr',$poArr)->with('supplierArr',$supplierArr)->with('productArr',$productArr);
		//return view('purchase_order/edit-order', compact('po','poArr','supplierArr','productArr'));
	}
	
    public function show(Supplier $supplier)
    {
        //
    }

    public function edit(Supplier $supplier,$id)
    {
        //
    }

    public function update(Request $request, Supplier $supplier)
    {
        //
    }

    public function destroy(Supplier $supplier , $id)
    {
        //
    }
	public function od_delete(Request $request , $id, $pId)
    {
		$cost = DB::table('purchase_order_details')
				->select('cost')
				->where('id', $id)
				->first();
		$tot_amt = DB::table('purchase_order')
				->select('tot_amt')
				->where('id', $pId)
				->first();
		$res=purchase_order::find($pId);
		$res->tot_amt=$tot_amt->tot_amt - $cost->cost;
		if($res->save()){
			if(purchase_order_details::destroy(array('id',$id))){
				session()->flash('msg', 'Data deleted successfully');
				session()->flash('msgType', 'success');
			}else{
				session()->flash('msg', 'Data not deleted.');
				session()->flash('msgType', 'danger');
			}
		}else{
			session()->flash('msg', 'Data not deleted.');
			session()->flash('msgType', 'danger');
		}
		return redirect('edit-order/'.$pId);
    }
	public function sent_order(Request $request,$id)
    {
		//$id = $request->input( 'id' );
        $res=purchase_order::find($id);
		$res->order_status=2;
		if($res->save()){
			$request->session()->flash('msg', 'Order sent successfully.');
			$request->session()->flash('msgType', 'success');
			return response()->json(['status'=>'Success']);
		}else{
			$request->session()->flash('msg', 'Order not sent.');
			$request->session()->flash('msgType', 'danger');
			return response()->json(['status'=>'Error']);
		}
    }
	
	public function getBarcode(Request $request)
    {
		$ref_no = $request->input( 'ref_no' );
		$barcode = $request->input( 'barcode' );
		$poArr = DB::table('purchase_order_details')
				->select('purchase_order_details.*')
				->where('ref_no', $ref_no)
				->where('barcode', $barcode)
				->first();
		//$poArr = $request->input( 'ref_no' )."/".$request->input( 'barcode' );
		return response()->json($poArr, 200);
		return $poArr;
    }
	public function getSDetails(Request $request)
    {
		$settings = DB::table('settings')
				->select('*')
				->where('id', '1')
				->first();
		$poArr = DB::table('supplier')
				->select('*')
				->where('id', $request->supplier_id)
				->first();
		$ccEmail = trim($poArr->cc_email,',').','.trim($settings->cc_email,',');
		$sMailDetails = '';
		if($request->ref_no!='' && $request->email_subject!=''){
			$email = DB::table('email_management')
				->select('*')
				->where('emailSubject', $request->email_subject)
				->first();
			$mailBody = str_replace(array("[NAME]","[PO]","[ADDRESS]"), array($poArr->name,$request->ref_no,$request->delivery_address), $email->emailBody);
			$sMailDetails .='<div class="col-md-12">
							<div class="form-group">
								<label>Email body</label>
								<textarea class="form-control" id="email_pre_body" name="email_pre_body" placeholder="Email body pre field" required>'.$mailBody.'</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Supplier Mail</label>
								<input type="text" class="form-control mb-1" name="email_one" value="'.$poArr->email.'" placeholder="Supplier Email" required>';
								if($poArr->cc_email!=''){
								$sMailDetails .='<label>CC Mail</label>
								<input type="text" class="form-control mb-1" name="cc_email" value="'.$ccEmail.'" placeholder="Supplier Email">';
								}
								if($settings->bcc_email!=''){
								$sMailDetails .='<label>BCC Mail</label>
								<input type="text" class="form-control mb-1" name="bcc_email" value="'.$settings->bcc_email.'" placeholder="BCC Mail">';
								}
							$sMailDetails .='</div>
						</div>';
		
		}
		$sDetails = '';
		$sDetails .= '<div class="col-md-12"><h3>Supplier Details : </h3></div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Name : </label> '.$poArr->name.'
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Phone : </label> '.$poArr->person_phone.'
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Email : </label> '.$poArr->email.'
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Note : </label> '.$poArr->notes.'
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Address : </label> '.$poArr->address.'
						</div>
					</div>';
		/*$purchases = DB::table('supplier_cost_details')
				->where('supplier_id', $request->supplier_id)
				->sum('cost_value');*/
		$purchases = DB::table('supplier_cost_details')
				->select('*')
				->where('supplier_id', $request->supplier_id) 
				->get();		
		$sCost = '';
		$rowCost = 1;
		foreach($purchases as $purchasesVal){
			$sCost .= '<div id="rowCost'.$rowCost.'" class="row">
						<div class="col75">
							<div class="form-group">
								<input type="text" class="form-control" name="sName[]" value="'.$purchasesVal->cost_label.'">
							</div>
						</div>
						<div class="col10">
							<div class="form-group">
								<input type="text" class="form-control" name="sQuantity[]" value="1">
							</div>
						</div>
						<div class="col10">
							<div class="form-group">
								<input type="text" class="form-control purchaseCost total_amount cost_total" name="sCost[]" value="'.$purchasesVal->cost_value.'">
							</div>
						</div>
						<div class="col5">
							<button onclick="eCostRow('.$rowCost.')" type="button" name="btnSubmit" class="btn btn-danger pull-right btn_remove"><i class="fa fa-trash" style="color:#fff"></i></button>
						</div>
					</div>';
			$rowCost++;
		}
		return response()->json(['sDetails'=>$sDetails,'sCost'=>$sCost,'sMailDetails'=>$sMailDetails]);
    }
	public function searchProduct(Request $request)
    {
		$input = $request->term;
		$product = DB::table('product')
				->select('*')
				->where('status', 0)
				->where('delete_status', 0)
				->where(function($query) use ($input){
					$query->where('product_id', 'LIKE', '%'.$input.'%');
					$query->orWhere('name', 'LIKE', '%'.$input.'%');
					$query->orWhere('sku', 'LIKE', '%'.$input.'%');
					$query->orWhere('supplier_code', 'LIKE', '%'.$input.'%');
					$query->orWhere('barcode', 'LIKE', '%'.$input.'%');
				})
				->get();
		$productCustom = DB::table('product_custom')
				->select('*')
				->where('status', 0)
				->where('delete_status', 0)
				->where(function($query) use ($input){
					$query->where('product_id', 'LIKE', '%'.$input.'%');
					$query->orWhere('name', 'LIKE', '%'.$input.'%');
					$query->orWhere('sku', 'LIKE', '%'.$input.'%');
					$query->orWhere('supplier_code', 'LIKE', '%'.$input.'%');
					$query->orWhere('barcode', 'LIKE', '%'.$input.'%');
				})
				->limit(10)
				->get();
		$poArr = array_merge(json_decode($product),json_decode($productCustom));
		$domainData = array();
foreach($poArr as $poArrVal)
{
	$data['id'] 		= $poArrVal->product_id;
	$data['value'] 		= $poArrVal->name.' ('.$poArrVal->sku.')';
	$data['table'] 		= $poArrVal->table_name;
	array_push($domainData, $data);
}

// Return results as json encoded array
echo json_encode($domainData);
		/*$var = '';
		$var .= '<ul class="name-list" id="list">';
		if(!empty($poArr)){
			$p = 1;
			foreach($poArr as $poArrVal){
				if($p==1){
					$active = 'active';
				}else{
					$active = '';
				}
				$var .= '<li class="'.$active.'" onClick="selectProduct('.$poArrVal->product_id.',\''.$poArrVal->table_name.'\');">'.$poArrVal->name.' ('.$poArrVal->sku.')</li>';
				$p++;
			}
		}else{
			$var .= '<li onClick="selectProduct();">No Match Found</li>';
		}
		$var .= '</ul>';*/
		
		/*$var = '';
		if(!empty($poArr)){
			foreach($poArr as $poArrVal){
				$var .= '<option value="'.$poArrVal->product_id.'|'.$poArrVal->table_name.'">'.$poArrVal->name.' ('.$poArrVal->sku.')</option>';
			}
		}*/
		/*$array = [];
		if(!empty($poArr)){
			foreach($poArr as $poArrVal){
				$array[] = [
					'value' => $poArrVal->table_name.' ('.$poArrVal->sku.')',
				];
			}
		}
		return json_encode($array);*/
		//return $var;
    }
	public function getProduct(Request $request)
    {
		$poArr = DB::table($request->table_name)
				->select('*')
				->where('product_id', $request->id)
				->first();
		//$poArr = response()->json($poArr);
		$rowId = $request->rowId;
		$pDetails = '';
		if($poArr->purchase_cost=='' || $poArr->purchase_cost==0){
			$pCost = 1;
		}else{
			$pCost = $poArr->purchase_cost;
		}
		$pDetails .= '
					<div class="col5">
						<div class="form-group thumbImage">
							<div style="height:50px;width:50px;">
								 <img src="'.$poArr->images.'" class="zoom" alt="No Image" width="50" height="50" />
							</div>
						</div>
					</div>
					<div class="col30">
						<div class="form-group">
							<input type="text" class="form-control" value="'.$poArr->name.' ('.$poArr->sku.')" readonly>
							<input type="hidden" id="search_product_id" class="" value="'.$poArr->product_id.'">
							<input type="hidden" id="search_product_table" class="" value="'.$poArr->table_name.'">
						</div>
					</div>
					<div class="col15">
						<div class="form-group">
							<input type="text" class="form-control" value="'.$poArr->supplier_name.'" readonly>
						</div>
					</div>
					<div class="col10">
						<div class="form-group">
							<input type="text" class="form-control" value="'.$poArr->barcode.'" readonly>
						</div>
					</div>
					<div class="col10">
						<div class="form-group">
							<input type="text" class="form-control" value="'.$poArr->threshold.'" readonly>
						</div>
					</div>
					<div class="col10">
						<div class="form-group">
							<input type="text" class="form-control" value="'.$poArr->current_stock.'" readonly>
						</div>
					</div>
					<div class="col10">
						<div class="form-group">
							<input type="text" id="search_quantity" class="form-control">
						</div>
					</div>
					<div class="col10">
						<div class="form-group">
							<input type="text" id="search_cost" value="'.$pCost.'" class="form-control">
							<input type="hidden" id="search_total_amount" class="form-control">
						</div>
					</div>';
		return response()->json(['pDetails'=>$pDetails,'result'=>'300']);
    }
	public function getProductList(Request $request)
    {
		$poArr = DB::table($request->p_table)
				->select('*')
				->where('product_id', $request->id)
				->first();
		//$poArr = response()->json($poArr);
		$rowId = $request->rowId;
		$pDetails = '';
		$pDetails .= '
					<div class="col5">
						<div class="form-group thumbImage">
							<div style="height:50px;width:50px;">
								 <img src="'.$poArr->images.'" class="zoom" alt="No Image" width="50" height="50" />
								<input type="hidden" name="images[]" value="'.$poArr->images.'" readonly>
							</div>
						</div>
					</div>
					<div class="col30">
						<div class="form-group">
							<input type="text" class="form-control" name="" value="'.$poArr->name.' ('.$poArr->sku.')" readonly>
							<input type="hidden" id="product_id'.$rowId.'"name="product_id[]" class="product_id" value="'.$poArr->product_id.'">
							<input type="hidden" name="brand_name[]" value="'.$poArr->brand_name.'" readonly>
							<input type="hidden" name="name[]" value="'.$poArr->name.'" readonly>
							<input type="hidden" name="sku[]" value="'.$poArr->sku.'" readonly>
                            <input type="hidden" name="odId[]" value="">
						</div>
					</div>
					<div class="col15">
						<div class="form-group">
							<input type="text" class="form-control" name="supplier_code[]" value="'.$poArr->supplier_code.'" readonly>
						</div>
					</div>
					<div class="col10">
						<div class="form-group">
							<input type="text" class="form-control" name="barcode[]" value="'.$poArr->barcode.'" readonly>
						</div>
					</div>
					<div class="col5">
						<div class="form-group">
							<input type="text" id="uom'.$rowId.'" class="form-control uom" name="uom[]" autofocus>
						</div>
					</div>
					<div class="col10">
						<div class="form-group">
							<input type="text" class="form-control quantity" value="" id="quantity'.$rowId.'" name="quantity[]" required>
						</div>
					</div>
					<div class="col10">
						<div class="form-group">
							<input type="text" class="form-control cost" value="'.$poArr->purchase_cost.'" id="cost'.$rowId.'" name="cost[]" required>
						</div>
					</div>';
		$sTotal = '';
		$sTotal .= '<div class="col10">
						<div class="form-group">
							<input type="text" id="total_amount'.$rowId.'" name="total_cost[]" value="" class="form-control total_amount item_total">
						</div>
					</div>';
		return response()->json(['pDetails'=>$pDetails,'sTotal'=>$sTotal,'result'=>'300']);
    }
}
