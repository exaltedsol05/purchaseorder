<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('product/product')->with('productArr',product::all());
    }
	
    public function create()
    {
        return view('product/add-product');
    }

    public function store(Request $request)
    {
		$sku = preg_split("/\s+/", $request->input('name'));
		$getSku = "MB-";

		foreach ($sku as $s) {
		  $getSku .= $s[0];
		}
		
		$consumer_key = 'ck_85cf3eb73d3eeed9085350345008c6ad2c3baffa'; 
		$consumer_secret = 'cs_e061027c217ba8efc646f6396bb9857fe4214b4d'; 
		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://dev.nappies.co.nz/wp-json/wc/v3/products/?consumer_key='.$consumer_key.'&consumer_secret='.$consumer_secret,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		// CURLOPT_POSTFIELDS => "{\n  \"name\": \"Premium Quality\",\n  \"type\": \"variable\",\n  \"sku\" : \"123450000\"\n}",
		//CURLOPT_POSTFIELDS => "{\n  \"name\": \"".$request->input('name')."\",\n  \"type\": \"variable\",\n  \"sku\" : \"".$getSku."\"\n}",
		//CURLOPT_POSTFIELDS => "{\n  \"name\": \"".$request->input('name')."\",\n  \"sku\": \"".$getSku."\",\n  \"purchase_price\": \"".$request->input('purchase_cost')."\",\n  \"regular_price\": \"".$request->input('regular_price')."\",\n  \"manage_stock\": \"".$request->input('manage_stock')."\",\n  \"stock_quantity\": \"".$request->input('current_stock')."\",\n  \"out_stock_threshold\": \"".$request->input('threshold')."\"\n}",
		CURLOPT_POSTFIELDS => "{\n  \"name\": \"".$request->input('name')."\",\n  \"sku\": \"".$getSku."\",\n  \"purchase_price\": \"".$request->input('purchase_cost')."\",\n  \"regular_price\": \"".$request->input('regular_price')."\",\n  \"manage_stock\": \"".$request->input('manage_stock')."\",\n  \"stock_quantity\": \"".$request->input('current_stock')."\"\n}",
		CURLOPT_HTTPHEADER => array(
			"Authorization: Basic ",
			"Content-Type: application/json",
			"cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		
		if ($err) {
			session()->flash('msg', 'Product addded successfully.');
			session()->flash('msgType', 'success');
			return redirect('products');
		} else {
			session()->flash('msg', 'Product not added.');
			session()->flash('msgType', 'danger');
			return redirect('products');
		}
		/*$sku = preg_split("/\s+/", "Community - College District yuio");
		$getSku = "MB-";

		foreach ($sku as $s) {
		  $getSku .= $s[0];
		}
		$res=new product;
		$res->name=$request->input('name');
		$res->sku=$getSku;
		$res->purchase_cost=$request->input('purchase_cost');
		$res->regular_price=$request->input('regular_price');
		$res->manage_stock=$request->input('manage_stock');
		$res->current_stock=$request->input('current_stock');
		$res->threshold=$request->input('threshold');
		//if($res->save()){
			
			$consumer_key = 'ck_85cf3eb73d3eeed9085350345008c6ad2c3baffa'; 
			$consumer_secret = 'cs_e061027c217ba8efc646f6396bb9857fe4214b4d'; 

			$aData = array(
				array(
				'name' => $request->input('name'),
				'sku' => $getSku,
				'purchase_price' => $request->input('purchase_cost'),
				'regular_price' => $request->input('regular_price'),
				'manage_stock' => $request->input('manage_stock'),
				'stock_quantity' => $request->input('current_stock'),
				'out_stock_threshold' => $request->input('threshold')

				)
			);
			$sData = json_encode($aData);
			$ch = curl_init('https://dev.nappies.co.nz/wp-json/wc/v3/products/?consumer_key='.$consumer_key.'&consumer_secret='.$consumer_secret);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $sData);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_USERPWD, $consumer_key . ":" . $consumer_secret);                           
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                             
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($sData))                                                                       
			); 
			$result = curl_exec($ch);
			return json_decode($result);
			session()->flash('msg', 'Product addded successfully.');
			session()->flash('msgType', 'success');
			return redirect('products');
		}*/
    }

    public function show(Supplier $supplier)
    {
        //
    }

    public function edit(Supplier $supplier,$id)
    {
        //
    }

    public function update(Request $request, Supplier $supplier)
    {
        //
    }

    public function destroy(Supplier $supplier , $id)
    {
        //
    }
	
    public function woocomerceData()
    {
		/*
		//for delete
		$data1 = array();                                                                    
		$data_string1 = json_encode($data1);                                                                                   

		$consumer_key = 'ck_85cf3eb73d3eeed9085350345008c6ad2c3baffa'; 
		$consumer_secret = 'cs_e061027c217ba8efc646f6396bb9857fe4214b4d'; 

		$ch = curl_init('https://dev.nappies.co.nz/wp-json/wc/v3/products/50174?force=true&consumer_key='.$consumer_key.'&consumer_secret='.$consumer_secret);
		curl_setopt($ch, CURLOPT_USERPWD, $consumer_key . ":" . $consumer_secret);                                  
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string1);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string1))                                                                       
		); 
		$data1 = curl_exec($ch);
		$data1 = json_decode($data1);
		$data1 = response()->json([$data1]);
		return $data1;*/
		/*
		//for get 1single product 
		$data1 = array();                                                                    
		$data_string1 = json_encode($data1);                                                                                   

		$consumer_key = 'ck_85cf3eb73d3eeed9085350345008c6ad2c3baffa'; 
		$consumer_secret = 'cs_e061027c217ba8efc646f6396bb9857fe4214b4d'; 

		$ch = curl_init('https://dev.nappies.co.nz/wp-json/wc/v3/products/50161?consumer_key='.$consumer_key.'&consumer_secret='.$consumer_secret);
		curl_setopt($ch, CURLOPT_USERPWD, $consumer_key . ":" . $consumer_secret);                                  
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string1);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string1))                                                                       
		); 
		$data1 = curl_exec($ch);
		$data1 = json_decode($data1);
		$data1 = response()->json([$data1]);
		return $data1;*/
		
		//for get multiple product
		$data = array();                                                                    
		$data_string = json_encode($data);                                                                                   

		$consumer_key = 'ck_85cf3eb73d3eeed9085350345008c6ad2c3baffa'; 
		$consumer_secret = 'cs_e061027c217ba8efc646f6396bb9857fe4214b4d'; 

		$ch = curl_init('https://dev.nappies.co.nz/wp-json/wc/v3/products?consumer_key='.$consumer_key.'&consumer_secret='.$consumer_secret);
		//$ch = curl_init('https://dev.nappies.co.nz/wp-json/wc/v3/products?consumer_key='.$consumer_key.'&consumer_secret='.$consumer_secret);
		curl_setopt($ch, CURLOPT_USERPWD, $consumer_key . ":" . $consumer_secret);                                  
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
		); 
		$data = curl_exec($ch);
		$data = json_decode($data);
		//return $data;
		//truncate produt table start
		product::query()->truncate();
		//truncate produt table end
		//Add data in produt table start
		foreach($data as $val){
			if(!empty($val->meta_data)){
				$meta_data[] = $val->meta_data[1]->value;
				$brand = $val->meta_data[1]->value;
			}else{
				$brand = '';
			}
			if(!empty($val->images)){
				$images = $val->images[0]->src;
				$images = $val->images[0]->src;
			}else{
				$images = '';
			}
			$res=new product;
			$res->product_id=$val->id;
			$res->name=$val->name;
			$res->sku=$val->sku;
			$res->brand_name=$brand;
			$res->images=$images;
			$res->supplier_name=$val->supplier_id;
			$res->supplier_code=$val->supplier_sku;
			$res->purchase_cost=$val->purchase_price;
			$res->uom='';
			$res->regular_price=$val->regular_price;
			$res->barcode=rand(00000000, 99999999);
			$res->manage_stock=$val->manage_stock=='true' || $val->manage_stock=='yes' ? 'yes' : 'No';
			$res->current_stock=$val->stock_quantity;
			$res->threshold=$val->out_stock_threshold;
			$res->bin_location='';
			$res->save();
		}
		//Add data in produt table end
		session()->flash('msg', 'Data addded successfully.');
		session()->flash('msgType', 'success');
		return redirect('products');
		//return $meta_data;
		//return $data;
    }
}
