<?php

namespace App\Http\Controllers;

use App\Models\email_management;
use Illuminate\Http\Request;
use DB;

class emailManagement extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	public function email()
    {
        return view('email/emails')->with('emails',email_management::orderBy('id')->get());
    }
	public function create()
    {
		return view('email/create-email');
	}
	public function store(Request $request)
    {
		if($request['submit']){
			$res=new email_management;
				$res->emailSubject=$request->input('emailSubject');
				$res->emailBody=$request->input('emailBody');
				if($res->save()){
					$request->session()->flash('msg', 'Email added successfully');
					$request->session()->flash('msgType', 'success');
					return redirect('emails');
				}else{
					$request->session()->flash('msg', 'Email not added');
					$request->session()->flash('msgType', 'danger');
					return redirect('emails');
				}
		}
	}
	public function edit_email($id)
	{
		$email = DB::table('email_management')
			->select('*')
			->where('id', $id)
			->first();
		return view('email/edit-email')->with('email',$email);
	}
	public function edit_email_post(Request $request, email_management $email_management, $id)
	{
		//echo $id;exit;
		if($request['submit']){
			$res=email_management::find($id);
			$res->emailSubject=$request->input('emailSubject');
			$res->emailBody=$request->input('emailBody');
			if($res->save()){
				$request->session()->flash('msg', 'Email updated successfully');
				$request->session()->flash('msgType', 'success');
				return redirect('emails');
			}else{
				$request->session()->flash('msg', 'Email not updated');
				$request->session()->flash('msgType', 'danger');
				return redirect('edit-email/'.$id);
			}
		}
	}
}
