<?php

namespace App\Http\Controllers;

use App\Models\activity_message;
use App\Models\activity_log;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\File;

class messageLog extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
	public function store_log($msg,$ref)
    {
		$activity_message = DB::table('activity_message')
			->select('*')
			->where('id', $msg)
			->first();
		$user = auth()->user();
		$login_user_id =  $user->id;
		$login_user_name =  $user->name;
		
		//insert into activity_log table start
		$res=new activity_log;
		$res->login_user_id=$login_user_id;
		$res->login_user_name=$login_user_name;
		$res->message_id=$msg;
		$res->message=$activity_message->message;
		$res->activity_date=date('Y-m-d H:i:s');
		if($ref!=''){
			$res->ref_no=$ref;
		}
		if($res->save()){
			return 1;
		}else{
			return 2;
		}
		//insert into activity_log table start
	}
	public function get_log($ref)
    {
		$log_details = activity_log::select('*')
			->where('ref_no', $ref)
			->orderBy('id', 'desc')->first();
		return $log_details;
	}
	public function get_status_changelog($message_id,$ref)
    {
		$log_details = activity_log::select('*')
			->where('message_id', $message_id)
			->where('ref_no', $ref)
			->orderBy('id', 'desc')->first();
		return $log_details;
	}
}
