<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use App\Models\Supplier_cost_details;
use Illuminate\Http\Request;
use DB;

class SupplierController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
		$supplierArr = DB::table('supplier')
				->select('supplier.*')
				->where('isActive', 0)
				->where('isDeleted', 0)
				->get();
        return view('supplier/supplier')->with('supplierArr',$supplierArr);
    }
	
    public function create()
    {
		$alreadyAddedId =  DB::table('supplier')
				->select('woocomerce_supplier_id')
				->where('isActive', 0)
				->where('isDeleted', 0)
				->groupBy('woocomerce_supplier_id')
				->get();
		$value = '';
		foreach($alreadyAddedId as $val){
			$value .= $val->woocomerce_supplier_id.',';
		}
		$value = trim($value,",");
		$wSupplierId = DB::table('product')
				->select('supplier_name')
				->where('status', 0)
				->where('delete_status', 0)
				->whereNotIn('supplier_name', [$value])
				->groupBy('supplier_name')
				->get();
        return view('supplier/add-supplier')->with('wSupplierId',$wSupplierId);
    }

    public function store(Request $request)
    {
		/*$request->validate([
			'email' => 'required|email|unique:supplier,email',
		]);*/
        $res=new supplier;
		$res->woocomerce_supplier_id=$request->input('woocomerce_supplier_id');
		$res->name=$request->input('name');
		$res->supplier_code=$request->input('supplier_code');
		$res->person_name=$request->input('person_name');
		$res->person_phone=$request->input('person_phone');
		$res->email=$request->input('email');
		$res->cc_email=$request->input('cc_email');
		$res->notes=$request->input('notes');
		$res->bank_details=$request->input('bank_details');
		$res->address=$request->input('address');
		$res->save();
		if($res->save()){
			
			foreach ($request->input('cost_label') as $n => $key){
				$res1=new supplier_cost_details;
				$res1->supplier_id=$res->id;
				$res1->cost_label=$request->input('cost_label')[$n];
				$res1->cost_value=$request->input('cost_value')[$n];
				$res1->save();
			}
			$request->session()->flash('msg', 'Data submitted successfully');
			$request->session()->flash('msgType', 'success');
			return redirect('suppliers');
		}else{
			$request->session()->flash('msg', 'Data not added');
			$request->session()->flash('msgType', 'danger');
			return redirect('suppliers');
		}
    }

    public function show(Supplier $supplier)
    {
        //
    }

    public function edit(Supplier $supplier,$id)
    {
		$alreadyAddedId =  DB::table('supplier')
				->select('woocomerce_supplier_id')
				->where('id', '!=' , $id)
				->where('isActive', 0)
				->where('isDeleted', 0)
				->groupBy('woocomerce_supplier_id')
				->get();
		$value = '';
		foreach($alreadyAddedId as $val){
			$value .= $val->woocomerce_supplier_id.',';
		}
		$value = trim($value,",");
		$wSupplierId = DB::table('product')
				->select('supplier_name')
				->where('status', 0)
				->where('delete_status', 0)
				->whereNotIn('supplier_name', [$value])
				->groupBy('supplier_name')
				->get();
		$poArr = DB::table('supplier_cost_details')
				->select('supplier_cost_details.*')
				->where('supplier_id', $id)
				->get();
        return view('supplier/edit-supplier')->with('supplierArr',supplier::find($id))->with('costArr', $poArr)->with('wSupplierId', $wSupplierId);
    }
    public function view(Supplier $supplier,$id)
    {
		$poArr = DB::table('supplier_cost_details')
				->select('supplier_cost_details.*')
				->where('supplier_id', $id)
				->get();
        return view('supplier/view-supplier')->with('supplierArr',supplier::find($id))->with('costArr', $poArr);
    }

    public function update(Request $request)
    {
		/*$request->validate([
			'email' => 'required|email|unique:supplier,email,'.$request->id,
		]);*/
        $res=supplier::find($request->id);
		
		$res->woocomerce_supplier_id=$request->input('woocomerce_supplier_id');
		$res->name=$request->input('name');
		$res->supplier_code=$request->input('supplier_code');
		$res->person_name=$request->input('person_name');
		$res->person_phone=$request->input('person_phone');
		$res->email=$request->input('email');
		$res->cc_email=$request->input('cc_email');
		$res->notes=$request->input('notes');
		$res->bank_details=$request->input('bank_details');
		$res->address=$request->input('address');
		$res->save();
		foreach ($request->input('cost_label') as $n => $key){
			$cost_detais = $request->input('cost_detais')[$n];
			if($cost_detais!=''){
				$res1=supplier_cost_details::find($cost_detais);
				$res1->supplier_id=$res->id;
				$res1->cost_label=$request->input('cost_label')[$n];
				$res1->cost_value=$request->input('cost_value')[$n];
				$res1->save();
			}else{
				$res1=new supplier_cost_details;
				$res1->supplier_id=$res->id;
				$res1->cost_label=$request->input('cost_label')[$n];
				$res1->cost_value=$request->input('cost_value')[$n];
				$res1->save();
			}
		}
		$request->session()->flash('msg', 'Data updated successfully');
		$request->session()->flash('msgType', 'success');
		return redirect('supplier_edit/'.$request->id);
    }

    public function destroy(Supplier $supplier , $id)
    {
		DB::table('supplier')->where('id', $id)->update(array('isDeleted' => 1));
        //supplier::destroy(array('id',$id));
		//DB::table('supplier_cost_details')->where('supplier_id', $id)->delete();
		session()->flash('msg', 'Data deleted successfully');
		session()->flash('msgType', 'success');
		return redirect('suppliers');
    }
	public function cost_delete(Request $request , $id, $costId)
    {
        if(supplier_cost_details::destroy(array('id',$id))){
			session()->flash('msg', 'Data deleted successfully');
			session()->flash('msgType', 'success');
		}else{
			session()->flash('msg', 'Data not deleted.');
			session()->flash('msgType', 'danger');
		}
		return redirect('supplier_edit/'.$costId);
    }
	public function changeStatus(Request $request)
    {
		$change = DB::table('supplier')->where('id', $request->id)->update(array('isActive' => $request->val));
		if($change){
			return response()->json(['success'=>'Status change successfully.']);
		}else{
			return response()->json(['success'=>'Status not change.']);
		}
    }
	public function supplierList(Request $request)
    {
		$supplierArr = DB::table('supplier')
				->select('supplier.*')
				->where('isActive', $request->status)
				->where('isDeleted', 0)
				->get();	
		$html = '<table id="example1" class="table table-bordered table-striped">
                <thead>
				<tr>
					<th>Supplier Name</th>
					<th>Supplier Code</th>
					<th>Contact Name</th>
					<th>Phone</th>
					<th>Email</th>
					<th class="text-center" style="width:80px">Action</th>
                </tr>
                </thead>
                <tbody>';
		foreach($supplierArr as $supplier){
			$html .= '<tr>
					<td><a href="'.route('supplier.view',[$supplier->id]).'">'.$supplier->name.'</a></td>
					<td>'.$supplier->supplier_code.'</td>
					<td>'.$supplier->person_name.'</td>
					<td>'.$supplier->person_phone.'</td>
					<td>'.$supplier->email.'</td>
					<td class="text-center" style="width:80px">
						<a href="'.route('supplier.edit',[$supplier->id]).'" data-toggle="tooltip" title="Edit" style="text-decoration:none;color:#333;"><i class="fas fa-edit" aria-hidden="true"></i></a>
					</td>
                </tr>';
		}
		$html .= '</tbody>
				</table>';
		return response()->json(['html'=>$html]);
	}
}
