<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class activity_message extends Model
{
    use HasFactory;
	protected $table = 'activity_message';
}
