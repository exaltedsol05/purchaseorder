<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier_cost_details extends Model
{
    use HasFactory;
	protected $table = 'supplier_cost_details';
}
