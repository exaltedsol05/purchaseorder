<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class delivery_address extends Model
{
    use HasFactory;
	protected $table = 'delivery_address';
}
