<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase_order_details extends Model
{
    use HasFactory;
	protected $table = 'purchase_order_details';
}
